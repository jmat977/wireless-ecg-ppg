#include "RFM12B_demo1.h"


//FREQ_SEL: 1-----434MHz
//FREQ_SEL: 2-----868MHz
//FREQ_SEL: 3-----915MHz
#define FREQ_SEL		1		
	

const word RFM12BFreqTbl[4][2] = {
 {0x0000, 0x0000},					//Not 315MHz
 {0x80DD, 0xA640},					//434MHz
 {0x80ED, 0xA640},					//868MHz
 {0x80FD, 0xA7D0},					//915MHz(load capacitor: 14PF)
};

const word RFM12BConfigTbl[13] = {
 0x82D8,        //enable receive,!PA
 0x94A0,        //VDI,FAST,BW:134kHz,LNA:0dBm,DRSSI:-103dBm
 0xC2AC,        //Data Filter,DQD
 0xCA80,        //
 0xCED4,        //
 0xCA83,        //FIFO8,SYNC,2DD4
 0xC49B,        //enable AFC
 0xCC77,        //
 0xE000,        //NOT USE
 0xC80e,        //NOT USE
 0xC691,        //BR=2.4K
 0x9810,        //434M,Max=8dbm ; 868M/915M,Max=6dbm
 0xC060,        //CLK 2.0MHz,LBD 2.2V
};
                        
const byte RFM12BData[] = {"HopeRF RFM COBRFM12BS"};
byte gb_Count=0;

/**********************************************************
**Name:     RFM12B_Config
**Function: Initialize RFM12B
**Input:    none
**Output:   none
**********************************************************/
void RFM12B_Config(void)
{
 byte i;
 for(i=0;i<2;i++)
  SPIWrite(RFM12BFreqTbl[FREQ_SEL][i]);                   //Frequency parameter
 for(i=0;i<13;i++)                                        //base parameters
  SPIWrite(RFM12BConfigTbl[i]);
}

/**********************************************************
**Name:     RFM12B_EntryRx
**Function: Set RFM12B entry Rx_mode
**Input:    None
**Output:   "0" for Error Status
**********************************************************/
byte RFM12B_EntryRx(void)
{
 Output_FSK;
 FSK=1;                                                   //FIFO mode
 RFM12B_Config();                                         //config RFM12B base parameters
	
 SPIWrite(0x82D8);                                        //Entry in RxMode
 return 1;
}

/**********************************************************
**Name:     RFM12B_EntryTx
**Function: Set RFM12B entry Tx_mode
**Input:    None
**Output:   "0" for Error Status
**********************************************************/
byte RFM12B_EntryTx(void)
{ 
 Output_FSK;
 FSK=1;                                                   //FIFO mode	
 RFM12B_Config();                                         //config RFM12B base parameters
	
 SPIWrite(0x8238);                                        //Entry in TxMode
 return 1;
}

/**********************************************************
**Name:     RFM12B_ClearFIFO
**Function: Change to RxMode from StandbyMode, can clear FIFO buffer
**Input:    None
**Output:   None
**********************************************************/
void RFM12B_ClearFIFO(void)
{
 SPIWrite(0xCA80);
 SPIWrite(0xCA83); 																			  //reset FIFO and read to receive next Byte
}

/**********************************************************
**Name:     RFM12B_Sleep
**Function: Set RFM12B to sleep mode 
**Input:    none
**Output:   none
**********************************************************/
void RFM12B_Sleep(void)
{
 SPIWrite(0x8200);                                        //sleep mode
}

/**********************************************************
**Name:     RFM12B_Standby
**Function: Set RFM12B to Standby mode
**Input:    none
**Output:   none
**********************************************************/
void RFM12B_Standby(void)
{
 SPIWrite(0x8208);                                        //standby mode
}

/**********************************************************
**Name:     RFM12B_TxByte
**Function: Send one byte data to RFM12B
**Input:    data
**Output:   0 -> error
**********************************************************/
byte RFM12B_TxByte(byte dat)
{
 byte  RGIT=0;
 word temp=0xB800;

 temp|=dat;
Loop: 
 SCK=0;
 nSEL=0;
 SDI=0;
 SCK=1;
 if(SDO) 																										
 {
  RGIT=1;
 }
 else
 {
  RGIT=0;
 }
 SCK=0;
 SDI=1;
 nSEL=1;
 if(RGIT==0)
 {
  goto Loop;
 }
 else
 {
  RGIT=0;
  SPIWrite(temp);
 }
 return (1);
}

/**********************************************************
**Name:     RFM12B_RxByte
**Function: Receive one byte data from RFM12B
**Input:    none
**Output:   data
**********************************************************/
byte RFM12B_RxByte(void)
{
 byte i,Result;

 SCK=0;
 SDI=0;
 nSEL=0;
 for(i=0;i<16;i++)                                        //skip status bits
  { 
  SCK=1;
  NOP();
  NOP();
  SCK=0;
  NOP();
  NOP();
	}
 Result=0;
 for(i=0;i<8;i++)                                         //read fifo data byte
  { 
  Result=Result<<1;
  if(SDO)
   {
   Result|=1;
   }
  SCK=1;
  NOP();
  NOP();
  SCK=0;
  NOP();
  NOP();
  }
 nSEL=1;
 return(Result);
}

/**********************************************************
**Name:     RFM12B_RxPacket
**Function: Check for receive one packet
**Input:    none
**Output:   "!0"-----Receive one packet
**          "0"------Nothing for receive
**********************************************************/
byte RFM12B_RxPacket(void)
{
 byte i;
 byte ret=0; 
 
 if(!nIRQ)
  {
  RxData[gb_Count++]=RFM12B_RxByte();
  if(gb_Count>=22)
   {
   gb_Count=0;
   RFM12B_ClearFIFO();                                    //clear FIFO
   for(i=0;i<14;i++)
    {
    if(RxData[i]!=RFM12BData[i])
     break;	
    }
    if(i>=14)
     ret=1;	                                              //Success
    else
     ret=0;
    for(i=0;i<14;i++)RxData[i]=0;
   }
  }
 else
  return(ret);
}

/**********************************************************
**Name:     RFM12B_TxPacket
**Function: Check RFM12B send over & send next packet
**Input:    none
**Output:   none
**********************************************************/
void RFM12B_TxPacket(void)
{
 byte i;
 if(!nIRQ)
  {
  if(!KeyFree_F)                                          //Key hold for press & hold for send 
   {
   TxStatus_F = 0;	
   RFM12B_Standby();
   }
  else
   {			
   for(i=0;i<4;i++)
    RFM12B_TxByte(0x55);                                  //Preamble
   RFM12B_TxByte(0xAA);		
   RFM12B_TxByte(0x2D);                                   //Sync word
   RFM12B_TxByte(0xD4);
   for(i=0;i<21;i++)                                      //Data
    RFM12B_TxByte(RFM12BData[i]);
   RFM12B_TxByte(0xAA);
   }
  }
}

