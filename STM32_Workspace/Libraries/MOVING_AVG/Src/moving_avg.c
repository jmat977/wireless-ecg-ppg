/*
 * moving_avg.c
 *
 *  Created on: Nov 4, 2020
 *      Author: jakub
 */

#include "../Inc/moving_avg.h"

void n_pt_moving_avg(CircBuff_f_t *CircBuff_f, const uint8_t n, float *acc){
	float elem=0,accumulator=0;

	for(int i=0;i<n;i++)
	{
	  if( !circbuff_f_peek_nth_oldest(CircBuff_f, &elem, i) ){
		  *acc=0;
		  //printf("unable to get nth oldest, %d",i);
		  return;
	  }
	  accumulator += elem;
	  *acc += elem;
	  //printf("Oldest_val_peek = %d\n", elem);

	}
	*acc = *(acc)/n;
	//*acc = *(acc)/n;
	//printf( "%d", *acc );
	//*acc=0;
}
