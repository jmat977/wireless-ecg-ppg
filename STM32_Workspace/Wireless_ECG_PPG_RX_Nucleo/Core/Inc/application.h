/*
 * application.h
 *
 *  Created on: Nov 29, 2020
 *      Author: jakub
 */

#ifndef INC_APPLICATION_H_
#define INC_APPLICATION_H_



#include "main.h"
#include "usart.h"
//#include "circ_buff_f.h"
#include "my_uart.h"
//#include "moving_avg.h"
//#include "circ_buff_f.h"
#include <math.h>
#include "stdbool.h"
#include "stm32l4xx_hal.h"
#include "rfm12b.h"

//extern CircBuff_f_t Main_buff_f, Abs_dif_long_buff;

void app();
void app_init();



#endif /* INC_APPLICATION_H_ */
