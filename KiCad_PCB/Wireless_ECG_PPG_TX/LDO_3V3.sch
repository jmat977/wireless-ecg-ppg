EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8000 5000
encoding utf-8
Sheet 3 9
Title ""
Date "2020-08-01"
Rev "1"
Comp "AGH Faculty of Electrical Engineering, Automatics, Computer Science and Biomedical Engineering"
Comment1 ""
Comment2 "Sytem for aquisition and transmission of biomedical data "
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3160 1720 0    50   Input ~ 0
V_IN
Text HLabel 4750 1720 2    50   Output ~ 0
V_OUT
$Comp
L Regulator_Linear:LM1117-3.3 U6
U 1 1 5F968F70
P 4050 1720
F 0 "U6" H 4050 1962 50  0000 C CNN
F 1 "LM1117-3.3" H 4050 1871 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 4050 1720 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 4050 1720 50  0001 C CNN
	1    4050 1720
	1    0    0    -1  
$EndComp
Wire Wire Line
	3160 1720 3400 1720
$Comp
L power:GNDD #PWR01
U 1 1 5F96AE27
P 4050 2340
F 0 "#PWR01" H 4050 2090 50  0001 C CNN
F 1 "GNDD" H 4054 2185 50  0000 C CNN
F 2 "" H 4050 2340 50  0001 C CNN
F 3 "" H 4050 2340 50  0001 C CNN
	1    4050 2340
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2340 4050 2310
$Comp
L Device:CP1 C17
U 1 1 5F96C3B4
P 4600 2050
F 0 "C17" H 4715 2127 50  0000 L CNN
F 1 "10μ " H 4715 2036 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 4600 2050 50  0001 C CNN
F 3 "~" H 4600 2050 50  0001 C CNN
F 4 "Tantalum" H 4715 1959 31  0000 L CNN "Type"
	1    4600 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5F96DC86
P 3400 2050
F 0 "C16" H 3515 2096 50  0000 L CNN
F 1 "1μ" H 3515 2005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3438 1900 50  0001 C CNN
F 3 "~" H 3400 2050 50  0001 C CNN
	1    3400 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2200 3400 2310
Wire Wire Line
	3400 2310 4050 2310
Connection ~ 4050 2310
Wire Wire Line
	4050 2310 4050 2020
Wire Wire Line
	4050 2310 4600 2310
Wire Wire Line
	4600 2310 4600 2200
Wire Wire Line
	3400 1900 3400 1720
Connection ~ 3400 1720
Wire Wire Line
	3400 1720 3750 1720
Wire Wire Line
	4400 1720 4600 1720
Connection ~ 4600 1720
Wire Wire Line
	4600 1720 4750 1720
Wire Wire Line
	4600 1720 4600 1900
$EndSCHEMATC
