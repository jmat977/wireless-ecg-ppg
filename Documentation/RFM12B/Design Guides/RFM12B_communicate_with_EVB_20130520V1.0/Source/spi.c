#include "RFM12B_demo1.h"

/**********************************************************
**Name:     SPICmd8bit
**Function: SPI Write one byte
**Input:    WrPara
**Output:   none
**note:     use for burst mode
**********************************************************/
void SPICmd8bit(byte WrPara)
{
 byte bitcnt;	
 nSEL = 0;
 SCK  = 0;

 for(bitcnt=8; bitcnt!=0; bitcnt--)
 	{
 	SCK = 0;
 	if(WrPara&0x80)
 		SDI = 1;
 	else
 		SDI = 0;
 	SCK = 1;
 	WrPara <<= 1;
 	}
 SCK = 0;
 SDI = 1;
}

/**********************************************************
**Name:     SPIRead8bit
**Function: SPI Read one byte
**Input:    None
**Output:   result byte
**Note:     use for burst mode
**********************************************************/
byte SPIRead8bit(void)
{
 byte RdPara = 0;
 byte bitcnt;
 
 nSEL = 0;
 SDI  = 1;																								//Read one byte data from FIFO, MOSI hold to High 
 for(bitcnt=8; bitcnt!=0; bitcnt--)
 	{
 	SCK = 0;
 	RdPara <<= 1;
 	SCK = 1;
 	if(SDO)
 		RdPara |= 0x01;
 	else
 		asm("NOP"); 
 	}
 SCK = 0;	
 return(RdPara);
}

/**********************************************************
**Name:   	SPIRead
**Function: SPI Read CMD
**Input:    adr -> address for read
**Output:   None
**********************************************************/
byte SPIRead(byte adr)
{
 byte tmp;	
 SPICmd8bit(adr);                                         //Send address first
 tmp = SPIRead8bit();	
 nSEL = 1;
 return(tmp);
}

/**********************************************************
**Name:     SPIWrite
**Function: SPI Write CMD
**Input:    WrPara -> address & data
**Output:   None
**********************************************************/
void SPIWrite(word WrPara)								
{                                                       
 byte bitcnt;    
 
 SCK  = 0;				
 nSEL = 0;
 
 WrPara |= 0x8000;                                        //MSB must be "1" for write 
 
 for(bitcnt=16; bitcnt!=0; bitcnt--)
 	{
 	SCK = 0;
 	if(WrPara&0x8000)
 		SDI = 1; 
 	else
 		SDI = 0;
	SCK = 1;
 	WrPara <<= 1;
 	}
 SCK = 0;
 SDI = 1; 
 nSEL= 1;
}         

/**********************************************************
**Name:     SPIBurstRead
**Function: SPI burst read mode
**Input:    adr-----address for read
**          ptr-----data buffer point for read
**          length--how many bytes for read
**Output:   None
**********************************************************/
void SPIBurstRead(byte adr, byte *ptr, byte length)
{
 byte i;
 if(length<=1)                                            //length must more than one
 	return;
 else
 	{
 	SCK = 0;			
	nSEL= 0;
 	SPICmd8bit(adr);	
 	for(i=0;i<length;i++)
  		ptr[i] = SPIRead8bit();
	nSEL= 1;
	}
}

/**********************************************************
**Name:     SPIBurstWrite
**Function: SPI burst write mode
**Input:    adr-----address for write
**          ptr-----data buffer point for write
**          length--how many bytes for write
**Output:   none
**********************************************************/
void BurstWrite(byte adr, byte *ptr, byte length)
{ 
 byte i;

 if(length<=1)                                            //length must more than one
 	return;
 else	
 	{ 	
 	SCK = 0;			
 	nSEL= 0;	
 	SPICmd8bit(adr|0x80);	
 	for(i=0;i<length;i++)
  		SPICmd8bit(ptr[i]);
	nSEL= 1;
	}
}

