/*
 * application.h
 *
 *  Created on: Nov 4, 2020
 *      Author: jakub
 */

#ifndef INC_APPLICATION_H_
#define INC_APPLICATION_H_





#include "main.h"
#include "usart.h"

//#include "../Libraries/UART/Inc/my_uart.h"
#include "moving_avg.h"
#include "circ_buff_f.h"
#include "my_uart.h"
#include "max86150.h"
#include "rfm12b.h"



#include <math.h>


void app();
void app_init();

void CDC_receive_bytes_callback(uint8_t *Buf, uint32_t *Len);




#endif /* INC_APPLICATION_H_ */
