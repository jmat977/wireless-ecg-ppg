/*
 * max86150.c
 *
 *  Created on: Dec 20, 2020
 *      Author: jakub
 */


#include "../Inc/max86150.h"
#include "i2c.h"
#include "usart.h"
#include "stdio.h"

#define ECG_FRAME_SIZE 200
uint32_t ecg_frame[ECG_FRAME_SIZE]={0};
uint32_t n = 0, sample=0, interrupt_ongoing=0;
uint8_t entry_ctr=0;


//global variables

char i2cWriteBuffer[10];
char i2cReadBuffer[I2C_BUFFER_SIZE]; //32 bytes in i2c buffer size in Arduino by default.
unsigned long tim = 0; //for counting milliseconds
volatile bool intFlagged = 0; //ISR variable
long data[4][5]; //data from the first measurement type
long dataAvg[4]; //currently using data1 for ECG. This variable stores the IIR filtered avg.
long dataBaseline[4]; //this variable stores the few second baseline for substraction (IIR filter)
int ind = 0; //index to keep track of whether to print data or not (not printing every sample to serial monitor b/c it is too slow)
int bits2Avg[4]; //array stores the selected averaging time constants set below for filtering (loaded in setup)
int bits2AvgBaseline[4]; //stores below filter time constants (loaded in setup)
char FIFOData[BYTES_2_READ]={0};


struct interrupts {
		uint8_t fifo_a_full;
		uint8_t ppg_rdy;
		uint8_t alc_ovf;
		uint8_t prox_int;
		uint8_t pwr_rdy;
		uint8_t vdd_oor;
		uint8_t ecg_rdy;
} int_st = {0};



void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	if( GPIO_Pin == GPIO_PIN_8){
		//max86150_int_callback();
		if(!interrupt_ongoing){
			entry_ctr++;
			intEvent();
			interrupt_ongoing=0;
		}


	}
}

//clears interrupts
void clearInterrupts(char *data)
{
	max86xxx_read_reg(InterruptStatusReg1,data,1);
	max86xxx_read_reg(InterruptStatusReg2,data,1);
}

void readInterrupts(){
	uint8_t IntStReg1=0, IntStReg2=0;
	max86xxx_read_reg(InterruptStatusReg1,IntStReg1,1);
	max86xxx_read_reg(InterruptStatusReg2,IntStReg2,1);
	int_st.fifo_a_full = IntStReg1 & (1<<7);
	int_st.ppg_rdy     = IntStReg1 & (1<<6);
	int_st.alc_ovf     = IntStReg1 & (1<<5);
	int_st.prox_int    = IntStReg1 & (1<<4);
	int_st.pwr_rdy     = IntStReg1 & (1<<0);
	int_st.vdd_oor     = IntStReg2 & (1<<7);
	int_st.ecg_rdy     = IntStReg2 & (1<<2);
}

void intEvent(void)
{
	uint8_t rx_data[100]={0}, rdptr[3]={0};
	interrupt_ongoing=1;
	//max86xxx_read_reg(FIFODataReg, rx_data, 3);
	max86xxx_read_ecg_sample();
	//max86xxx_read_reg(EcgConfigReg1, rdptr, 1);


    intFlagged = 1;
    //HAL_UART_Transmit(&huart2, '6', 1,100);
    if(entry_ctr<2){
    	clearInterrupts(i2cReadBuffer);
    }



}



uint8_t max86xxx_read_reg(uint8_t reg_addr, uint8_t *read_data, uint16_t length)
{
	uint8_t result = HAL_I2C_Mem_Read(&hi2c1, MAX86150_Addr, reg_addr, 1, read_data, length, HAL_MAX_DELAY);

	if(result != HAL_OK){
	  return false;
	}
	return true;
}

uint8_t max86xxx_write_reg(uint8_t reg_addr, uint8_t write_data, uint16_t length)
{
	uint8_t result = HAL_I2C_Mem_Write(&hi2c1, MAX86150_Addr, reg_addr, 1, &write_data, length, HAL_MAX_DELAY);

	if(result != HAL_OK){
	  return false;
	}
	return true;
}




int32_t max86xxx_read_ecg_sample(){
	uint8_t data[9]= {0},string_data[100], ptrs_fifo[2]={0};
	uint32_t ppg1_acc=0,ppg2_acc=0;
	int32_t ecg_acc=0;
	//max86xxx_read_reg(MAX86908_REG_FIFO_WR_PTR,ptrs_fifo, 1);
	//max86xxx_read_reg(MAX86908_REG_OVF_CNT,ptrs_fifo+1, 1);




	//uint8_t reg_content[2];
	uint8_t read_ptr = 0;
	uint8_t write_ptr = 0;

	max86xxx_read_reg(FIFOWritePointerReg, &write_ptr, 1);
	max86xxx_read_reg(FIFOReadPointerReg, &read_ptr, 1);

	//max86xxx_read_reg(MAX86908_REG_INT_STATUS_1,reg_content, 1);
	//max86xxx_read_reg(MAX86908_REG_INT_STATUS_2,reg_content+1, 1);

	//max86xxx_read_reg(MAX86908_REG_FIFO_DATA_REG,data, 3);

	uint8_t samples_to_read = write_ptr-read_ptr;
	//max86xxx_is_ecg_rdy();
	/*
	if( (samples_to_read>0) & (n<ECG_FRAME_SIZE)){
		max86xxx_read_reg(MAX86908_REG_FIFO_DATA_REG,data, 3);
		n++;
	}
	*/

	/*
	int i=0;
	for (i = 0; i < 17; i++) {
		max86xxx_read_reg(FIFODataReg,data, 9);
		accumulator += (int32_t)(data[5] |data[4]<<8 |data[3]<<16 );
	}
	//accumulator = (accumulator/17);
	uint8_t size = sprintf(string_data,"%d\n\r",accumulator);
	*/
	if ( samples_to_read != 0 ){
		samples_to_read=17;
		uint32_t total_samples = samples_to_read;
		while(samples_to_read>0){
			max86xxx_read_reg(FIFODataReg,data, 9);
			ppg1_acc += (uint32_t)(data[2] |data[1]<<8 |data[0]<<16 );
			ppg2_acc += (uint32_t)(data[5] |data[4]<<8 |data[3]<<16 );
			ecg_acc  +=  (int32_t)(data[8] |data[7]<<8 |data[6]<<16 );
			samples_to_read--;

		}
		ppg1_acc = ppg1_acc/total_samples;
		ppg2_acc = ppg2_acc/total_samples;
		ecg_acc  = ecg_acc/total_samples;

		uint8_t size = sprintf(string_data,"%d,%d,%d\n\r",ppg1_acc, ppg2_acc, ecg_acc);
		HAL_UART_Transmit(&huart2, (uint8_t *)string_data, size,100);
		}
	return sample;
}

/*
uint8_t max86xxx_init(){
	//uint8_t reg_addr = MAX86908_REG_WHOAMI_PART_ID;
	uint8_t reg_content[2]={0},string_data[20];
	max86xxx_write_reg(MAX86908_REG_SYSTEM_CTRL,MAX86908_SYSTEM_RESET,1);
	HAL_Delay(10);
	max86xxx_write_reg(MAX86908_REG_FIFO_CFG,
						MAX86908_A_FULL_CLR|
						MAX86908_A_FULL_TYPE|
						MAX86908_FIFO_ROLLS_ON_FULL|
						(MAX86908_FIFO_A_FULL_MASK& 0xA),
						1);

	max86xxx_write_reg(MAX86908_REG_ECG_CFG_1, (MAX86908_ECG_CFG_1_MASK & MAX86908_ECG_ADC_OSR_3), 1);
	max86xxx_write_reg(MAX86908_REG_ECG_CFG_3, (MAX86908_MASK_ECG_PGA_GAIN & MAX86908_ECG_PGA_GAIN_8), 1);
	//max86xxx_write_reg(MAX86908_REG_SYSTEM_CTRL,MAX86908_SYSTEM_RESET,1);
	max86xxx_write_reg(MAX86908_REG_INT_ENABLE_2,MAX86908_INT_ECG_RDY_EN_,1);

	max86xxx_write_reg(MAX86908_REG_FIFO_WR_PTR, 0, 1);
	max86xxx_write_reg(MAX86908_REG_FIFO_RD_PTR, 0, 1);

	max86xxx_read_reg(MAX86908_REG_INT_STATUS_1,reg_content, 1);
	max86xxx_read_reg(MAX86908_REG_INT_STATUS_2,reg_content, 1);
	max86xxx_read_reg(MAX86908_REG_WHOAMI_PART_ID,reg_content, 1);
	uint8_t size = sprintf(string_data,"Part_ID:0x%X\n\r",reg_content[0]);
	HAL_UART_Transmit(&huart2, (uint8_t *)string_data, size,100);

	max86xxx_write_reg(MAX86908_REG_SYSTEM_CTRL,MAX86908_SYSTEM_FIFO_EN,1);
	max86xxx_write_reg(MAX86908_REG_FIFO_DATA_CTRL_1, (FD1_MASK & ECG_CHANNEL),1);

	max86xxx_read_reg(MAX86908_REG_FIFO_DATA_CTRL_1, reg_content, 1);


	//max86xxx_read_reg(MAX86908_REG_SYSTEM_CTRL,reg_content,1);
	size = sprintf(string_data,"fifo data ctrl:0x%X\n\r",reg_content[0]);
		HAL_UART_Transmit(&huart2, (uint8_t *)string_data, size,100);
	return 1;
}
*/

void setupASIC(void)
{
	//int_st =
    //pc.printf("Running Setup\r\n");
    //runSetup = 0; //only run once
    //i2c.frequency(recommendedi2cFreq); //set I2C frequency to 400kHz
//        intPin.mode(PullUp); //pullups on the sensor board
    //configure MAX86150 register settings
    max86150_init();
    //pc.printf("register dump\r\n");

//    //print register configuration
//    regDump(MAX86150_Addr,0x00, 0x06);
//    regDump(MAX86150_Addr,0x08, 0x15);
//    regDump(MAX86150_Addr,0x3C, 0x3F);
//    //go to test mode
//    writeRegister(MAX86150_Addr,0xFF,0x54);
//    writeRegister(MAX86150_Addr,0xFF,0x4D);
//    regDump(MAX86150_Addr,0xCE, 0xCF);
//    regDump(MAX86150_Addr,0xD0, 0xD0);
//    writeRegister(MAX86150_Addr,0xFF,0x00);
      //clearInterrupts(i2cReadBuffer);

//    i2c.frequency(maxi2cFreq);
    //configure averaging
    if(PPG_ON) {
        bits2Avg[R_CHANNEL]=PPG_BITS_2_AVG;
        bits2Avg[IR_CHANNEL]=PPG_BITS_2_AVG;
        bits2AvgBaseline[R_CHANNEL]=PPG_BASE_LINE_BITS;
        bits2AvgBaseline[IR_CHANNEL]=PPG_BASE_LINE_BITS;
    }
    if(ECG_ON) {
        bits2Avg[ECG_CHANNEL]=ECG_BITS_2_AVG;
        bits2AvgBaseline[ECG_CHANNEL]=ECG_BASE_LINE_BITS;
    }
    if(ETI_ON) {
        bits2Avg[ETI_CHANNEL]=ETI_BITS_2_AVG;
        bits2AvgBaseline[ETI_CHANNEL]=ETI_BASE_LINE_BITS;
    }
    //pc.printf("Done w/setup\r\n");
}


void max86150_init(void)
{
	//Configure the FIFO Settings

	max86xxx_write_reg(0x02, 0x80, 1); // 0x80 for A_FULL_EN
	max86xxx_write_reg(0x0D, 0x01, 1); // Reset part
	max86xxx_write_reg(0x0D, 0x04, 1); // Enable FIFO
	max86xxx_write_reg(0x08, 0x1F, 1); // 0x1F for FIFO_ROLLS_ON_FULL to 1 and lost old samples when FIFO is full, Read FIFO data when there are 17 samples

	//Enable the PPG and ECG Modes
	max86xxx_write_reg(0x09, 0x21, 1); // LED1 in slot 1 and LED2 in slot 2
	max86xxx_write_reg(0x0A, 0x09, 1); // ECG in slot 3

	//Configure the Acquisition Settings for the Best PPG Performance
	max86xxx_write_reg(0x11, 0x55, 1); // LED1 current setting, optimal setting can vary depending on human physiology
	max86xxx_write_reg(0x12, 0x55, 1); // LED2 current setting, optimal setting can vary depending on human physiology
	max86xxx_write_reg(0x0E, 0xD3, 1); // 0xD3 for PPG_ADC_RGE= 32µA, PPG_SR = 100Hz, PPG_LED_PW = 400μs, actual sample rate can vary depending on the use case
	max86xxx_write_reg(0x0F, 0x18, 1); // 0x18 for 20µs delay from the rising edge of the LED to the start of integration

	//Configure the Acquisition Settings for the Best ECG Performance
	max86xxx_write_reg(0x3C, 0x03, 1); //0x03 for ECG_ADC_OSR = 200Hz, actual sample rate can vary depending on the use case
	max86xxx_write_reg(0x3E, 0x0D, 1); // 0x0D for PGA_ECG_Gain = 8, and IA_Gain = 9.5, total gain = 76 V/V being the most accurate gain setting

	//Configure the AFE Settings for the Best ECG Performance
	max86xxx_write_reg(0xFF, 0x54, 1); // series of code to set appropriate AFE settings, to be executed in sequential order
	max86xxx_write_reg(0xFF, 0x4D, 1);
	max86xxx_write_reg(0xCE, 0x0A, 1);
	max86xxx_write_reg(0xCF, 0x18, 1);
	max86xxx_write_reg(0xFF, 0x00, 1); // Complete

} //end max86150_init


bool readFIFO(int numSamples, char *fifodata)
{
//    char stat[1];
    bool dataValid = 0;
//    uint8_t tries = 0;
//    char newReadPointer;
//    clearInterrupts(stat);
    //get FIFO position
//    readRegisters(MAX86150_Addr, FIFOReadPointerReg, i2cReadBuffer, 1); //you can do more sophisticated stuff looking for missed samples, but I'm keeping this lean and simple
//    char readPointer = i2cReadBuffer[0];
//    while(!dataValid) {
//        tries++;
        //try reading FIFO
        max86xxx_read_reg(FIFODataReg, fifodata, BYTES_2_READ); //get data
        //see if it worked if you are not servicing interrupts faster than the sample rate
        //if you are servicing interrupts much faster than the sample rate, then you can get rid of the extra pointer register check.
        dataValid=1;
        //readRegisters(MAX86150_Addr, FIFOReadPointerReg, i2cReadBuffer, 1); //check that the read pointer has moved (otherwise FIFO was being written to)
//        newReadPointer = i2cReadBuffer[0];
//        if( (newReadPointer - readPointer == numSamples) || (newReadPointer+32 -readPointer ==numSamples ) ){ //check that we got the right number of samples (including FIFO pointer wrap around possiblity)
//            dataValid=1;
//            return 1;
//        }
//        else if (tries > 1) { //if it failed twice, you've got a different problem perhaps, exiting with error code (0) so you can void the data
//            break;
//        }
//        else {
//            wait_us(100); //try again a moment later
//        }
//    }
    return dataValid;
} //end readFIFO

//for serial monitoring
void printData(uint16_t numSamples,char *fifodata)
{
	uint8_t size=0, str_data[1000]={0};
    //cat and mask the bits from the FIFO
    for (int n = 0; n < numSamples; n++) { //for every sample
        char p = BYTES_PER_SAMPLE;
        for (int m=0; m<NUM_MEAS_EN; m++) { //for every enabled measurement
            data[m][n] = ( (long)(fifodata[p*n +3*m] & 0x7) << 16) | ( (long)fifodata[p*n + 1+3*m] << 8) | ( (long)fifodata[p*n + 2+3*m]);
            if (bitRead(data[m][n], 17) &&  ( (ECG_CHANNEL==m) || (ETI_CHANNEL==m) )  ) {//handle ECG and ETI differently than PPG data.       data[m][n] |= 0xFFFC0000;
                data[m][n] |= 0xFFFC0000;
            }
        }
    }
    for (int n = 0; n < numSamples; n++) { //for every sample
        ind++;
        ind = ind % PRINT_EVERY;

        //calc avg and baseline
        for(int m=0; m<NUM_MEAS_EN; m++) { //for every enabled measurement
            dataAvg[m] += ( data[m][n] - dataAvg[m] ) >> bits2Avg[m] ; //get the running average
            dataBaseline[m] +=  ( data[m][n] - dataBaseline[m] ) >> bits2AvgBaseline[m]; //get the long baseline
        }

        //print data
        if (ind == 1) { //printing only every specified number of samples to reduce serial traffic to manageable level
            for (int m=0; m<NUM_MEAS_EN; m++) { //for every enabled measurement
                if(bits2AvgBaseline[m]>0) {
                	size = sprintf(str_data,"%i, ",dataAvg[m]-dataBaseline[m]);

                    //pc.printf("%i, ",dataAvg[m]-dataBaseline[m]); //print with baseline subtraction

                } else {
                	size = sprintf(str_data,"%i, ", dataAvg[m]);
                	HAL_UART_Transmit(&huart2, (uint8_t *)data, size,100);
                	//pc.printf("%i, ", dataAvg[m]); //print without baseline subtraction
                }
            }
			HAL_UART_Transmit(&huart2,"\r\n" , 3, 100);
            //pc.printf("\r\n");
        } //end print loop
    } //end sample loop
}//end printData()

bool bitRead(long data, uint8_t bitNum)
{
    long mask = 1<<bitNum;
    long masked_bit = data & mask;
    return masked_bit >> bitNum;
}

/*
uint8_t max86xxx_is_ecg_rdy(){
	uint8_t reg_content;
	max86xxx_read_reg(MAX86908_REG_INT_STATUS_2,&reg_content, 1);
	return (reg_content & MAX86908_INT_ECG_RDY_);
}
*/
/*
void MAX86150_setFIFOAverage(uint8_t numberOfSamples)
{
  bitMask(MAX86150_FIFOCONFIG, MAX86150_SAMPLEAVG_MASK, numberOfSamples);
}
*/
/*
void MAX86150_bitMask(uint8_t reg, uint8_t mask, uint8_t thing)
{
  // Grab current register context
  uint8_t originalContents;
  max86xxx_read_reg(reg, *originalContents,1);

  // Zero-out the portions of the register we're interested in
  originalContents = originalContents & mask;

  // Change contents
  max86xxx_write_reg( reg, originalContents | thing);
}
*/
/*
int max86xxx_init(void)
{
	int ret = 0;
	uint8_t buffer[2];
	struct max86xxx_dev *sd;
	struct max86xxx_sensor *sensor;
	int i;

	buffer[0] = MAX86XXX_REG_WHOAMI_REG_REV;
	ret = max86xxx_read_reg(buffer, 2);
	if (ret < 0) {
		pr_err("%s - communication failed\n");
		return -1;
	}

	if (buffer[1] != EXPECTED_PART_ID) {
		pr_err("Part ID Error. Part_id: %.2X, rev_id: %.2X, ret: %d\n",
			buffer[1], buffer[0], ret);
		return -1;
	}
	printf("Max86xxx detected\n");
	sd = malloc(sizeof(struct max86xxx_dev));
	if (sd == NULL) {
		printf("%s:%d - Out of memory\n", __func__, __LINE__);
		return -1;
	}

	memset(sd, 0, sizeof(struct max86xxx_dev));
	max86xxx_set_device_data(sd);
	sd->sensors = &max86xxx_sensors;
	sd->num_sensors = ARRAY_SIZE(max86xxx_sensors);

	sd->rev_id = buffer[0];
	sd->part_id = buffer[1];
	pr_info("Part_id: %.2X, rev_id: %.2X\n",
			sd->part_id, sd->rev_id);

	for (i = 0; i < sd->num_sensors; i++) {
		sensor = get_sensor_ptr(sd, i);
		printf("Initializing %.20s, i: %d\n", sensor->name, i);

		if (sensor->init) {
			ret = sensor->init(sd);
			if (ret < 0)
				goto fail;
		}
		sensor->dev = sd;
		printf("%s is succeed\n", sensor->name);
	}

	ret = max86xxx_startup_init(sd);
	if (ret < 0)
	{
		printf("max86xxx_startup_init FAILED\n");
		return -1;
	}
	register_gpio_irq_handler((void *)max86xxx_irq_handler, &maxdev_int, 0);

	init_events(&irq_events, 1000000, "irq");

#if 0
#if defined(MAX86XXX_PPG_ENABLED)
	ret = max86xxx_sensor_enable(sd, MAX86XXX_PPG_MODE, 1);
	if (ret < 0)
		return ret;
#elif defined (MAX86XXX_ECG_ENABLED)
	ret = max86xxx_sensor_enable(sd, MAX86XXX_ECG_MODE, 1);
	if (ret < 0)
		return ret;
#elif defined (MAX86XXX_ECPPG_ENABLED)
	ret = max86xxx_sensor_enable(sd, MAX86XXX_ECPPG_MODE, 1);
	if (ret < 0)
		return ret;
#endif
#endif
	return ret;
fail:
	printf("Init failed %s:%d\n", __func__, __LINE__);
	return -1;
}


int max86xxx_dump_regs(uint8_t *buf, int num_reg)
{
  int i;
  uint8_t tmp[2];

  for (i = 0; i < num_reg; i++) {
       tmp[0] = i;
    max86xxx_read_reg(tmp, 1);
    printf("address = [0x%.2x] val= 0x%.2x\n",i,tmp[0]);

       buf[i] = tmp[0];
  }
  return 0;
}
*/
