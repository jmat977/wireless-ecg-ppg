EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 5FA8A937
P 3100 3800
F 0 "J2" H 3135 3855 50  0000 L CNN
F 1 "Conn_01x02_Female" H 3128 3685 50  0000 L CNN
F 2 "Wireless_ECG_PPG_lib:Electrodes" H 3100 3800 50  0001 C CNN
F 3 "~" H 3100 3800 50  0001 C CNN
	1    3100 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5FA8BBC7
P 2410 3800
F 0 "J1" H 2365 3870 50  0000 C CNN
F 1 "Conn_01x02_Male" H 2060 3690 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 2410 3800 50  0001 C CNN
F 3 "~" H 2410 3800 50  0001 C CNN
	1    2410 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2610 3800 2900 3800
Wire Wire Line
	2610 3900 2900 3900
$Comp
L Mechanical:MountingHole H1
U 1 1 5FA9152B
P 1940 4880
F 0 "H1" H 2040 4926 50  0000 L CNN
F 1 "MountingHole" H 2040 4835 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1940 4880 50  0001 C CNN
F 3 "~" H 1940 4880 50  0001 C CNN
	1    1940 4880
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FA92285
P 1940 5085
F 0 "H2" H 2040 5131 50  0000 L CNN
F 1 "MountingHole" H 2040 5040 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1940 5085 50  0001 C CNN
F 3 "~" H 1940 5085 50  0001 C CNN
	1    1940 5085
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5FA928F4
P 1950 5315
F 0 "H4" H 2050 5361 50  0000 L CNN
F 1 "MountingHole" H 2050 5270 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1950 5315 50  0001 C CNN
F 3 "~" H 1950 5315 50  0001 C CNN
	1    1950 5315
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5FA92CAA
P 1945 5515
F 0 "H3" H 2045 5561 50  0000 L CNN
F 1 "MountingHole" H 2045 5470 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 1945 5515 50  0001 C CNN
F 3 "~" H 1945 5515 50  0001 C CNN
	1    1945 5515
	1    0    0    -1  
$EndComp
$EndSCHEMATC
