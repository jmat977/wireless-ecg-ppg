#include "RFM12B_demo1.h"

/**********************************************************
**Name:     BeepOn
**Function: Buzzer On
**Input:    none
**Output:   none
**********************************************************/
void BeepOn(void)
{

 CCPR1L  = 0xC0;                                          //Duty
 CCP1CON = 0b00001100;                                    //PWM Mode
 TMR2ON  = 1;                                             //Active
}

/**********************************************************
**Name:     BeepOff
**Function: Buzzer Off
**Input:    none
**Output:   none
**********************************************************/
void BeepOff(void)
{
 CCP1CON = 0b00000000;
 TMR2ON  = 0;                                             //Disable timer2
 Buzzer  = 0;
}




