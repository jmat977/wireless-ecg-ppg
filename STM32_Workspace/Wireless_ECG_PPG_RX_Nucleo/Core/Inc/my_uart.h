#ifndef MY_UART_H_
#define MY_UART_H_

#include "main.h"
#include "stdbool.h"

//#define UART_RX_DATA_SIZE 2048
#define UART_TX_DATA_SIZE 2048
#define UART_RX_DATA_SIZE 2048
void uart_rxne_handler(UART_HandleTypeDef *huart);
void uart_txe_handler(UART_HandleTypeDef *huart);
void uart_tc_handler(UART_HandleTypeDef *huart);

void uart_init(void);
//bool uart_update(void);
//bool uart_parse_cmd(uint8_t cmd[]);

bool uart_recv_byte(uint8_t byte);
bool uart_send_byte(uint8_t byte);

void uart_send_string(char str[]);
void uart_recv_string(char str[]);

//void uart_set_is_prompt_enabled(bool is_enabled);

#endif /* MY_UART_H_ */
