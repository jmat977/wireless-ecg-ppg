/*
 * max86150.h
 *
 *  Created on: Dec 20, 2020
 *      Author: jakub
 */

#ifndef LIBRARIES_MAX86150_INC_MAX86150_H_
#define LIBRARIES_MAX86150_INC_MAX86150_H_

#include "max86150_hw.h"
#include <stdbool.h>
#include <stdint.h>


//USER SELECTABLE****************************
#define  PPG_ON  			0 //turn on PPG (IR and Red both)
#define  ECG_ON 			1 //turn on ECG measurement
#define  ETI_ON 			0 //turn on ETI (lead check) electrical tissue impedance. Checks if your fingers are on or not.
//#define  ecgRate 			maxECGrate; //use normECGrate 800Hz or maxECGrate 1600Hz
#define  ECG_RATE 			MAX_ECG_RATE //use normECGrate 800Hz or maxECGrate 1600Hz
#define  PRINT_EVERY 		5 *(2-ECG_RATE) //print data only every X samples to reduce serial data BW (print fewer for faster sampling)
#define  PPG_BITS_2_AVG 	4 //log(2) of IIR filter divisor, data = data + (new_data-data)>>bits2Avg for PPG IR and R, use 0 for no averaging
#define  ECG_BITS_2_AVG		3 + (1-ECG_RATE) //(Recommend 3) log(2) of IIR filter divisor, data = data + (new_data-data)>>bits2Avg for ECG, use 0 for no averaging, or 4 with fast rate
#define  ETI_BITS_2_AVG 	0 //log(2) of IIR filter divisor, data = data + (new_data-data)>>bits2Avg for ETI, use 0 for no averaging
#define  PPG_BASE_LINE_BITS 9 //log(2) of baseline IIR filter divisor for PPG IR and PPG R, use 0 for no baseline removal
#define  ECG_BASE_LINE_BITS 9 //(Recommend 9-10)log(2) of baseline IIR filter divisor for ecg, use 0 for no baseline removal
#define  ETI_BASE_LINE_BITS 0 //log(2) of baseline IIR filter divisor for eti, use 0 for no baseline removal
#define  R_CURRENT 			0x50 //PPG LED current (when enabled), max=0xFF (255)
#define  IR_CURRENT 		0x50 //PPG LED current (when enabled), max=0xFF (255) in increments of 0.2mA or 0.4mA if hi pwr is enabled.
#define  RED_HI_PWR			0 //0 = use normal pwr level (recommended for PPG). Note hi pwr is useful for proximity detection.
#define  IR_HI_PWR 			0 //0 = use normal pwr level (recommended for PPG). Note hi pwr is useful for proximity detection.
#define  PPG_AVG  			1 //1 = turn on ppg averaging on-chip
#define  PPG_ON_CHIP_AVG  	3 //average 2^n ppg samples on chip
//bool runSetup =1; //tells program to reprogram ASIC (used on startup)
//***************************************

//USER SELECTABLE (though not recommended)
#define  PPG_RDY_INT_EN		0 //not using ppgRdyInt (too slow)
#define  ECG_RDY_INT_EN 	0 //not using ECG_RDY interrrupt. It will interrupt frequently if this is enabled (too fast)
//**************************************

//NOT USER CONFIGURABLE******************


#define  IR_CHANNEL  		PPG_ON-1 //-1 or 0
#define  R_CHANNEL 			(PPG_ON*(2))-1 //-1 or 1
#define  ECG_CHANNEL		ECG_ON*(1+2*PPG_ON)-1 //-1, 0, or 2
#define  ETI_CHANNEL 		ETI_ON*(1+ECG_ON+2*PPG_ON)-1 //-1, 0,1 or 3
#define  NUM_MEAS_EN 		2*PPG_ON + ECG_ON + ETI_ON //Index # of how many measurements will be in each block of the FIFO
#define  BYTES_PER_SAMPLE	(3*NUM_MEAS_EN) //each measurement is 3bytes
#define  SAMPLES_2_READ 	(I2C_BUFFER_SIZE/BYTES_PER_SAMPLE) //assuming 32 byte I2C buffer default
#define  ALMOST_FULL_INT_EN	1 //priority to AFULL vs. PPG in code
#define  BYTES_2_READ     	BYTES_PER_SAMPLE*SAMPLES_2_READ

void max86150_init(void);
uint32_t max86xxx_read_ecg_data();
uint8_t max86xxx_read_reg(uint8_t reg_addr, uint8_t *read_data, uint16_t length);
uint8_t max86xxx_write_reg(uint8_t reg_addr, uint8_t write_data, uint16_t length);

bool readFIFO(int numSamples, char *fifodata);

void setupASIC(void);

uint8_t max86xxx_is_ecg_rdy();
int32_t max86xxx_read_ecg_sample();

void max86150_int_callback();
//void fifo_data_control();

void clearInterrupts(char *data);
void printData(uint16_t numSamples,char *fifodata);
bool bitRead(long data, uint8_t bitNum);
//void max86150_dump_regs(uint8_t *buf, int num_reg);

//nt max86xxx_dump_regs(uint8_t *buf, int num_reg);


#endif /* LIBRARIES_MAX86150_INC_MAX86150_H_ */





