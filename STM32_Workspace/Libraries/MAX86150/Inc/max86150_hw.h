/*
 * max86150_hw.h
 *
 *  Created on: Dec 20, 2020
 *      Author: jakub
 */

#ifndef LIBRARIES_MAX86150_INC_MAX86150_HW_H_
#define LIBRARIES_MAX86150_INC_MAX86150_HW_H_
//============================ NEW


//********************************************** Arduino code
/* This program configures and reads data from the MAX86150 ECG/PPG (BioZ to come) ASIC from Maxim Integrated.
 *  Originally writter for Arduino. The MAX86150r4 or MAX86150r8 boards work well (others are noisy).
 *  Keep ECG electrodes as short as possible. Due to the nature of this chip low frequency RC filters cannot be used.
 *  Note the ECG input oscillates when not loaded with human body impedance.
 *  ECG inputs have internal ESD protection.
 *  Configuration is done by the initMAX86150() subroutine. Register settings are per recommendations from JY Kim at Maxim.
 *  In the global declarations are several options for enabling 4 kinds of measurements: PPG (IR and R), ECG, and ETI.
 *  The flex FIFO will be configured to order the selected measurements exactly in that order: first PPG, then ECG, then ETI.
 *  If PPG is not enabled (ppgOn=0), then the first data point will be ECG. If ecg is not enabled then the only data input will be ETI.
 *  This program uses by default the A-full interrupt which alerts when the FIFO is almost full. It does not attempt to read the entire FIFO,
 *  but rather reads a fixed number of samples. This operation is important because the ASIC can't update the FIFO while we are in the middle
 *  of an I2C transaction, so we have to keep I2C transactions short.
 *  Note the program limits the number of samples which can be read according to a specified I2C buffer size which will depend on the HW and
 *  library you use. For Arduino the default is 32 bytes.
 *  ECG data and PPG data are different. PPG data is unipolar 18 bits and has garbage in the MSBs that has to be masked.
 *  ECG data is bipolar (2's complement), 17 bits and has to have the MSBs masked with FFF.. if the signal is negative.
 *  ECG data is displayed with a short IIR noise filter and longer IIR baseline removal filter. The filter length is specified in bits
 *  so the math can be done with integers using register shifting, rather than floats.
 *  This chip practically requires at least 400kHZ i2c speed for ECG readout. (I have a query in to Maxim to see if it unofficially supports 1MHz.)
 *  ETI is electrical tissue impedance, a few kHz sampling of impedance to check for presence of user's fingers. This is not a datasheet feature.
 *  ETI settings were delivered by Maxim.
 *  ECG comes too fast to read every EOC interrupt. Either read every few interrupts or use AFULL int.
 *  PPG data is measured at a much lower rate than ECG, but data is buffered through in the FIFO, showing the most recent sample.
 *  To reduce data rate it would be possible to only send PPG data when it changes. Otherwise ECG can be filtered down to the PPG data rate and
 *  filtered data sent at a the reduced bandwidth (what Maxim does on their BLE EV kit).
 *
 *  IMPORTANT: Maxim recommends to read the FIFO pointer after getting data to make sure it changed. I have not implemented that yet.
 *  Basically, if you try to read the FIFO during the few clock cycles while it is being updated (a rare but eventual event),
 *  then the FIFO data will be garbage. You have to reread the FIFO.
 *
 *  BTW I am not convinced it is possible to mix PPG and ETI in the same row of the flex FIFO. Eg. FD2= ECG, FD1 = PPG
 *
 *  Note: IR and R LED power should be closed loop controlled to an optimal region above 90% of full scale for best resolution.
 *  (Not yet implemented).
 *
 *  Note: PPG system uses sophisticated background subtraction to cancel ambient light flicker and large ambient light signals (sunlight).
 *  This is important for finger measurements, but less important for measurements indoors under the thigh, for example, where a generic
 *  light source and LED can be used in DC operation.
 *
 *  Note the PPG ADC uses feedback cancellation from the current source to reduce noise. No LED current will result in noisy ADC readings.
 *  In other words you can't measure ADC noise without LED current. There are test registers not available to us which can disconnect the photodiode and
 *  measure with noise with test currents on the chip.
 *
 *  Note ASIC has configurable on-chip PPG averaging if-desired (prior to being written to FIFO).
 */
//#include "math.h"
#include <inttypes.h>
//Register definitions
#define MAX86150_Addr 0xBC //updated per I2Cscanner, 8 bit version of 7 bit code 0x5E
#define InterruptStatusReg1 0x00 //Interrupt status byte 0 (read both bytes 0x00 and 0x01 when checking int status)
#define InterruptStatusReg2 0x01
#define InterruptEnableReg1 0x02 //Interrupt enable byte 0
#define InterruptEnableReg2 0x03
#define FIFOWritePointerReg 0x04
#define OverflowCounterReg 0x05
#define FIFOReadPointerReg 0x06
#define FIFODataReg 0x07
#define FIFOConfigReg 0x08
#define FIFODataControlReg1 0x09
#define FIFODataControlReg2 0x0A
#define SystemControlReg 0x0D
#define ppgConfigReg0 0x0E
#define ppgConfigReg1 0x0F
#define ProxIntThreshReg 0x10
#define LED1PulseAmpReg 0x11
#define LED2PulseAmpReg 0x12
#define LEDRangeReg 0x14
#define LEDPilotPAReg 0x15
#define EcgConfigReg1 0x3C
#define EcgConfigReg2 0x3D //!!!!!
#define EcgConfigReg3 0x3E
#define EcgConfigReg4 0x3F //!!!!!
#define PartIDReg 0xFF
#define maxi2cFreq 1000000
#define recommendedi2cFreq 400000
#define interrupt_pin D12 //INTB pin --see InterruptIn declaration
#define MAX_ECG_RATE 0
#define NORM_ECG_RATE 1
//#define BaudRate 921600
//#define BaudRate 256000
//also try 921600, 460800 230400
#define I2C_BUFFER_SIZE 32 //32 was stable. In this rev exploring 36



void intEvent(void);




#endif /* LIBRARIES_MAX86150_INC_MAX86150_HW_H_ */
