/*
 * circ_buff.h
 *
 *  Created on: Sep 19, 2020
 *      Author: jakub
 */

#ifndef CIRC_BUFF_H_
#define CICR_BUFF_H_



#include "main.h"
#include <stdbool.h>
#include <string.h>

typedef struct CircBuff_t
{
	uint8_t *data;
	uint32_t size;
	uint32_t rd, wr;
}CircBuff_t;

void circbuff_init(CircBuff_t *circbuff, uint8_t *data, uint32_t size);
bool circbuff_write_newest(CircBuff_t *circbuff, uint8_t *elem);
bool circbuff_discard_newest(CircBuff_t *circbuff, uint8_t *elem);
bool circbuff_get_oldest(CircBuff_t *circbuff, uint8_t *elem);
bool circbuff_peek_nth_oldest(CircBuff_t *circbuff, uint8_t *elem, uint32_t n);
void circbuff_emptify(CircBuff_t *circbuff);
bool circbuff_is_empty(CircBuff_t *circbuff);
bool circbuff_is_full(CircBuff_t *circbuff);

#endif /* CIRC_BUFF_H_ */
