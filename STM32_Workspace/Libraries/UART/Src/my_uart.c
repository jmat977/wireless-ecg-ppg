#include "../../CIRC_BUFF/Inc/circ_buff.h"
#include "../../UART/Inc/my_uart.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern UART_HandleTypeDef huart2;

static volatile CircBuff_t tx_ringbuf;
static volatile CircBuff_t rx_ringbuf;
static volatile uint8_t rx_data[UART_RX_DATA_SIZE];
static volatile uint8_t tx_data[UART_TX_DATA_SIZE];
volatile uint8_t rx_byte;
volatile uint8_t rx_bytes[128];

extern uint8_t msg_requested;

uint8_t sample_available =0;

volatile bool is_cmd_ready = false;
//static volatile bool is_rx_exceeded = false;
//static volatile bool should_send_prompt = true;

//static volatile bool is_echo_enabled = true;
//static volatile bool is_prompt_enabled = true;

void uart_rxne_handler(UART_HandleTypeDef *huart)
{
	__HAL_UART_DISABLE_IT(huart, UART_IT_RXNE);
	// Clear RXNE by reading RDR.
	uint8_t rx_byte = huart->Instance->RDR;


	if (rx_byte == '\r' || rx_byte == '\n') {
			is_cmd_ready = true;
		}
	circbuff_write_newest((CircBuff_t *)&rx_ringbuf, &rx_byte);

	//__HAL_UART_GET_IT(&huart2, UART_IT_RXNE);
	huart->RxState = HAL_UART_STATE_READY;
	HAL_UART_Receive_IT(&huart2, (uint8_t *)&rx_byte, 1);
}

void uart_tc_handler(UART_HandleTypeDef *huart)
{
	__HAL_UART_DISABLE_IT(huart, UART_IT_TC);
}

void uart_txe_handler(UART_HandleTypeDef *huart)
{
	if (circbuff_is_empty((CircBuff_t *)&tx_ringbuf)) {
		__HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
		__HAL_UART_ENABLE_IT(huart, UART_IT_TC);
	} else {
		uint8_t tx_byte;
		circbuff_get_oldest((CircBuff_t *)&tx_ringbuf, &tx_byte);

		// Writing to TDR starts transmission.
		huart->Instance->TDR = tx_byte;
	}
}

int __io_putchar(int ch)
{
	if (uart_send_byte(ch)) {
		return ch;
	}
	return EOF;
}


/*
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	//circbuff_write_newest((CircBuff_t *)&rx_ringbuf, &rx_byte);

	float ECG_value = 0;
	unsigned char byte_arr[4] = {rx_bytes[0],rx_bytes[1],rx_bytes[2],rx_bytes[3]};

 	if( rx_bytes[0] | rx_bytes[1] | rx_bytes[2] | rx_bytes[3] ){
 		memcpy(&ECG_value, &byte_arr, sizeof(ECG_value));
 	 	add_ecg_sample(ECG_value);
 	 	sample_available = 1;
 	}
	//int32_t elem = rx_bytes[0]|(rx_bytes[1]<<8)|(rx_bytes[2]<<16)|(rx_bytes[3]<<24);

	HAL_UART_Receive_IT(&huart2, rx_bytes, 4); // Ponowne włączenie nasłuchiwania
	*/


/*
int __io_getchar(void)
{
	uint8_t c=0;
	if ( uart_recv_byte(c) ){
		return c;
	}
	return EOF;
}
*/
void uart_init(void)
{
	circbuff_init((CircBuff_t *)&rx_ringbuf, (uint8_t *)&rx_data, UART_RX_DATA_SIZE);
	circbuff_init((CircBuff_t *)&tx_ringbuf, (uint8_t *)&tx_data, UART_TX_DATA_SIZE);
	//__HAL_UART_ENABLE_IT(&huart2, UART_IT_TC);
	HAL_UART_Receive_IT(&huart2, (uint8_t *)&rx_byte, 4);
	//HAL_UART_Receive_IT(&huart2, &rx_byte, 1); // Ponowne włączenie nas
}

bool uart_recv_byte(uint8_t byte)
{
	return circbuff_get_oldest((CircBuff_t *)&rx_ringbuf, &byte);
}

void uart_recv_string(char str[])
{
	uint8_t byte;
	uint32_t i=0;

	while(uart_recv_byte(byte))
		if (byte !='\0'){
			str[i] = byte;
			i++;
		}
		else{
			return; // buffer empty
		}
}



bool uart_send_byte(uint8_t byte)
{
	bool result = circbuff_write_newest((CircBuff_t *)&tx_ringbuf, &byte);
	__HAL_UART_ENABLE_IT(&huart2, UART_IT_TXE);
	return result;
}

void uart_send_string(char str[])
{
	for (uint32_t i = 0; str[i] != '\0'; ++i) {
		uart_send_byte(str[i]);
	}
}
/*
void my_printf(uint8_t *format_str,){
	uint8_t container[100];
	uint8_t size = sprintf(container,"Part_ID:0x%X\n\r",reg_content[0]);
	HAL_UART_Transmit(&huart2, (uint8_t *)string_data, size,100);
}
*/
