/*
 * application.c
 *
 *  Created on: Nov 4, 2020
 *      Author: jakub
 */


#include "../Inc/application.h"
//#include "../Libraries/CIRC_BUFF/Inc/circ_buff_f.h"

#include "usbd_cdc_if.h"
#include "usb_device.h"

#include "r_peak_alg.h"

//extern RTC_HandleTypeDef hrtc;
extern USBD_HandleTypeDef hUsbDeviceFS;


//static void RTC_CalendarConfig(void);
//static void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate);
uint8_t time[20]={0}, date[20]={0};

uint8_t UserTxBuffer[2048] = {'H','E','L','L','O',' '};
uint8_t UserRxBuffer[2048] = {0};

uint8_t byte_arr[100]={0};
float 	ECG_value = 0;

uint8_t *received_bytes_ptr = 0;
uint8_t received_byte = 0, msg_available = 0;
uint32_t num_of_bytes = 0;

extern volatile bool intFlagged;
extern char FIFOData[BYTES_2_READ];





void app_init(void){
  HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);

  HAL_UART_Init( &huart2 );
  //uart_init();


  r_peak_alg_init();

  rfm_config();

  //max86150_init();
  //https://protocentral.com/product/protocentral-max86150-ppg-and-ecg-breakout-with-qwiic-v2/setupASIC();
  //rfm_initTxMode();
  //rfm_transmisionChain(1);
  //HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

};







void app(void){
	uint8_t buffer[2], reg_content[2];
	static uint8_t ctr;

	HAL_Delay(50);
	HAL_GPIO_TogglePin(Red_LED_GPIO_Port, Red_LED_Pin);

	//print_debug("%i",8,str_data);
	//HAL_UART_Transmit(&huart2,"Hello\r\n" , 7, 100);




/*
    while(1) {
        char stat[1];
        static int n = 0;
        HAL_Delay(10);
        if (intFlagged) {
//            pc.printf("intFlagged\r\n");
            if (ALMOST_FULL_INT_EN) {
                n = 0;
                intFlagged = 0;
                readFIFO(SAMPLES_2_READ,FIFOData);
                readFIFO(SAMPLES_2_READ,FIFOData);
                printData(SAMPLES_2_READ,FIFOData);
//              printRawFIFO(samples2Read,FIFOData); //for debugging
            } //end if AFULL
            else { //this is to handle end of conversion interrupts (not configured by default)
                if (n < SAMPLES_2_READ) {
                    n++;
                    intFlagged = 0;
                    clearInterrupts(stat);
                    //      Serial.println(n);
                } else {
                    n = 0;
                    intFlagged = 0;
                    readFIFO(SAMPLES_2_READ,FIFOData);
                    //printData(SAMPLES_2_READ,FIFOData);
                    HAL_UART_Transmit(&huart2,"\r\n" , 3, 100);
                    //pc.printf("\r\n");
                    //      printRawFIFO(samples2Read);
                }
            } //end if/else AFULL
        } //end if intFlagged
    } //end while
*/
/*
	uint16_t status;
	do {
		status=rfm_readStatus();
	} while (!(status&RFM_SR_RGIT_MASK));

	rfm_transmisionChain(0);
*/
	//max86xxx_read_ecg_sample();

	//USBD_CDC_SetTxBuffer(&hUsbDeviceFS, UserTxBuffer, 6);
	//USBD_CDC_TransmitPacket(&hUsbDeviceFS);
	//HAL_Delay(50);


	//get_value(cnt);
	wait_for_sample();



	calc_moving_avg();
	calc_abs_dif();
	check_for_extremum();

	send_val();
/*
	cnt+=1;
	cnt = cnt%SIG_LEN;
	if(cnt == SIG_LEN-1){
		clean_up();
	}
	abs_cnt++;
*/


}


void CDC_receive_bytes_callback(uint8_t *Buf, uint32_t *Len){
	//unsigned char byte_arr[4] = {rx_bytes[0],rx_bytes[1],rx_bytes[2],rx_bytes[3]};
	received_bytes_ptr =  Buf;
	num_of_bytes = *Len;
	msg_available = 1;
}

void service_byte(){
	static uint8_t byte_ctr = 0;

	if(msg_available){
		ECG_value = atof(received_bytes_ptr);
		add_ecg_sample(&ECG_value);
		sample_available = 1;
		msg_available = 0;
	}
}

//	  RTC_TimeTypeDef Time = {0};

//Time.Hours = 0;
//Time.Minutes = 0;
//Time.Seconds = 0;
//HAL_RTC_SetTime(&hrtc, &Time, RTC_FORMAT_BIN);
  //HAL_Delay(2);
//HAL_RTC_GetTime(&hrtc, &Time, RTC_FORMAT_BIN);
//uint8_t second = Time.Seconds;


//uint8_t second = 0;
//printf("Hello, %u",second);

//H:%d, M%d, S:%d Time.Hours,Time.Minutes,Time.Seconds
//HAL_Delay(200);
//printf("count=%d");

//size = sprintf(data, "\nLiczba wyslanych wiadomosci: %d.\r\n", cnt);
//HAL_UART_Transmit_IT(&huart2, data, size); // R
//RTC_CalendarShow(time, date);
//sprintf("%s\t%s\r\n",time,date);


//static void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate)
//{
//  RTC_DateTypeDef sdatestructureget;
//  RTC_TimeTypeDef stimestructureget;
//
//   Get the RTC current Time
//  HAL_RTC_GetTime(&hrtc, &stimestructureget, RTC_FORMAT_BIN);
//
//   Get the RTC current Date
//  HAL_RTC_GetDate(&hrtc, &sdatestructureget, RTC_FORMAT_BIN);
//   Display time Format : hh:mm:ss
//  sprintf((char *)showtime, "%2d:%2d:%2d", stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);
//  Display date Format : mm-dd-yy
//  sprintf((char *)showdate, "%2d-%2d-%2d", sdatestructureget.Month, sdatestructureget.Date, 2000 + sdatestructureget.Year);
// }
