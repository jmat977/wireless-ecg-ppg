EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8000 5000
encoding utf-8
Sheet 7 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3060 1030 0    50   Input ~ 0
V_IN
Text HLabel 2810 1280 0    50   Input ~ 0
EN
Text HLabel 3770 930  2    50   Output ~ 0
3V3
$Comp
L Regulator_Linear:LM1117-3.3 U?
U 1 1 5F968F70
P 4050 1730
F 0 "U?" H 4050 1972 50  0000 C CNN
F 1 "LM1117-3.3" H 4050 1881 50  0000 C CNN
F 2 "" H 4050 1730 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 4050 1730 50  0001 C CNN
	1    4050 1730
	1    0    0    -1  
$EndComp
$EndSCHEMATC
