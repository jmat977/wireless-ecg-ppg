#include <pic.h>
#include <htc.h>
#include <stdio.h>
#include <pic16f1519.h>

/**********************************************************
//
**********************************************************/
#define RTC_INTV  0x8000                                  //RTC interval time  2s==0x0000
                                                          //                   1s==0x8000
                                                          //                   500ms==0xC000					
                                                          //                   250ms==0xE000

/**********************************************************
Macro definition
**********************************************************/
#define BitSet(var, bitno) ((var)|=(1<<(bitno)))	
#define BitClr(var, bitno) ((var)&= (~(1<<(bitno))))

typedef unsigned char byte;
typedef unsigned int  word;

/**********************************************************
struct
**********************************************************/
typedef union
{
 struct
	{
	byte Bit0: 1;
	byte Bit1: 1;
	byte Bit2: 1;
	byte Bit3: 1;
	byte Bit4: 1;
	byte Bit5: 1;
	byte Bit6: 1;
	byte Bit7: 1;
	}BBits;
 byte BByte;
}FlagSTR;	


/**********************************************************
IO define
**********************************************************/
//Input
//PortA                         // TRISA   PORTA   LATA  ANSELA   WPA	
//#define              RA0      //  0        1      1       0      -  //
#define DIO2In         RA1      //  1(0)     0	    0       0      -  //RF -DIO2		
#define DIO3In         RA2      //	1(0)     0      0       0      -  //RF -DIO3
#define DIO4In         RA3      //	1(0)     0      0       0      -  //RF -DIO4
//#define              RA4      //  0        1      1       0 	   -  //
//#define              RA5      //  0        0 	    0       0      -  //
//#define              RA6      //	1        0 	    0       0      -  //HF -Xout
//#define              RA7      //	1        0 	    0       0      -  //HF -Xin 

//PortB							          // TRISB   PORTB   LATB  ANSELB   WPB
#define DownKey        RB0      //  1        0      0       0      1		
#define LeftKey        RB1      //  1        0      0       0      1 
#define UpKey          RB2      //	1        0      0       0      1
#define RightKey       RB3      //	1        0      0       0      1
#define DIO1In         RB4      //	1        0      0       0      0  //RF -DIO1
#define DIO0In         RB5      //	1        0      0       0      0  //RF -DIO0
//#define              RB6      //  0        0      0       0      0  //PGM-CLK
//#define              RB7      //	0        0      0       0      0  //PGM-DAT 

//PortC                         // TRISC   PORTC   LATC  ANSELC   WPC
//#define              RC0      //  1        0      0       0      -  //LF -Xout
//#define              RC1      //	1        0      0       0      -  //LF -Xin
//#define              RC2      //  0        0      0       0      -  //
//#define              RC3      //	0        0      0       0      -  //
//#define              RC4      //	0        1      1       0      -  //
#define SDO            RC5      //	1        0      0       0      -  //SPI-SDO
//#define              RC6      //	0        1      1       0      -  //
//#define              RC7      //	1        0      0       0      -  //

//PortD                         // TRISD   PORTD   LATD  ANSELD   WPD
#define PORIn          RD0      //	1(0)     0      0       0      -  //RF -POR	
//#define              RD1      //	0        0      0       0 	   -  //
//#define              RD2      //	0        0      0       0 	   -  //
//#define              RD3      //	0        0      0       0 	   -  //
//#define              RD4      //	0        0      0       0 	   -  //
//#define              RD5      //	0        0      0       0 	   -  //
//#define              RD6      //	0        0      0       0 	   -  //
//#define              RD7      //	0        0      0       0 	   -  //

//PortE                         // TRISE   PORTE   LATE  ANSELE   WPE
//#define              RE0      //  0        0      0      0       0  //
//#define              RE1      //  0        0      0      0       0  //
//#define              RE2      //	0        1      1      0       0  //
//#define              RE3      //	1        0      0      0       0  //

//Output
//PortA                         // TRISA   PORTA   LATA  ANSELA   WPA	
#define nSEL           LATA0    //	0        1      1      0       -  //SPI-nSEL
#define DIO2Out        LATA1    //	1(0)     0      0      0       -  //RF -DIO2
#define DIO3Out        LATA2    //	1(0)     0      0      0       -  //RF -DIO3
#define DIO4Out        LATA3    //	1(0)     0      0      0       -  //RF -DIO4
//#define              LATA4    //  0        0      0      0       -  //
//#define              LATA5    //  0        0      0      0       -  //
//#define              LATA6    //	1        0      0      0       -  //HF -Xout
//#define              LATA7    //	1        0      0      0       -  //HF -Xin 

//PortB                         // TRISB   PORTB   LATB  ANSELB   WPB
//#define              LATB0    //  1        0      0      0       1		
//#define              LATB1    //  1        0      0      0       1 
//#define              LATB2    //	1        0      0      0       1
//#define              LATB3    //	1        0      0      0       1
#define	DIO1Out        LATB4    //	1        0      0      0       0  //RF -DIO1
#define	DIO0Out        LATB5    //	1        0      0      0       0  //RF -DIO0
//#define              LATB6    //  0        0      0      0       0  //PGM-CLK
//#define              LATB7    //	0        0      0      0       0  //PGM-DAT 

//PortC                         // TRISC   PORTC   LATC  ANSELC   WPC
//#define              LATC0    //  1        0      0      0       -  //LF -Xout
//#define              LATC1    //	1        0      0      0       -  //LF -Xin
#define	Buzzer         LATC2    //  0        0      0      0       -  //HAL-Buzzer
#define	SCK            LATC3    //	0        0      0      0       -  //
#define	SDI            LATC4    //	0        1      1      0       -  //
//#define              LATC5    //	1        0      0      0       -  //SPI-SDO
//#define              LATC6    //	0        1      1      0       -  //
//#define              LATC7    //	1        0      0      0       -  //

//PortD                         // TRISD   PORTD   LATD  ANSELD   WPD
#define	POROut         LATD0    //	1(0)     0      0      0       - 	//RF -POR	
#define	TxLed4         LATD1    //	0        0      0      0       -  //HAL-D4
#define	TxLed5         LATD2    //	0        0      0      0       -  //HAL-D5
#define	TxLed6         LATD3    //	0        0      0      0       -  //HAL-D6
#define	TxLed7         LATD4    //	0        0      0      0       -  //HAL-D7
#define	TxLed8         LATD5    //	0        0      0      0       -  //HAL-D8
#define	TxLed9         LATD6    //	0        0      0      0       -  //HAL-D9
#define	RxLed          LATD7    //	0        0      0      0       -  //HAL-D10

//PortE                         // TRISE   PORTE   LATE  ANSELE   WPE
//#define              LATE0    //  0        0      0      0       0  //
//#define              LATE1    //  0        0      0      0       0  //
//#define              LATE2    //	0        1      1      0       0  //
//#define              LATE3    //	1        0      0      0       0  //

#define	nIRQ           DIO0In   //RFM12B IRQ pin
#define	FSK            DIO2Out  //RFM12B FSK pin
#define	Output_FSK     TRISA2=0 //RFM12B FSK pin output

#define	TRISA_DATA     0xCE
#define PORTA_DATA     0x11
#define	LATA_DATA      0x11
#define	ANSELA_DATA    0x00	
                        	
#define	TRISB_DATA     0x3F
#define PORTB_DATA     0x00
#define	LATB_DATA      0x00
#define	ANSELB_DATA    0x00
#define	WPUB_DATA      0x0F
                        	
#define	TRISC_DATA     0xA3
#define	PORTC_DATA     0x50
#define	LATC_DATA      0x50
#define	ANSELC_DATA    0x00
                        	
#define	TRISD_DATA     0x01	
#define	PORTD_DATA     0x00	
#define	LATD_DATA      0x00
#define	ANSELD_DATA    0x00
                        	
#define	TRISE_DATA     0x08
#define	PORTE_DATA     0x04
#define	LATE_DATA      0x04
#define	ANSELE_DATA    0x00
#define	WPUE_DATA      0x00


/**********************************************************
Variable define
**********************************************************/
extern FlagSTR _SysTime;
	#define	SysTime		_SysTime.BByte
	#define	SysTime0	_SysTime.BBits.Bit0	
	#define	SysTime1	_SysTime.BBits.Bit1
	#define	SysTime2	_SysTime.BBits.Bit2
	#define	SysTime3	_SysTime.BBits.Bit3
	#define	SysTime4	_SysTime.BBits.Bit4
	#define	SysTime5	_SysTime.BBits.Bit5
	#define	SysTime6	_SysTime.BBits.Bit6
	#define	SysTime7	_SysTime.BBits.Bit7
	
extern FlagSTR _SecTime;
	#define	SecTime		_SecTime.BByte
	#define	SecTime0	_SecTime.BBits.Bit0
	#define	SecTime1	_SecTime.BBits.Bit1
	#define	SecTime2	_SecTime.BBits.Bit2
	#define	SecTime3	_SecTime.BBits.Bit3
	#define	SecTime4	_SecTime.BBits.Bit4
	#define	SecTime5	_SecTime.BBits.Bit5
	#define	SecTime6	_SecTime.BBits.Bit6
	#define	SecTime7	_SecTime.BBits.Bit7

extern FlagSTR _KeyFlag;
	#define	KeyFlag		_KeyFlag.BByte
	
	
	#define	KeyPre_F	_KeyFlag.BBits.Bit6
	#define	KeyFree_F 	_KeyFlag.BBits.Bit7
	
extern FlagSTR _Flag1;
	#define	Flag1 		_Flag1.BByte
	#define	TxStatus_F	_Flag1.BBits.Bit0
	#define	RxStatus_F	_Flag1.BBits.Bit1	

extern byte TxLedTime;
extern byte KeyTime;
extern byte RxLimtTime;

extern byte KeyCode;
extern byte TxLedCnt;

extern byte RxData[32];

/**********************************************************
Function declaration
**********************************************************/
//main.c
extern void InitSfr(void);
extern void InitPort(void);
extern void TxLedOff(void);
extern void TxLedFlash(void);

//interrupt.c

//buzzer.c
extern void BeepOn(void);
extern void BeepOff(void);

//keyscan.c
extern void KeyScan(void);
extern byte ReadKey(void);

//spi.c
extern void SPICmd8bit(byte WrPara);
extern byte SPIRead8bit(void);
extern byte SPIRead(byte adr);
extern void SPIWrite(word WrPara);
extern void SPIBurstRead(byte adr, byte *ptr, byte length);
extern void BurstWrite(byte adr, byte *ptr, byte length);

//RFM12B.c
extern byte RFM12B_EntryRx(void);
extern byte RFM12B_EntryTx(void);
extern void RFM12B_ClearFIFO(void);
extern void RFM12B_Sleep(void);
extern void RFM12B_Standby(void);
extern void RFM12B_Config(void);
extern byte RFM12B_RxPacket(void);
extern void RFM12B_TxPacket(void);




