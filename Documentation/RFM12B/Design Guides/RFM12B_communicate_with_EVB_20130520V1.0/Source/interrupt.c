#include "RFM12B_demo1.h"

/**********************************************************
**Name:     Interrupt ISR
**Function: 
**Input:    none
**Output:   none
**********************************************************/
void interrupt ISR(void)
{
 if(TMR0IF)                                               //TMR0 interrupt
 	{
 	TMR0IF = 0;
	SysTime++;
 	KeyTime++;
	RxLimtTime++;
	TxLedTime++;
 	}
 
 if(TMR1IF)                                               //TMR1 interrupt
 	{
 	TMR1IF = 0;
 	TMR1L  = (byte)RTC_INTV;
 	TMR1H  = (byte)(RTC_INTV>>8);	
	SecTime++;
 	}
}