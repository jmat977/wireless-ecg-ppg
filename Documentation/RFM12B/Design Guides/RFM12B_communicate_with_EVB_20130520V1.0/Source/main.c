//*******************************************************************************
//  Software Description: 
//        Using RFM12B communicate with RF-EVB 
//  Software Function:  
//   	  The same with HopeRF RF-EVB
//        1)If Key press, and send data out;
//        2)Without key press, It is in Rx mode;
//        3)If receive valid data, and it is BeepOn; 
//
//  Hardware: 
//        Base on "HopeRF RF-DK for PIC16"
//  Description: 
//                       PIC16F1519
//                    -----------------
//       RFM12B-nSEL-|RA0               |
//       RFM12B-FSK--|RA1            RD7|---Rx-LED
//       RFM12B-DCLK-|RA2            RD6|-
//       RFM12B-CLK--|RA3             | |-\-Tx-LEDs
//              |----|RA6            RD1|
//           XO-16MHz|               RD0|-nRES-RFM12B
//              |----|RA7            RC5|--SDO-RFM12B
//              KEY1-|RB0            RC4|--SDI-RFM12B
//              KEY2-|RB1            RC3|--SCK-RFM12B
//              KEY3-|RB2            RC2|-Buzzer
//              KEY4-|RB3            RC1|-----| 
//       RFM12B-VDI--|RB4               |  XO-32KHz
//       RFM12B-nIRQ-|RB5            RC0|-----|
//                    ------------------
//
//  RF module:           RFM12B
//  Carry Frequency:     434MHz/868MHz/915MHz
//  Frequency Deviation: +/-35KHz
//  Bit Rate:            2.4Kbps
//  Coding:              NRZ
//  Packet Format:       0x5555555555+0xAA2DD4+"HopeRF RFM COBRFM12BS" (total: 29 Bytes)
//  Tx Power Output:     about 8dBm (max.)
//  Tx Current:          about 22mA (typ.)
//  
//  Writer: Geman Deng
//  HOPE MICROELECTRONICS CO.,LTD. 
//  May 2013
//
//  Built with Hi-Tech PICC Version: 9.83 and MPLAB Version: 8.88
//******************************************************************************
#include "RFM12B_demo1.h"

__CONFIG(FOSC_ECH&FOSC_HS&WDTE_OFF&PWRTE_ON&MCLRE_OFF&CP_ON&BOREN_OFF&CLKOUTEN_OFF&IESO_OFF&FCMEN_OFF);
__CONFIG(WRT_OFF&VCAPEN_OFF&STVREN_ON&BORV_LO&LPBOR_OFF&LVP_OFF);

void main(void)
{ 
 InitSfr();
 InitPort();

 for(SysTime=0;SysTime<16;);                              //delay for stabilize

 RFM12B_Config();
 RxStatus_F = 0;
 TxStatus_F = 0;
 while(1)
 {
  KeyScan();	

  if(TxStatus_F)                                          //Tx Mode
   {
   TxLedFlash();	
   RFM12B_TxPacket();
   }
  else if(RxStatus_F)                                     //Rx Mode
   {
   if(RFM12B_RxPacket())                                   //does receive one packet message
    {
    RxLimtTime = 0;	
    BeepOn();
    }
   if(RxLimtTime>=15)                                     //over time stop Buzzer
    BeepOff();

   if(KeyPre_F)                                           //if key press, change to Tx Mode
    {
    BeepOff();
    RFM12B_EntryTx();
    RxStatus_F = 0;
    RxLed = 0;
    TxStatus_F = 1;
    TxLedCnt = 0;
    TxLedTime = 0;
    }
   }
  else                                                    //default status change to Rx Mode
   {
   RxStatus_F = 1;
   RxLed = 1;
   TxStatus_F = 0;
   TxLedOff();
   RFM12B_EntryRx();			
   }
 }	
}

/**********************************************************
**Name:     TxLedFlash
**Function: Blink Tx LEDs
**Input:    none
**Output:   none
**********************************************************/
void TxLedFlash(void)
{
 if(TxLedTime>=15)                                        //Blink rate	
  {
   TxLedCnt++;
   if(TxLedCnt>=7)                                        //0��7
    TxLedCnt = 0;
   TxLedTime = 0;
	
   switch(TxLedCnt)
    {
    case 1:                                               //D7
     TxLed7 = 1; break;
    case 2:                                               //D7 & D6 & D8
     TxLed6 = TxLed8 = 1; break;
    case 3:                                               //D7 & D6 & D8 & D4 & D5 & D9
     TxLed4 = TxLed5 = TxLed9 = 1; break;
    case 4:                                               //!D7
     TxLed7 = 0; break;
    case 5:                                               //!D7 & !D6 & !D8	
     TxLed6 = TxLed8 = 0; break;
    case 6:                                               //!D7 & !D6 & !D8 & !D4 & !D5 & !D9
     TxLed4 = TxLed5 = TxLed9 = 0; break;
    default:                                              //Off	
     break;
    }
  }
}

/**********************************************************
**Name:     TxLedOff
**Function: Off all Tx LEDs
**Input:    none
**Output:   none
**********************************************************/
void TxLedOff(void)
{
 LATD &= 0x81;		
}

/**********************************************************
**Name:     InitSfr
**Function: Initialize SFR
**Input:    none
**Output:   none
**********************************************************/
void InitSfr(void)
{
 //Bank7
 IOCBP   = 0;                                              //no need IO Change interrupt function	
 IOCBN	 = 0;	
 IOCBF   = 0;
 
 //Bank6 None
 
 //Bank5
 CCP1CON = 0;                                              //disable CCP
 CCP2CON = 0;
	
 //Bank4
 WPUB    = 0x0F;                                           //PortB.0-PortB.3,  enable pull up
 WPUE    = 0;		
 SSPCON1 = 0;		
 SSPCON2 = 0;
 SSPCON3 = 0;
 
 //Bank2
 BORCON  = 0x00;
 FVRCON  = 0x00;
 APFCON  = 0x02;

 //Bank1
 OPTION_REG  = 0x07;                                       //Timer0 DIV=256 -> 256*(4*256/16M)=16ms @16MHz 
 WDTCON  = 0;
 OSCCON  = 0;
 ADCON0  = 0;
 ADCON1	 = 0;
 PIE1    = 0;		
 PIE2    = 0;

 //Bank0
 PIR1    = 0;
 PIR2    = 0;
 TMR1L	 = (byte)RTC_INTV;
 TMR1H   = (byte)(RTC_INTV>>8);	
 T1CON   = 0x8D;                                           //Ext 32KHz��1:1	
 T1GCON  = 0;                                              //disable gate 
 
 T2CON   = 0b00000001;                                     //Fosc/4/4 = 16M/16 = 1MHz
 PR2     = 0xFF;		
 
 GIE     = 1;                                              //Enable GIE
 PEIE    = 1;                                              //Enable PEIE
 TMR0IE  = 1;                                              //Enable TMR0
 TMR1IE  = 1;                                              //Enable TMR1
}

/**********************************************************
**Name:     InitPort
**Function: Initialize Port
**Input:    none
**Output:   none
**********************************************************/
void InitPort(void)
{
 ANSELA= ANSELA_DATA;
 ANSELB= ANSELB_DATA;
 ANSELC= ANSELC_DATA;
 ANSELD= ANSELD_DATA;
 ANSELE= ANSELE_DATA;
 
 TRISA = TRISA_DATA;
 TRISB = TRISB_DATA;
 TRISC = TRISC_DATA;
 TRISD = TRISD_DATA;
 TRISE = TRISE_DATA;
 
 PORTA = PORTA_DATA;
 PORTB = PORTB_DATA;
 PORTC = PORTC_DATA;
 PORTD = PORTD_DATA;
 PORTE = PORTE_DATA;

 LATA  = LATA_DATA;
 LATB  = LATB_DATA;
 LATC  = LATC_DATA;
 LATD  = LATD_DATA;
 LATE  = LATE_DATA;
}

 












