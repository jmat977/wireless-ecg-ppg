EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8000 5000
encoding utf-8
Sheet 6 9
Title ""
Date "2020-08-01"
Rev "1"
Comp "AGH Faculty of Electrical Engineering, Automatics, Computer Science and Biomedical Engineering"
Comment1 ""
Comment2 "Sytem for aquisition and transmission of biomedical data "
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Battery_Management:MCP73831-2-OT U?
U 1 1 5F816B74
P 4100 1900
AR Path="/5F816B74" Ref="U?"  Part="1" 
AR Path="/5F80FF53/5F816B74" Ref="U5"  Part="1" 
F 0 "U5" H 3960 2290 50  0000 C CNN
F 1 "MCP73831-2-OT" H 3740 2170 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4150 1650 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 3950 1850 50  0001 C CNN
	1    4100 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5F816B80
P 4100 2620
AR Path="/5F816B80" Ref="#PWR?"  Part="1" 
AR Path="/5F80FF53/5F816B80" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 4100 2470 50  0001 C CNN
F 1 "GNDD" H 4104 2465 50  0000 C CNN
F 2 "" H 4100 2620 50  0001 C CNN
F 3 "" H 4100 2620 50  0001 C CNN
	1    4100 2620
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2620 4100 2340
Wire Wire Line
	4500 1800 5340 1800
$Comp
L Device:R_Small_US R?
U 1 1 5F816B9B
P 3640 2170
AR Path="/5F816B9B" Ref="R?"  Part="1" 
AR Path="/5F80FF53/5F816B9B" Ref="R12"  Part="1" 
F 0 "R12" H 3430 2220 50  0000 L CNN
F 1 "1k" H 3470 2130 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 3640 2170 50  0001 C CNN
F 3 "~" H 3640 2170 50  0001 C CNN
	1    3640 2170
	1    0    0    -1  
$EndComp
Wire Wire Line
	3640 2070 3640 2000
Wire Wire Line
	3640 2000 3700 2000
$Comp
L Device:LED_Small D?
U 1 1 5F816BA6
P 4530 1390
AR Path="/5F816BA6" Ref="D?"  Part="1" 
AR Path="/5F80FF53/5F816BA6" Ref="D2"  Part="1" 
F 0 "D2" H 4530 1277 50  0000 C CNN
F 1 "Red" H 4530 1520 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 4530 1390 50  0001 C CNN
F 3 "~" V 4530 1390 50  0001 C CNN
	1    4530 1390
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 2000 4700 2000
Wire Wire Line
	4700 2000 4700 1390
Wire Wire Line
	4700 1390 4630 1390
Wire Wire Line
	4230 1390 4100 1390
Wire Wire Line
	4100 1390 4100 1600
Connection ~ 4100 1390
Text HLabel 5610 1800 2    50   Output ~ 0
VBAT
$Comp
L Device:C_Small C14
U 1 1 5F829EA0
P 3160 1820
F 0 "C14" H 3252 1866 50  0000 L CNN
F 1 "4.7μ" H 3252 1775 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3160 1820 50  0001 C CNN
F 3 "~" H 3160 1820 50  0001 C CNN
	1    3160 1820
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C15
U 1 1 5F82E446
P 5340 2020
F 0 "C15" H 5432 2066 50  0000 L CNN
F 1 "4.7μ" H 5432 1975 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5340 2020 50  0001 C CNN
F 3 "~" H 5340 2020 50  0001 C CNN
	1    5340 2020
	1    0    0    -1  
$EndComp
Wire Wire Line
	3160 2340 3160 1920
Wire Wire Line
	3160 1720 3160 1390
Wire Wire Line
	3160 1390 4100 1390
Wire Wire Line
	5340 1920 5340 1800
Connection ~ 5340 1800
Wire Wire Line
	5340 1800 5610 1800
Text HLabel 4790 2000 2    50   Output ~ 0
CHRG_ST
Wire Wire Line
	4700 2000 4790 2000
Connection ~ 4700 2000
Text Notes 1000 2810 0    50   ~ 0
R_prog sets quick charge current such that  
Text Notes 1550 2920 0    50   ~ 0
I = 1000 V/R_prog
Text HLabel 3020 1390 0    50   Input ~ 0
VDD
Wire Wire Line
	3020 1390 3160 1390
Connection ~ 3160 1390
Wire Wire Line
	3160 2340 3640 2340
Wire Wire Line
	3640 2340 3640 2270
Wire Wire Line
	3640 2340 4100 2340
Connection ~ 3640 2340
Connection ~ 4100 2340
Wire Wire Line
	4100 2340 5340 2340
$Comp
L Device:R_Small_US R?
U 1 1 5F816B94
P 4330 1390
AR Path="/5F816B94" Ref="R?"  Part="1" 
AR Path="/5F80FF53/5F816B94" Ref="R13"  Part="1" 
F 0 "R13" V 4443 1390 50  0000 C CNN
F 1 "10k" V 4190 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4330 1390 50  0001 C CNN
F 3 "~" H 4330 1390 50  0001 C CNN
	1    4330 1390
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 2200 4100 2340
Wire Wire Line
	5340 2120 5340 2340
$EndSCHEMATC
