EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5260 4750 5360 4750
Connection ~ 5360 4750
Wire Wire Line
	5360 4750 5460 4750
$Comp
L Connector_Generic:Conn_01x06 J?
U 1 1 605151D9
P 7020 2870
F 0 "J?" V 6984 2482 50  0000 R CNN
F 1 "Conn_01x06" V 7140 3020 50  0000 R CNN
F 2 "Connector_Molex:Molex_PicoBlade_53048-0610_1x06_P1.25mm_Horizontal" H 7020 2870 50  0001 C CNN
F 3 "~" H 7020 2870 50  0001 C CNN
	1    7020 2870
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 605151DF
P 5360 2640
F 0 "#PWR?" H 5360 2490 50  0001 C CNN
F 1 "+3.3V" H 5500 2780 50  0000 C CNN
F 2 "" H 5360 2640 50  0001 C CNN
F 3 "" H 5360 2640 50  0001 C CNN
	1    5360 2640
	1    0    0    -1  
$EndComp
$Comp
L MCU_ST_STM32L4:STM32L432KCUx U?
U 1 1 605151E5
P 5360 3680
F 0 "U?" H 5090 4560 50  0000 C CNN
F 1 "STM32L432KCUx" V 5390 3330 50  0000 L BNN
F 2 "Package_DFN_QFN:QFN-32-1EP_5x5mm_P0.5mm_EP3.45x3.45mm" H 4960 2780 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00257205.pdf" H 5360 3680 50  0001 C CNN
	1    5360 3680
	1    0    0    -1  
$EndComp
Wire Wire Line
	5260 4750 5260 4680
Wire Wire Line
	5360 4680 5360 4750
Wire Wire Line
	5460 4680 5460 4750
Text GLabel 5970 3980 2    50   BiDi ~ 0
I2C_SDA
Text GLabel 5980 3780 2    35   BiDi ~ 0
~INT
Text GLabel 5970 3880 2    50   BiDi ~ 0
I2C_SCL
Wire Wire Line
	5860 3880 5970 3880
Wire Wire Line
	5860 3980 5970 3980
Wire Wire Line
	5860 3780 5980 3780
Text GLabel 4760 3380 0    35   BiDi ~ 0
BOOT
Wire Wire Line
	4860 3380 4760 3380
Text GLabel 4740 2980 0    35   BiDi ~ 0
~RST
Wire Wire Line
	4740 2980 4860 2980
Text GLabel 4760 4280 0    35   BiDi ~ 0
MOSI
Wire Wire Line
	4860 4280 4760 4280
Text GLabel 4760 4380 0    35   BiDi ~ 0
MISO
Wire Wire Line
	4860 4380 4760 4380
Text GLabel 5970 4180 2    50   BiDi ~ 0
USB_D+
Text GLabel 5970 4080 2    50   BiDi ~ 0
USB_D-
Wire Wire Line
	5860 4080 5970 4080
Wire Wire Line
	5860 4180 5970 4180
Text GLabel 4760 4080 0    35   BiDi ~ 0
SWO
Wire Wire Line
	4860 4080 4760 4080
Text GLabel 5960 4280 2    35   BiDi ~ 0
SWIO
Wire Wire Line
	5860 4280 5960 4280
Text GLabel 5960 4380 2    35   BiDi ~ 0
SWCLK
Wire Wire Line
	5860 4380 5960 4380
Text GLabel 6720 2870 0    35   BiDi ~ 0
SWCLK
Wire Wire Line
	6820 2870 6720 2870
Text GLabel 6660 3070 0    35   BiDi ~ 0
SWO
Wire Wire Line
	6820 3070 6660 3070
Text GLabel 6640 2770 0    35   BiDi ~ 0
~RST
Wire Wire Line
	6640 2770 6820 2770
$Comp
L power:GNDD #PWR?
U 1 1 6051520C
P 5360 4820
F 0 "#PWR?" H 5360 4670 50  0001 C CNN
F 1 "GNDD" H 5364 4665 50  0000 C CNN
F 2 "" H 5360 4820 50  0001 C CNN
F 3 "" H 5360 4820 50  0001 C CNN
	1    5360 4820
	1    0    0    -1  
$EndComp
Wire Wire Line
	5360 4750 5360 4820
Wire Wire Line
	5260 2710 5360 2710
Connection ~ 5360 2710
Wire Wire Line
	5360 2710 5460 2710
Wire Wire Line
	5260 2710 5260 2780
Wire Wire Line
	5360 2780 5360 2710
Wire Wire Line
	5460 2780 5460 2710
Wire Wire Line
	5360 2710 5360 2640
Text GLabel 6680 2970 0    35   BiDi ~ 0
SWIO
Wire Wire Line
	6820 2970 6680 2970
$Comp
L power:+3.3V #PWR?
U 1 1 6051521C
P 6650 2610
F 0 "#PWR?" H 6650 2460 50  0001 C CNN
F 1 "+3.3V" H 6790 2750 50  0000 C CNN
F 2 "" H 6650 2610 50  0001 C CNN
F 3 "" H 6650 2610 50  0001 C CNN
	1    6650 2610
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2670 6650 2610
$Comp
L power:GNDD #PWR?
U 1 1 60515223
P 6650 3210
F 0 "#PWR?" H 6650 3060 50  0001 C CNN
F 1 "GNDD" H 6654 3055 50  0000 C CNN
F 2 "" H 6650 3210 50  0001 C CNN
F 3 "" H 6650 3210 50  0001 C CNN
	1    6650 3210
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3210 6650 3170
Wire Wire Line
	6650 3170 6820 3170
Wire Wire Line
	6650 2670 6820 2670
Text GLabel 5960 3080 2    35   BiDi ~ 0
SCK
Wire Wire Line
	5860 3080 5960 3080
Text GLabel 5960 3180 2    35   BiDi ~ 0
SS
Wire Wire Line
	5860 3180 5960 3180
$EndSCHEMATC
