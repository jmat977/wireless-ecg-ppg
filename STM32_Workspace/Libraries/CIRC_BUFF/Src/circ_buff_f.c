/*
 * circ_buff.c
 *
 *  Created on: Sep 19, 2020
 *      Author: jakub
 */




#include "../Inc/circ_buff_f.h"

typedef struct CircBuff
{
	float *data;
	uint32_t size;
	uint32_t rd, wr;
}CircBuff_t;

CircBuff_f_t Main_buff_f = {0}, Abs_dif_long_buff = {0};;


void circbuff_f_init(CircBuff_f_t *circbuff, float *data, uint32_t size)
{
	circbuff->data = data;
	circbuff->size = size;

	circbuff->rd = 0;
	circbuff->wr = 0;
}

bool circbuff_f_write_newest(CircBuff_f_t *circbuff, float *elem)
{
	if (circbuff_f_is_full(circbuff)) {
		return false;
	}

	circbuff->data[circbuff->wr] = *elem;
	circbuff->wr = (circbuff->wr+1)%circbuff->size;

	return true;
}

bool circbuff_f_discard_newest(CircBuff_f_t *circbuff, float *elem)
{
	if (circbuff_f_is_empty(circbuff)) {
		return false;
	}

	circbuff->wr = (circbuff->wr-1)%circbuff->size;
	*elem = circbuff->data[circbuff->wr];

	return true;
}

bool circbuff_f_get_oldest(CircBuff_f_t *circbuff, float *elem)
{
	if (circbuff_f_is_empty(circbuff)) {
		return false;
	}

	*elem = circbuff->data[circbuff->rd];
	circbuff->rd = (circbuff->rd+1)%circbuff->size;

	return true;
}

bool circbuff_f_peek_nth_oldest(CircBuff_f_t *circbuff, float *elem, uint32_t n)
{
	if (circbuff_f_is_empty(circbuff)) {
		return false;
	}
	float tmp_rd = (circbuff->rd - n) % (circbuff->size);
	if(tmp_rd < (circbuff->wr) ){
		*elem = circbuff->data[circbuff->rd - n];
		return true;
	}
	else{
		return false;
	}
}

void circbuff_f_emptify(CircBuff_f_t *circbuff)
{
	circbuff->rd = circbuff->wr;
}

bool circbuff_f_is_empty(CircBuff_f_t *circbuff)
{
	return circbuff->rd == circbuff->wr;
}

bool circbuff_f_is_full(CircBuff_f_t *circbuff)
{
	return circbuff->rd == (circbuff->wr+1)%circbuff->size;
}
