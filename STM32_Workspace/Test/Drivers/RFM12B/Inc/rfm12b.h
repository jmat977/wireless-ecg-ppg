/**
 * This library was based on Arduino RFM12B library.
 * Rewritten for STM32
 *
 * Original driver from jeelabs.com (2009-02-09 <jc@wippler.nl>).
 *
 * Contact:
 * Website: http://www.srcpro.pl
 * e-mail: piotr.krzyzaniak@gmail.com
 *
 * (C) 2017-10-09 Piotr Krzyzaniak
 */
#ifndef RFM12B_H
#define RFM12B_H

#include <stdint.h>

#define BASE_ADDRESS                    0x00U
#define CONFIG_SETTING_CMD              (uint16_t)(BASE_ADDRESS + 0x00U)
#define POWER_MANAGEMENT_CMD            (uint16_t)(BASE_ADDRESS + 0x01U)
#define FREQUENCY_SETTING_CMD           (uint16_t)(BASE_ADDRESS + 0x02U)
#define DATA_RATE_CMD                   (uint16_t)(BASE_ADDRESS + 0x03U)
#define RECEIVER_CONTROL_CMD            (uint16_t)(BASE_ADDRESS + 0x04U)
#define DATA_FILTER_CMD                 (uint16_t)(BASE_ADDRESS + 0x05U)
#define FIFO_AND_RESET_MODE_CMD         (uint16_t)(BASE_ADDRESS + 0x06U)
#define SYNCHRON_PATTERN_CMD            (uint16_t)(BASE_ADDRESS + 0x07U)
#define AFC_CMD                         (uint16_t)(BASE_ADDRESS + 0x08U)
#define TX_CONFIG_CMD                   (uint16_t)(BASE_ADDRESS + 0x09U)
#define PLL_SETTING_CMD                 (uint16_t)(BASE_ADDRESS + 0x0aU)
#define WAKE_UP_TIMER_CMD               (uint16_t)(BASE_ADDRESS + 0x0bU)
#define LOW_DUTY_CYCLE_CMD              (uint16_t)(BASE_ADDRESS + 0x0cU)
#define LOW_BAT_DET_AND_UP_CLK_DIV_CMD  (uint16_t)(BASE_ADDRESS + 0x0dU)

// Configuration Setting Command
#define RFM_CS_EL_BIT                   7U //Enable internal data register
#define RFM_CS_EF_BIT                   6U //Enable FIFO
#define RFM_CS_BAND_BIT                 4U
#define RFM_CS_CAPACITOR_BIT            0U

#define RFM_CS_EL                       0x0080U
#define RFM_CS_EF                       0x0040U
#define RFM_CS_BAND                     0x0030U
#define RFM_CS_CAP                		0x000FU

#define RFM_CS_BAND_433                 0x00D0U
#define RFM_CS_BAND_868                 0x00E0U
#define RFM_CS_BAND_915                 0x00F0U

#define RFM_CS_CAP_8_5     				(uint16_t)0b0000000000000000
#define RFM_CS_CAP_9_0     				(uint16_t)0b0000000000000001
#define RFM_CS_CAP_9_5      			(uint16_t)0b0000000000000010
#define RFM_CS_CAP_10_0    				(uint16_t)0b0000000000000011
#define RFM_CS_CAP_10_5    				(uint16_t)0b0000000000000100
#define RFM_CS_CAP_11_0    				(uint16_t)0b0000000000000101
#define RFM_CS_CAP_11_5    				(uint16_t)0b0000000000000110
#define RFM_CS_CAP_12_0    				(uint16_t)0b0000000000000111
#define RFM_CS_CAP_12_5    				(uint16_t)0b0000000000001000
#define RFM_CS_CAP_13_0    				(uint16_t)0b0000000000001001
#define RFM_CS_CAP_13_5    				(uint16_t)0b0000000000001010
#define RFM_CS_CAP_14_0    				(uint16_t)0b0000000000001011
#define RFM_CS_CAP_14_5    				(uint16_t)0b0000000000001100
#define RFM_CS_CAP_15_0    				(uint16_t)0b0000000000001101
#define RFM_CS_CAP_15_5   				(uint16_t)0b0000000000001110
#define RFM_CS_CAP_16_0    				(uint16_t)0b0000000000001111


// Power Management Command
#define RFM_PM_ER_BIT                   7U
#define RFM_PM_EBB_BIT                  6U
#define RFM_PM_ET_BIT                   5U
#define RFM_PM_ES_BIT                   4U
#define RFM_PM_EX_BIT                   3U
#define RFM_PM_EB_BIT                   2U
#define RFM_PM_EW_BIT                   1U
#define RFM_PM_DC_BIT                   0U

#define RFM_PM_ENABLE_WHOLE_RX          0x0080U
#define RFM_PM_BASEBAND_ON              0x0040U
#define RFM_PM_PLL_PA_TX_ON             0x0020U
#define RFM_PM_SYMTHEZIER_ON            0x0010U
#define RFM_PM_CRYSAL_ON                0x0008U
#define RFM_PM_LOW_BATTERY_DET_ON       0x0004U
#define RFM_PM_WAKE_UP_TIM_ON           0x0002U
#define RFM_PM_DISABLE_CLK_OUT          0x0001U
#define RFM_PM_POWER_MASK               0x00FFU


// Frequency Setting Command
#define RFM_FS_F_BIT                    0U
#define RFM_FS_FRQ_MASK                 0x0FFFU


// Data Rate
#define RFM_DR_CS_BIT                   7U
#define RFM_DR_R_BIT                    0U

#define RFM_DR_CS_MASK                  0x0080U
#define RFM_DR_R_MASK                   0x007FU
#define RFM_DATA_RATE_MASK              0x00FFU


// Receiver control
#define RFM_RC_P16_BIT                  10U
#define RFM_RC_D_BIT                    8U
#define RFM_RC_I_BIT                    5U
#define RFM_RC_G_BIT                    3U
#define RFM_RC_R_BIT                    0U

#define RFM_RC_P16_MASK                 0x0400U
#define RFM_RC_D_MASK                   0x0300U
#define RFM_RC_I_MASK                   0x00E0U
#define RFM_RC_G_MASK                   0x0018U
#define RFM_RC_R_MASK                   0x0007U
#define RFM_RX_CTRL_MASK                0x07FFU

#define RFM_RC_IRQ_INPUT                0x0000U
#define RFM_RC_VDI_OUTPUT               0x0400U
#define RFM_RC_VDI_FAST                 0x0000U
#define RFM_RC_VDI_MEDIUM               0x0100U
#define RFM_RC_VDI_SLOW                 0x0200U
#define RFM_RC_VDI_ALWAYS_ON            0x0300U
#define RFM_RC_BW_400                   0x0020U
#define RFM_RC_BW_340                   0x0040U
#define RFM_RC_BW_270                   0x0060U
#define RFM_RC_BW_200                   0x0080U
#define RFM_RC_BW_134                   0x00A0U
#define RFM_RC_BW_67                    0x00C0U
#define RFM_RC_LNA_0                    0x0000U
#define RFM_RC_LNA_6                    0x0008U
#define RFM_RC_LNA_14                   0x0010U
#define RFM_RC_LNA_20                   0x0018U
#define RFM_RC_RSSI_103                 0x0000U
#define RFM_RC_RSSI_97                  0x0001U
#define RFM_RC_RSSI_91                  0x0002U
#define RFM_RC_RSSI_85                  0x0003U
#define RFM_RC_RSSI_79                  0x0004U
#define RFM_RC_RSSI_73                  0x0005U


// Data Filter
#define RFM_DF_AL_BIT                   7U
#define RFM_DF_ML_BIT                   6U
#define RFM_DF_S_BIT                    4U
#define RFM_DF_F_BIT                    0U

#define RFM_DF_AL_MASK                  0x0080U
#define RFM_DF_ML_MASK                  0x0040U
#define RFM_DF_S_MASK                   0x0010U
#define RFM_DF_F_MASK                   0x0007U

#define RFM_DF_CR_AUTO_LOCK_CTRL        0x0080U
#define RFM_DF_CR_MANUAL_LOCK_CTRL      0x0000U
#define RFM_DF_CR_FAST_LOCK_CTRL        0x0040U
#define RFM_DF_CR_SLOW_LOCK_CTRL        0x0000U
#define RFM_DF_RC_FILTER_DIGITAL        0x0000U
#define RFM_DF_RC_FILTER_ANALOG         0x0010U
#define RFM_DATA_FILTER_MASK            0x00D7U


// FIFO and Reset Mode
#define RFM_FARM_F_BIT                  4U
#define RFM_FARM_SP_BIT                 3U
#define RFM_FARM_AL_BIT                 2U
#define RFM_FARM_FF_BIT                 1U
#define RFM_FARM_DR_BIT                 0U

#define RFM_FARM_F_MASK                 0x00F0U
#define RFM_FARM_SP_MASK                0x0008U
#define RFM_FARM_AL_MASK                0x0004U
#define RFM_FARM_FF_MASK                0x0002U
#define RFM_FARM_DR_MASK                0x0001U

#define RFM_FARM_SYNC_PATTERN           0x0008U
#define RFM_FARM_ALWAYS_FILL            0x0004U
#define RFM_FARM_FILL_FIFO              0x0002U
#define RFM_FARM_DIS_SENS_RST           0x0001U
#define RFM_FARM_ENA_SENS_RST           0x0000U
#define RFM_FARM_MASK                   0x00FFU


// AFC
#define RFM_AFC_A_BIT                   6U
#define RFM_AFC_R_BIT                   4U
#define RFM_AFC_ST_BIT                  3U
#define RFM_AFC_FI_BIT                  2U
#define RFM_AFC_OE_BIT                  1U
#define RFM_AFC_EN_BIT                  0U

#define RFM_AFC_A_MASK                  0x00C0U
#define RFM_AFC_R_MASK                  0x0030U
#define RFM_AFC_ST_MASK                 0x0008U
#define RFM_AFC_FI_MASK                 0x0004U
#define RFM_AFC_OE_MASK                 0x0002U
#define RFM_AFC_EN_MASK                 0x0001U
#define RFM_AFC_MASK                    0x00FFU

#define RFM_AFC_AUTO_MODE_OFF           0x0000U
#define RFM_AFC_ON_POWER_UP             0x0040U
#define RFM_AFC_KEEP_WHEN_VDI           0x0080U
#define RFM_AFC_KEEP_WHEN_NO_VDI        0x00C0U
#define RFM_AFC_FREQ_NO_RANGE           0x0000U
#define RFM_AFC_FREQ_RANGE_15           0x0010U
#define RFM_AFC_FREQ_RANGE_7            0x0020U
#define RFM_AFC_FREQ_RANGE_3            0x0030U


// TX Configuration
#define RFM_TXC_MP_BIT                  8U
#define RFM_TXC_M_BIT                   4U
#define RFM_TXC_P_BIT                   0U

#define RFM_TXC_MP_MASK                 0x0100U
#define RFM_TXC_M_MASK                  0x00F0U
#define RFM_TXC_P_MASK                  0x0007U
#define RFM_TX_CONTROL_MASK             0x01F7U

#define RFM_TXC_POW_0dB                 0U
#define RFM_TXC_POW_2_5dB               1U
#define RFM_TXC_POW_5dB                 2U
#define RFM_TXC_POW_7_5dB               3U
#define RFM_TXC_POW_10dB                4U
#define RFM_TXC_POW_12_5d               5U
#define RFM_TXC_POW_15dB                6U
#define RFM_TXC_POW_17_5d               7U


// PLL setting
#define RFM_PLLS_OB_BIT                 5U
#define RFM_PLLS_DLY_BIT                3U
#define RFM_PLLS_DDIT_BIT               2U
#define RFM_PLLS_BW0_BIT                0U

#define RFM_PLLS_OB_MASK                0x0060U
#define RFM_PLLS_DLY_MASK               0x0008U
#define RFM_PLLS_DDIT_MASK              0x0004U
#define RFM_PLLS_BW0_MASK               0x0001U
#define RFM_PLL_SETTING_MASK            0x006DU

#define RFM_PLLS_BW0_86kbps             0U
#define RFM_PLLS_BW0_256kbps            1U


// Wake-up timer
#define RFM_WUT_R_BIT                   8U
#define RFM_WUT_M_BIT                   0U

#define RFM_WUT_R_MASK                  0x1F00U
#define RFM_WUT_M_MASK                  0x00FFU
#define RFM_WAKE_UP_TIMER_MASK          0x1FFFU


// Low Duty Cycle
#define RFM_LDC_D_BIT                   1U
#define RFM_LDC_EN_BIT                  0U

#define RFM_LDC_D_MASK                  0x00FEU
#define RFM_LDC_EN_MASK                 0x0001U


// Low Battery Detector
#define RFM_LBD_D_BIT                   5U
#define RFM_LBD_V_BIT                   0U

#define RFM_LBD_D_MASK                  0x00E0U
#define RFM_LBD_V_MASK                  0x000FU
#define RFM_LOW_BATTERY_MASK            0x00EFU


// Status Read
#define RFM_SR_RGIT_BIT                 15U
#define RFM_SR_FFIT_BIT                 15U
#define RFM_SR_POR_BIT                  14U
#define RFM_SR_RGUR_BIT                 13U
#define RFM_SR_FFOV_BIT                 13U
#define RFM_SR_WKUP_BIT                 12U
#define RFM_SR_EXT_BIT                  11U
#define RFM_SR_LBD_BIT                  10U
#define RFM_SR_FFEM_BIT                 9U
#define RFM_SR_ATS_BIT                  8U
#define RFM_SR_RSSI_BIT                 8U
#define RFM_SR_DQD_BIT                  7U
#define RFM_SR_CRL_BIT                  6U
#define RFM_SR_ATGL_BIT                 5U
#define RFM_SR_OFFS6_BIT                4U
#define RFM_SR_OFFS3_0_BIT              0U

#define RFM_SR_RGIT_MASK                0x8000U
#define RFM_SR_FFIT_MASK                0x8000U
#define RFM_SR_POR_MASK                 0x4000U
#define RFM_SR_RGUR_MASK                0x2000U
#define RFM_SR_FFOV_MASK                0x2000U
#define RFM_SR_WKUP_MASK                0x1000U
#define RFM_SR_EXT_MASK                 0x0800U
#define RFM_SR_LBD_MASK                 0x0400U
#define RFM_SR_FFEM_MASK                0x0200U
#define RFM_SR_ATS_MASK                 0x0100U
#define RFM_SR_RSSI_MASK                0x0100U
#define RFM_SR_DQD_MASK                 0x0080U
#define RFM_SR_CRL_MASK                 0x0040U
#define RFM_SR_ATGL_MASK                0x0020U
#define RFM_SR_OFFS6_MASK               0x0010U
#define RFM_SR_OFFS3_0_MASK             0x000FU


#define FREQ_BAND_433                   1U
#define FREQ_BAND_868                   2U
#define FREQ_BAND_915                   3U

#define RFM_PREAMBLE_BYTE_0             0xD4U
#define RFM_PREAMBLE_BYTE_1             0x2DU

#define PREAMBLE_FIELD                  0U
#define DEV_ADDR_FIELD                  1U
#define SIZE_FIELD                      2U
#define HEADER_LEN                      3

//#define ENABLE                          1
//#define DISABLE                         0x0

#define RX_FIFO_READ_MASK				0xB000U

#define RFM_CS_BAND_opt						RFM_CS_BAND_868

#define SW_RESET (uint16_t)0b1111111000000000
//#ifndef _GLOBAL_H_
//  #include <stm32f030x6.h>
//  #define PORT_CE         GPIOB->ODR
//  #define nSEL            6
//  #define FSK             7
//#endif




// setup
void rfm_config(void);
void rfm_initRxMode(void);
void rfm_initTxMode(void);

// register update
void rfm_setFrequency(uint16_t frequency);
void rfm_setBand(uint16_t band);
void rfm_enableFIFOmode(void);
void rfm_selectCapacitor(uint16_t capacitor);
void rfm_powerManager(uint16_t settings);
void rfm_rxControl(uint16_t settings);
void rfm_dataFilter(uint16_t settings);
void rfm_fifoAndResetMode(uint16_t settings);
void rfm_setSynchronPattern(uint8_t pattern);
void rfm_afc(uint16_t settings);
void rfm_pllSetting(uint16_t settings);
void rfm_wakeUpTimer(uint8_t r, uint8_t m);
void rfm_lowDutyCycle(uint8_t duty);
void rfm_dataRate(uint8_t r, uint8_t cs);
void rfm_txConfig(uint8_t mp, uint8_t modulation, uint8_t power);
void rfm_lowBatteryDetector(uint8_t divider, uint8_t voltage);
void rfm_software_reset(void);

// turn on/off
void rfm_enableReceiveChain(uint8_t enable);
void rfm_baseband(uint8_t enable);
void rfm_transmisionChain(uint8_t enable);
void rfm_synthesizer(uint8_t enable);
void rfm_crystalOscilator(uint8_t enable);
void rfm_activateLowBatteryDetector(uint8_t enable);
void rfm_activateWakeUpTimer(uint8_t enable);
void rfm_clkOutputPin(uint8_t enable);
void rfm_offsetCalculation(uint8_t enable);

// utils
void rfm_clearFIFO(void);
void rfm_sleep(void);
void rfm_standby(void);
void rfm_setWakeUpTimer(void);
void rfm_setLNAGain(uint16_t gain);
uint16_t rfm_readStatus(void);

// low level commands
void rfm_waitForReady(void);
void rfm_writeReg(uint16_t reg, uint16_t mask, uint16_t value);
void rfm_writeDataByte(uint8_t *buffer, uint8_t size);
uint8_t rfm_readDatabyte(void);
uint16_t rfm_transmitSPI(uint16_t value);
uint8_t rfm_crc(uint8_t *buffer, uint8_t size);

// general send & receive commands
void rfm_sendPayload(uint8_t* buffer, uint8_t size);
void rfm_receivePayload(uint8_t* rxBuffer, uint8_t sizeLimit);

#endif

