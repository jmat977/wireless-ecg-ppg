opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 10920"

opt pagewidth 120

	opt pm

	processor	16F1519
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
indf1	equ	1
pc	equ	2
pcl	equ	2
status	equ	3
fsr0l	equ	4
fsr0h	equ	5
fsr1l	equ	6
fsr1h	equ	7
bsr	equ	8
wreg	equ	9
intcon	equ	11
c	equ	1
z	equ	0
pclath	equ	10
# 47 "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 47 "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	dw 0xFFFF&0xFFFA&0xFFE7&0xFFDF&0xFFBF&0xFF7F&0xF9FF&0xFFFF&0xEFFF&0xDFFF ;#
# 48 "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 48 "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	dw 0xFFFF&0xFFFF&0xFFFF&0xFFFF&0xFFFF&0xDFFF ;#
	FNCALL	_main,_InitSfr
	FNCALL	_main,_InitPort
	FNCALL	_main,_RFM12B_Config
	FNCALL	_main,_KeyScan
	FNCALL	_main,_TxLedFlash
	FNCALL	_main,_RFM12B_TxPacket
	FNCALL	_main,_RFM12B_RxPacket
	FNCALL	_main,_BeepOn
	FNCALL	_main,_BeepOff
	FNCALL	_main,_RFM12B_EntryTx
	FNCALL	_main,_TxLedOff
	FNCALL	_main,_RFM12B_EntryRx
	FNCALL	_RFM12B_EntryRx,_RFM12B_Config
	FNCALL	_RFM12B_EntryRx,_SPIWrite
	FNCALL	_RFM12B_EntryTx,_RFM12B_Config
	FNCALL	_RFM12B_EntryTx,_SPIWrite
	FNCALL	_RFM12B_RxPacket,_RFM12B_RxByte
	FNCALL	_RFM12B_RxPacket,_RFM12B_ClearFIFO
	FNCALL	_RFM12B_TxPacket,_RFM12B_Standby
	FNCALL	_RFM12B_TxPacket,_RFM12B_TxByte
	FNCALL	_RFM12B_TxByte,_SPIWrite
	FNCALL	_RFM12B_Standby,_SPIWrite
	FNCALL	_RFM12B_ClearFIFO,_SPIWrite
	FNCALL	_RFM12B_Config,_SPIWrite
	FNCALL	_KeyScan,_ReadKey
	FNROOT	_main
	FNCALL	intlevel1,_ISR
	global	intlevel1
	FNROOT	intlevel1
	global	_RFM12BConfigTbl
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
	global    __stringtab
__stringtab:
	retlw	0
psect	strings
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	17
_RFM12BConfigTbl:
	retlw	0D8h
	retlw	082h

	retlw	0A0h
	retlw	094h

	retlw	0ACh
	retlw	0C2h

	retlw	080h
	retlw	0CAh

	retlw	0D4h
	retlw	0CEh

	retlw	083h
	retlw	0CAh

	retlw	09Bh
	retlw	0C4h

	retlw	077h
	retlw	0CCh

	retlw	0
	retlw	0E0h

	retlw	0Eh
	retlw	0C8h

	retlw	091h
	retlw	0C6h

	retlw	010h
	retlw	098h

	retlw	060h
	retlw	0C0h

	global	_RFM12BData
psect	strings
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	33
_RFM12BData:
	retlw	048h
	retlw	06Fh
	retlw	070h
	retlw	065h
	retlw	052h
	retlw	046h
	retlw	020h
	retlw	052h
	retlw	046h
	retlw	04Dh
	retlw	020h
	retlw	043h
	retlw	04Fh
	retlw	042h
	retlw	052h
	retlw	046h
	retlw	04Dh
	retlw	031h
	retlw	032h
	retlw	042h
	retlw	053h
	retlw	0
	global	_RFM12BFreqTbl
psect	strings
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	10
_RFM12BFreqTbl:
	retlw	0
	retlw	0

	retlw	0
	retlw	0

	retlw	0DDh
	retlw	080h

	retlw	040h
	retlw	0A6h

	retlw	0EDh
	retlw	080h

	retlw	040h
	retlw	0A6h

	retlw	0FDh
	retlw	080h

	retlw	0D0h
	retlw	0A7h

	global	_RFM12BConfigTbl
	global	_RFM12BData
	global	_RFM12BFreqTbl
	global	_RxData
	global	_RxLimtTime
	global	_TxLedTime
	global	_KeyTime
	global	_TxLedCnt
	global	__Flag1
	global	__KeyFlag
	global	__SysTime
	global	_gb_Count
	global	_PIR1
_PIR1	set	17
	global	_PIR2
_PIR2	set	18
	global	_PORTA
_PORTA	set	12
	global	_PORTB
_PORTB	set	13
	global	_PORTC
_PORTC	set	14
	global	_PORTD
_PORTD	set	15
	global	_PORTE
_PORTE	set	16
	global	_PR2
_PR2	set	27
	global	_T1CON
_T1CON	set	24
	global	_T1GCON
_T1GCON	set	25
	global	_T2CON
_T2CON	set	28
	global	_TMR1H
_TMR1H	set	23
	global	_TMR1L
_TMR1L	set	22
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_RB5
_RB5	set	109
	global	_RC5
_RC5	set	117
	global	_TMR0IE
_TMR0IE	set	93
	global	_TMR0IF
_TMR0IF	set	90
	global	_TMR1IF
_TMR1IF	set	136
	global	_TMR2ON
_TMR2ON	set	226
	global	_ADCON0
_ADCON0	set	157
	global	_ADCON1
_ADCON1	set	158
	global	_OPTION_REG
_OPTION_REG	set	149
	global	_OSCCON
_OSCCON	set	153
	global	_PIE1
_PIE1	set	145
	global	_PIE2
_PIE2	set	146
	global	_TRISA
_TRISA	set	140
	global	_TRISB
_TRISB	set	141
	global	_TRISC
_TRISC	set	142
	global	_TRISD
_TRISD	set	143
	global	_TRISE
_TRISE	set	144
	global	_WDTCON
_WDTCON	set	151
	global	_TMR1IE
_TMR1IE	set	1160
	global	_TRISA2
_TRISA2	set	1122
	global	_APFCON
_APFCON	set	285
	global	_BORCON
_BORCON	set	278
	global	_FVRCON
_FVRCON	set	279
	global	_LATA
_LATA	set	268
	global	_LATB
_LATB	set	269
	global	_LATC
_LATC	set	270
	global	_LATD
_LATD	set	271
	global	_LATE
_LATE	set	272
	global	_LATA0
_LATA0	set	2144
	global	_LATA1
_LATA1	set	2145
	global	_LATC2
_LATC2	set	2162
	global	_LATC3
_LATC3	set	2163
	global	_LATC4
_LATC4	set	2164
	global	_LATD1
_LATD1	set	2169
	global	_LATD2
_LATD2	set	2170
	global	_LATD3
_LATD3	set	2171
	global	_LATD4
_LATD4	set	2172
	global	_LATD5
_LATD5	set	2173
	global	_LATD6
_LATD6	set	2174
	global	_LATD7
_LATD7	set	2175
	global	_ANSELA
_ANSELA	set	396
	global	_ANSELB
_ANSELB	set	397
	global	_ANSELC
_ANSELC	set	398
	global	_ANSELD
_ANSELD	set	399
	global	_ANSELE
_ANSELE	set	400
	global	_SSPCON1
_SSPCON1	set	533
	global	_SSPCON2
_SSPCON2	set	534
	global	_SSPCON3
_SSPCON3	set	535
	global	_WPUB
_WPUB	set	525
	global	_WPUE
_WPUE	set	528
	global	_CCP1CON
_CCP1CON	set	659
	global	_CCP2CON
_CCP2CON	set	666
	global	_CCPR1L
_CCPR1L	set	657
	global	_IOCBF
_IOCBF	set	918
	global	_IOCBN
_IOCBN	set	917
	global	_IOCBP
_IOCBP	set	916
	file	"RFDK_RFM12B.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_KeyTime:
       ds      1

_TxLedCnt:
       ds      1

__Flag1:
       ds      1

__KeyFlag:
       ds      1

__SysTime:
       ds      1

_gb_Count:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_RxData:
       ds      32

_RxLimtTime:
       ds      1

_TxLedTime:
       ds      1

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR0 containing the base address, and
;	WREG with the size to clear
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf0		;clear RAM location pointed to by FSR
	addfsr	0,1
	decfsz wreg		;Have we reached the end of clearing yet?
	goto clrloop	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	global __pbssCOMMON
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	global __pbssBANK0
	movlw	low(__pbssBANK0)
	movwf	fsr0l
	movlw	high(__pbssBANK0)
	movwf	fsr0h
	movlw	022h
	fcall	clear_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
movlb 0
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	??_ReadKey
??_ReadKey:	; 0 bytes @ 0x0
	global	?_InitSfr
?_InitSfr:	; 0 bytes @ 0x0
	global	??_InitSfr
??_InitSfr:	; 0 bytes @ 0x0
	global	?_InitPort
?_InitPort:	; 0 bytes @ 0x0
	global	??_InitPort
??_InitPort:	; 0 bytes @ 0x0
	global	?_RFM12B_Config
?_RFM12B_Config:	; 0 bytes @ 0x0
	global	?_TxLedFlash
?_TxLedFlash:	; 0 bytes @ 0x0
	global	??_TxLedFlash
??_TxLedFlash:	; 0 bytes @ 0x0
	global	?_RFM12B_TxPacket
?_RFM12B_TxPacket:	; 0 bytes @ 0x0
	global	?_TxLedOff
?_TxLedOff:	; 0 bytes @ 0x0
	global	??_TxLedOff
??_TxLedOff:	; 0 bytes @ 0x0
	global	?_SPIWrite
?_SPIWrite:	; 0 bytes @ 0x0
	global	?_BeepOn
?_BeepOn:	; 0 bytes @ 0x0
	global	??_BeepOn
??_BeepOn:	; 0 bytes @ 0x0
	global	?_BeepOff
?_BeepOff:	; 0 bytes @ 0x0
	global	??_BeepOff
??_BeepOff:	; 0 bytes @ 0x0
	global	?_ISR
?_ISR:	; 0 bytes @ 0x0
	global	??_ISR
??_ISR:	; 0 bytes @ 0x0
	global	?_KeyScan
?_KeyScan:	; 0 bytes @ 0x0
	global	??_KeyScan
??_KeyScan:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?_RFM12B_ClearFIFO
?_RFM12B_ClearFIFO:	; 0 bytes @ 0x0
	global	?_RFM12B_Standby
?_RFM12B_Standby:	; 0 bytes @ 0x0
	global	??_RFM12B_RxByte
??_RFM12B_RxByte:	; 0 bytes @ 0x0
	global	?_ReadKey
?_ReadKey:	; 1 bytes @ 0x0
	global	?_RFM12B_RxPacket
?_RFM12B_RxPacket:	; 1 bytes @ 0x0
	global	?_RFM12B_EntryTx
?_RFM12B_EntryTx:	; 1 bytes @ 0x0
	global	?_RFM12B_EntryRx
?_RFM12B_EntryRx:	; 1 bytes @ 0x0
	global	?_RFM12B_TxByte
?_RFM12B_TxByte:	; 1 bytes @ 0x0
	global	?_RFM12B_RxByte
?_RFM12B_RxByte:	; 1 bytes @ 0x0
	global	RFM12B_RxByte@Result
RFM12B_RxByte@Result:	; 1 bytes @ 0x0
	global	SPIWrite@WrPara
SPIWrite@WrPara:	; 2 bytes @ 0x0
	ds	1
	global	RFM12B_RxByte@i
RFM12B_RxByte@i:	; 1 bytes @ 0x1
	ds	1
	global	??_SPIWrite
??_SPIWrite:	; 0 bytes @ 0x2
	global	SPIWrite@bitcnt
SPIWrite@bitcnt:	; 1 bytes @ 0x2
	ds	1
	global	??_RFM12B_Config
??_RFM12B_Config:	; 0 bytes @ 0x3
	global	??_RFM12B_RxPacket
??_RFM12B_RxPacket:	; 0 bytes @ 0x3
	global	??_RFM12B_ClearFIFO
??_RFM12B_ClearFIFO:	; 0 bytes @ 0x3
	global	??_RFM12B_Standby
??_RFM12B_Standby:	; 0 bytes @ 0x3
	global	??_RFM12B_TxByte
??_RFM12B_TxByte:	; 0 bytes @ 0x3
	global	RFM12B_Config@i
RFM12B_Config@i:	; 1 bytes @ 0x3
	global	RFM12B_TxByte@dat
RFM12B_TxByte@dat:	; 1 bytes @ 0x3
	ds	1
	global	??_RFM12B_EntryTx
??_RFM12B_EntryTx:	; 0 bytes @ 0x4
	global	??_RFM12B_EntryRx
??_RFM12B_EntryRx:	; 0 bytes @ 0x4
	global	RFM12B_TxByte@temp
RFM12B_TxByte@temp:	; 2 bytes @ 0x4
	ds	1
	global	RFM12B_RxPacket@i
RFM12B_RxPacket@i:	; 1 bytes @ 0x5
	ds	1
	global	RFM12B_TxByte@RGIT
RFM12B_TxByte@RGIT:	; 1 bytes @ 0x6
	ds	1
	global	??_RFM12B_TxPacket
??_RFM12B_TxPacket:	; 0 bytes @ 0x7
	global	RFM12B_TxPacket@i
RFM12B_TxPacket@i:	; 1 bytes @ 0x7
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x8
;;Data sizes: Strings 0, constant 64, data 0, bss 40, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      8      14
;; BANK0           80      0      34
;; BANK1           80      0       0
;; BANK2           80      0       0
;; BANK3           80      0       0
;; BANK4           80      0       0
;; BANK5           80      0       0
;; BANK6           80      0       0
;; BANK7           80      0       0
;; BANK8           80      0       0
;; BANK9           80      0       0
;; BANK10          80      0       0
;; BANK11          80      0       0
;; BANK12          48      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_RFM12B_TxPacket
;;   _RFM12B_EntryRx->_RFM12B_Config
;;   _RFM12B_EntryTx->_RFM12B_Config
;;   _RFM12B_TxPacket->_RFM12B_TxByte
;;   _RFM12B_TxByte->_SPIWrite
;;   _RFM12B_Standby->_SPIWrite
;;   _RFM12B_ClearFIFO->_SPIWrite
;;   _RFM12B_Config->_SPIWrite
;;
;; Critical Paths under _ISR in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK2
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK4
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK4
;;
;;   None.
;;
;; Critical Paths under _main in BANK5
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK5
;;
;;   None.
;;
;; Critical Paths under _main in BANK6
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK6
;;
;;   None.
;;
;; Critical Paths under _main in BANK7
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK7
;;
;;   None.
;;
;; Critical Paths under _main in BANK8
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK8
;;
;;   None.
;;
;; Critical Paths under _main in BANK9
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK9
;;
;;   None.
;;
;; Critical Paths under _main in BANK10
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK10
;;
;;   None.
;;
;; Critical Paths under _main in BANK11
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK11
;;
;;   None.
;;
;; Critical Paths under _main in BANK12
;;
;;   None.
;;
;; Critical Paths under _ISR in BANK12
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 0     0      0    1476
;;                            _InitSfr
;;                           _InitPort
;;                      _RFM12B_Config
;;                            _KeyScan
;;                         _TxLedFlash
;;                    _RFM12B_TxPacket
;;                    _RFM12B_RxPacket
;;                             _BeepOn
;;                            _BeepOff
;;                     _RFM12B_EntryTx
;;                           _TxLedOff
;;                     _RFM12B_EntryRx
;; ---------------------------------------------------------------------------------
;; (1) _RFM12B_EntryRx                                       0     0      0     276
;;                      _RFM12B_Config
;;                           _SPIWrite
;; ---------------------------------------------------------------------------------
;; (1) _RFM12B_EntryTx                                       0     0      0     276
;;                      _RFM12B_Config
;;                           _SPIWrite
;; ---------------------------------------------------------------------------------
;; (1) _RFM12B_RxPacket                                      3     3      0     392
;;                                              3 COMMON     3     3      0
;;                      _RFM12B_RxByte
;;                   _RFM12B_ClearFIFO
;; ---------------------------------------------------------------------------------
;; (1) _RFM12B_TxPacket                                      1     1      0     326
;;                                              7 COMMON     1     1      0
;;                     _RFM12B_Standby
;;                      _RFM12B_TxByte
;; ---------------------------------------------------------------------------------
;; (2) _RFM12B_RxByte                                        2     2      0     117
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (2) _RFM12B_TxByte                                        4     4      0     142
;;                                              3 COMMON     4     4      0
;;                           _SPIWrite
;; ---------------------------------------------------------------------------------
;; (2) _RFM12B_Standby                                       0     0      0      70
;;                           _SPIWrite
;; ---------------------------------------------------------------------------------
;; (2) _RFM12B_ClearFIFO                                     0     0      0      70
;;                           _SPIWrite
;; ---------------------------------------------------------------------------------
;; (2) _RFM12B_Config                                        1     1      0     206
;;                                              3 COMMON     1     1      0
;;                           _SPIWrite
;; ---------------------------------------------------------------------------------
;; (1) _KeyScan                                              0     0      0       0
;;                            _ReadKey
;; ---------------------------------------------------------------------------------
;; (2) _SPIWrite                                             3     1      2      70
;;                                              0 COMMON     3     1      2
;; ---------------------------------------------------------------------------------
;; (1) _TxLedOff                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _TxLedFlash                                           0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _InitPort                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _InitSfr                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; (2) _ReadKey                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _BeepOff                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _BeepOn                                               0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (4) _ISR                                                  0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _InitSfr
;;   _InitPort
;;   _RFM12B_Config
;;     _SPIWrite
;;   _KeyScan
;;     _ReadKey
;;   _TxLedFlash
;;   _RFM12B_TxPacket
;;     _RFM12B_Standby
;;       _SPIWrite
;;     _RFM12B_TxByte
;;       _SPIWrite
;;   _RFM12B_RxPacket
;;     _RFM12B_RxByte
;;     _RFM12B_ClearFIFO
;;       _SPIWrite
;;   _BeepOn
;;   _BeepOff
;;   _RFM12B_EntryTx
;;     _RFM12B_Config
;;       _SPIWrite
;;     _SPIWrite
;;   _TxLedOff
;;   _RFM12B_EntryRx
;;     _RFM12B_Config
;;       _SPIWrite
;;     _SPIWrite
;;
;; _ISR (ROOT)
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BIGRAM             3F0      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;BITCOMMON            E      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;COMMON               E      8       E       2      100.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR2              0      0       0       3        0.0%
;;SFR2                 0      0       0       3        0.0%
;;STACK                0      0       3       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0      22       5       42.5%
;;BITSFR4              0      0       0       5        0.0%
;;SFR4                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BITSFR5              0      0       0       6        0.0%
;;SFR5                 0      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITSFR6              0      0       0       7        0.0%
;;SFR6                 0      0       0       7        0.0%
;;BITBANK2            50      0       0       8        0.0%
;;BITSFR7              0      0       0       8        0.0%
;;SFR7                 0      0       0       8        0.0%
;;BANK2               50      0       0       9        0.0%
;;BITSFR8              0      0       0       9        0.0%
;;SFR8                 0      0       0       9        0.0%
;;BITBANK3            50      0       0      10        0.0%
;;BITSFR9              0      0       0      10        0.0%
;;SFR9                 0      0       0      10        0.0%
;;BANK3               50      0       0      11        0.0%
;;BITSFR10             0      0       0      11        0.0%
;;SFR10                0      0       0      11        0.0%
;;BITBANK4            50      0       0      12        0.0%
;;BITSFR11             0      0       0      12        0.0%
;;SFR11                0      0       0      12        0.0%
;;BANK4               50      0       0      13        0.0%
;;BITSFR12             0      0       0      13        0.0%
;;SFR12                0      0       0      13        0.0%
;;BITBANK5            50      0       0      14        0.0%
;;BITSFR13             0      0       0      14        0.0%
;;SFR13                0      0       0      14        0.0%
;;BANK5               50      0       0      15        0.0%
;;BITSFR14             0      0       0      15        0.0%
;;SFR14                0      0       0      15        0.0%
;;BITBANK6            50      0       0      16        0.0%
;;BITSFR15             0      0       0      16        0.0%
;;SFR15                0      0       0      16        0.0%
;;BANK6               50      0       0      17        0.0%
;;BITSFR16             0      0       0      17        0.0%
;;SFR16                0      0       0      17        0.0%
;;BITBANK7            50      0       0      18        0.0%
;;BITSFR17             0      0       0      18        0.0%
;;SFR17                0      0       0      18        0.0%
;;BANK7               50      0       0      19        0.0%
;;BITSFR18             0      0       0      19        0.0%
;;SFR18                0      0       0      19        0.0%
;;BITSFR19             0      0       0      20        0.0%
;;SFR19                0      0       0      20        0.0%
;;ABS                  0      0      30      20        0.0%
;;BITBANK8            50      0       0      21        0.0%
;;BITSFR20             0      0       0      21        0.0%
;;SFR20                0      0       0      21        0.0%
;;BANK8               50      0       0      22        0.0%
;;BITSFR21             0      0       0      22        0.0%
;;SFR21                0      0       0      22        0.0%
;;BITBANK9            50      0       0      23        0.0%
;;BITSFR22             0      0       0      23        0.0%
;;SFR22                0      0       0      23        0.0%
;;BANK9               50      0       0      24        0.0%
;;BITSFR23             0      0       0      24        0.0%
;;SFR23                0      0       0      24        0.0%
;;BITBANK10           50      0       0      25        0.0%
;;BITSFR24             0      0       0      25        0.0%
;;SFR24                0      0       0      25        0.0%
;;BANK10              50      0       0      26        0.0%
;;BITSFR25             0      0       0      26        0.0%
;;SFR25                0      0       0      26        0.0%
;;BITBANK11           50      0       0      27        0.0%
;;BITSFR26             0      0       0      27        0.0%
;;SFR26                0      0       0      27        0.0%
;;BANK11              50      0       0      28        0.0%
;;BITSFR27             0      0       0      28        0.0%
;;SFR27                0      0       0      28        0.0%
;;BITBANK12           30      0       0      29        0.0%
;;BITSFR28             0      0       0      29        0.0%
;;SFR28                0      0       0      29        0.0%
;;BANK12              30      0       0      30        0.0%
;;BITSFR29             0      0       0      30        0.0%
;;SFR29                0      0       0      30        0.0%
;;BITSFR30             0      0       0      31        0.0%
;;SFR30                0      0       0      31        0.0%
;;DATA                 0      0      33      31        0.0%
;;BITSFR31             0      0       0      32        0.0%
;;SFR31                0      0       0      32        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 51 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 1D/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_InitSfr
;;		_InitPort
;;		_RFM12B_Config
;;		_KeyScan
;;		_TxLedFlash
;;		_RFM12B_TxPacket
;;		_RFM12B_RxPacket
;;		_BeepOn
;;		_BeepOff
;;		_RFM12B_EntryTx
;;		_TxLedOff
;;		_RFM12B_EntryRx
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	line	51
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 12
; Regs used in _main: [wreg-status,0+pclath+cstack]
	line	52
	
l9053:	
;main.c: 52: InitSfr();
	fcall	_InitSfr
	line	53
;main.c: 53: InitPort();
	fcall	_InitPort
	line	55
	
l9055:	
;main.c: 55: for(_SysTime.BByte=0;_SysTime.BByte<16;);
	clrf	(__SysTime)
	
l9057:	
	movlw	(010h)
	subwf	(__SysTime),w
	skipc
	goto	u691
	goto	u690
u691:
	goto	l9057
u690:
	line	57
	
l9059:	
;main.c: 57: RFM12B_Config();
	fcall	_RFM12B_Config
	line	58
	
l9061:	
;main.c: 58: _Flag1.BBits.Bit1 = 0;
	bcf	(__Flag1),1
	line	59
	
l9063:	
;main.c: 59: _Flag1.BBits.Bit0 = 0;
	bcf	(__Flag1),0
	line	62
	
l9065:	
;main.c: 61: {
;main.c: 62: KeyScan();
	fcall	_KeyScan
	line	64
	
l9067:	
;main.c: 64: if(_Flag1.BBits.Bit0)
	btfss	(__Flag1),0
	goto	u701
	goto	u700
u701:
	goto	l4483
u700:
	line	66
	
l9069:	
;main.c: 65: {
;main.c: 66: TxLedFlash();
	fcall	_TxLedFlash
	line	67
	
l9071:	
;main.c: 67: RFM12B_TxPacket();
	fcall	_RFM12B_TxPacket
	line	68
;main.c: 68: }
	goto	l9065
	line	69
	
l4483:	
;main.c: 69: else if(_Flag1.BBits.Bit1)
	btfss	(__Flag1),1
	goto	u711
	goto	u710
u711:
	goto	l4485
u710:
	line	71
	
l9073:	
;main.c: 70: {
;main.c: 71: if(RFM12B_RxPacket())
	fcall	_RFM12B_RxPacket
	xorlw	0&0ffh
	skipnz
	goto	u721
	goto	u720
u721:
	goto	l9079
u720:
	line	73
	
l9075:	
;main.c: 72: {
;main.c: 73: RxLimtTime = 0;
	movlb 0	; select bank0
	clrf	(_RxLimtTime)
	line	74
	
l9077:	
;main.c: 74: BeepOn();
	fcall	_BeepOn
	line	76
	
l9079:	
;main.c: 75: }
;main.c: 76: if(RxLimtTime>=15)
	movlw	(0Fh)
	movlb 0	; select bank0
	subwf	(_RxLimtTime),w
	skipc
	goto	u731
	goto	u730
u731:
	goto	l9083
u730:
	line	77
	
l9081:	
;main.c: 77: BeepOff();
	fcall	_BeepOff
	line	79
	
l9083:	
;main.c: 79: if(_KeyFlag.BBits.Bit6)
	btfss	(__KeyFlag),6
	goto	u741
	goto	u740
u741:
	goto	l4484
u740:
	line	81
	
l9085:	
;main.c: 80: {
;main.c: 81: BeepOff();
	fcall	_BeepOff
	line	82
	
l9087:	
;main.c: 82: RFM12B_EntryTx();
	fcall	_RFM12B_EntryTx
	line	83
	
l9089:	
;main.c: 83: _Flag1.BBits.Bit1 = 0;
	bcf	(__Flag1),1
	line	84
	
l9091:	
;main.c: 84: LATD7 = 0;
	bcf	(2175/8)^0100h,(2175)&7
	line	85
	
l9093:	
;main.c: 85: _Flag1.BBits.Bit0 = 1;
	bsf	(__Flag1),0
	line	86
	
l9095:	
;main.c: 86: TxLedCnt = 0;
	clrf	(_TxLedCnt)
	line	87
	
l9097:	
;main.c: 87: TxLedTime = 0;
	movlb 0	; select bank0
	clrf	(_TxLedTime)
	goto	l9065
	line	90
	
l4485:	
	line	92
;main.c: 90: else
;main.c: 91: {
;main.c: 92: _Flag1.BBits.Bit1 = 1;
	bsf	(__Flag1),1
	line	93
;main.c: 93: LATD7 = 1;
	movlb 2	; select bank2
	bsf	(2175/8)^0100h,(2175)&7
	line	94
;main.c: 94: _Flag1.BBits.Bit0 = 0;
	bcf	(__Flag1),0
	line	95
	
l9099:	
;main.c: 95: TxLedOff();
	fcall	_TxLedOff
	line	96
	
l9101:	
;main.c: 96: RFM12B_EntryRx();
	fcall	_RFM12B_EntryRx
	goto	l9065
	line	97
	
l4484:	
	goto	l9065
	global	start
	ljmp	start
	opt stack 0
psect	maintext
	line	99
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_RFM12B_EntryRx
psect	text463,local,class=CODE,delta=2
global __ptext463
__ptext463:

;; *************** function _RFM12B_EntryRx *****************
;; Defined at:
;;		line 58 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/2
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_RFM12B_Config
;;		_SPIWrite
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text463
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	58
	global	__size_of_RFM12B_EntryRx
	__size_of_RFM12B_EntryRx	equ	__end_of_RFM12B_EntryRx-_RFM12B_EntryRx
	
_RFM12B_EntryRx:	
	opt	stack 12
; Regs used in _RFM12B_EntryRx: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	59
	
l9045:	
;RFM12B.c: 59: TRISA2=0;
	movlb 1	; select bank1
	bcf	(1122/8)^080h,(1122)&7
	line	60
;RFM12B.c: 60: LATA1=1;
	movlb 2	; select bank2
	bsf	(2145/8)^0100h,(2145)&7
	line	61
	
l9047:	
;RFM12B.c: 61: RFM12B_Config();
	fcall	_RFM12B_Config
	line	63
	
l9049:	
;RFM12B.c: 63: SPIWrite(0x82D8);
	movlw	low(082D8h)
	movwf	(?_SPIWrite)
	movlw	high(082D8h)
	movwf	((?_SPIWrite))+1
	fcall	_SPIWrite
	line	65
	
l5643:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_EntryRx
	__end_of_RFM12B_EntryRx:
;; =============== function _RFM12B_EntryRx ends ============

	signat	_RFM12B_EntryRx,89
	global	_RFM12B_EntryTx
psect	text464,local,class=CODE,delta=2
global __ptext464
__ptext464:

;; *************** function _RFM12B_EntryTx *****************
;; Defined at:
;;		line 74 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/2
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_RFM12B_Config
;;		_SPIWrite
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text464
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	74
	global	__size_of_RFM12B_EntryTx
	__size_of_RFM12B_EntryTx	equ	__end_of_RFM12B_EntryTx-_RFM12B_EntryTx
	
_RFM12B_EntryTx:	
	opt	stack 12
; Regs used in _RFM12B_EntryTx: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	75
	
l9037:	
;RFM12B.c: 75: TRISA2=0;
	movlb 1	; select bank1
	bcf	(1122/8)^080h,(1122)&7
	line	76
;RFM12B.c: 76: LATA1=1;
	movlb 2	; select bank2
	bsf	(2145/8)^0100h,(2145)&7
	line	77
	
l9039:	
;RFM12B.c: 77: RFM12B_Config();
	fcall	_RFM12B_Config
	line	79
	
l9041:	
;RFM12B.c: 79: SPIWrite(0x8238);
	movlw	low(08238h)
	movwf	(?_SPIWrite)
	movlw	high(08238h)
	movwf	((?_SPIWrite))+1
	fcall	_SPIWrite
	line	81
	
l5646:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_EntryTx
	__end_of_RFM12B_EntryTx:
;; =============== function _RFM12B_EntryTx ends ============

	signat	_RFM12B_EntryTx,89
	global	_RFM12B_RxPacket
psect	text465,local,class=CODE,delta=2
global __ptext465
__ptext465:

;; *************** function _RFM12B_RxPacket *****************
;; Defined at:
;;		line 206 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    5[COMMON] unsigned char 
;;  ret             1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr1l, fsr1h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1D/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         2       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         3       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_RFM12B_RxByte
;;		_RFM12B_ClearFIFO
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text465
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	206
	global	__size_of_RFM12B_RxPacket
	__size_of_RFM12B_RxPacket	equ	__end_of_RFM12B_RxPacket-_RFM12B_RxPacket
	
_RFM12B_RxPacket:	
	opt	stack 12
; Regs used in _RFM12B_RxPacket: [wreg+fsr1l-status,0+pclath+cstack]
	line	208
	
l8989:	
	line	210
	
l8991:	
;RFM12B.c: 210: if(!RB5)
	btfsc	(109/8),(109)&7
	goto	u631
	goto	u630
u631:
	goto	l9033
u630:
	line	212
	
l8993:	
;RFM12B.c: 211: {
;RFM12B.c: 212: RxData[gb_Count++]=RFM12B_RxByte();
	movf	(_gb_Count),w
	addlw	_RxData&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	fcall	_RFM12B_RxByte
	movwf	indf1
	
l8995:	
	incf	(_gb_Count),f
	line	213
	
l8997:	
;RFM12B.c: 213: if(gb_Count>=22)
	movlw	(016h)
	subwf	(_gb_Count),w
	skipc
	goto	u641
	goto	u640
u641:
	goto	l5684
u640:
	line	215
	
l8999:	
;RFM12B.c: 214: {
;RFM12B.c: 215: gb_Count=0;
	clrf	(_gb_Count)
	line	216
	
l9001:	
;RFM12B.c: 216: RFM12B_ClearFIFO();
	fcall	_RFM12B_ClearFIFO
	line	217
	
l9003:	
;RFM12B.c: 217: for(i=0;i<14;i++)
	clrf	(RFM12B_RxPacket@i)
	line	219
	
l9009:	
;RFM12B.c: 218: {
;RFM12B.c: 219: if(RxData[i]!=RFM12BData[i])
	movf	(RFM12B_RxPacket@i),w
	addlw	low(_RFM12BData|8000h)
	movlp	high __stringtab
	callw
	pagesel	$
	movwf	(??_RFM12B_RxPacket+0)+0
	movf	(RFM12B_RxPacket@i),w
	addlw	_RxData&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	xorwf	(??_RFM12B_RxPacket+0)+0,w
	skipnz
	goto	u651
	goto	u650
u651:
	goto	l9013
u650:
	goto	l9017
	line	217
	
l9013:	
	incf	(RFM12B_RxPacket@i),f
	
l9015:	
	movlw	(0Eh)
	subwf	(RFM12B_RxPacket@i),w
	skipc
	goto	u661
	goto	u660
u661:
	goto	l9009
u660:
	line	222
	
l9017:	
;RFM12B.c: 221: }
;RFM12B.c: 222: if(i>=14)
	movlw	(0Eh)
	subwf	(RFM12B_RxPacket@i),w
	skipc
	goto	u671
	goto	u670
u671:
	goto	l9021
u670:
	line	223
	
l9019:	
	goto	l5680
	line	225
	
l9021:	
	
l5680:	
	line	226
;RFM12B.c: 226: for(i=0;i<14;i++)RxData[i]=0;
	clrf	(RFM12B_RxPacket@i)
	
l9027:	
	movf	(RFM12B_RxPacket@i),w
	addlw	_RxData&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	clrf	indf1
	
l9029:	
	incf	(RFM12B_RxPacket@i),f
	
l9031:	
	movlw	(0Eh)
	subwf	(RFM12B_RxPacket@i),w
	skipc
	goto	u681
	goto	u680
u681:
	goto	l9027
u680:
	goto	l5684
	line	230
	
l9033:	
;RFM12B.c: 229: else
;RFM12B.c: 230: return(ret);
	movlw	(0)
	line	231
	
l5684:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_RxPacket
	__end_of_RFM12B_RxPacket:
;; =============== function _RFM12B_RxPacket ends ============

	signat	_RFM12B_RxPacket,89
	global	_RFM12B_TxPacket
psect	text466,local,class=CODE,delta=2
global __ptext466
__ptext466:

;; *************** function _RFM12B_TxPacket *****************
;; Defined at:
;;		line 240 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1D/0
;;		On exit  : 1D/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_RFM12B_Standby
;;		_RFM12B_TxByte
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text466
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	240
	global	__size_of_RFM12B_TxPacket
	__size_of_RFM12B_TxPacket	equ	__end_of_RFM12B_TxPacket-_RFM12B_TxPacket
	
_RFM12B_TxPacket:	
	opt	stack 12
; Regs used in _RFM12B_TxPacket: [wreg+status,2+status,0+pclath+cstack]
	line	242
	
l8953:	
;RFM12B.c: 241: byte i;
;RFM12B.c: 242: if(!RB5)
	movlb 0	; select bank0
	btfsc	(109/8),(109)&7
	goto	u591
	goto	u590
u591:
	goto	l5694
u590:
	line	244
	
l8955:	
;RFM12B.c: 243: {
;RFM12B.c: 244: if(!_KeyFlag.BBits.Bit7)
	btfsc	(__KeyFlag),7
	goto	u601
	goto	u600
u601:
	goto	l8961
u600:
	line	246
	
l8957:	
;RFM12B.c: 245: {
;RFM12B.c: 246: _Flag1.BBits.Bit0 = 0;
	bcf	(__Flag1),0
	line	247
	
l8959:	
;RFM12B.c: 247: RFM12B_Standby();
	fcall	_RFM12B_Standby
	line	248
;RFM12B.c: 248: }
	goto	l5694
	line	251
	
l8961:	
;RFM12B.c: 249: else
;RFM12B.c: 250: {
;RFM12B.c: 251: for(i=0;i<4;i++)
	clrf	(RFM12B_TxPacket@i)
	line	252
	
l8967:	
;RFM12B.c: 252: RFM12B_TxByte(0x55);
	movlw	(055h)
	fcall	_RFM12B_TxByte
	line	251
	
l8969:	
	incf	(RFM12B_TxPacket@i),f
	
l8971:	
	movlw	(04h)
	subwf	(RFM12B_TxPacket@i),w
	skipc
	goto	u611
	goto	u610
u611:
	goto	l8967
u610:
	line	253
	
l8973:	
;RFM12B.c: 253: RFM12B_TxByte(0xAA);
	movlw	(0AAh)
	fcall	_RFM12B_TxByte
	line	254
;RFM12B.c: 254: RFM12B_TxByte(0x2D);
	movlw	(02Dh)
	fcall	_RFM12B_TxByte
	line	255
;RFM12B.c: 255: RFM12B_TxByte(0xD4);
	movlw	(0D4h)
	fcall	_RFM12B_TxByte
	line	256
	
l8975:	
;RFM12B.c: 256: for(i=0;i<21;i++)
	clrf	(RFM12B_TxPacket@i)
	line	257
	
l8981:	
;RFM12B.c: 257: RFM12B_TxByte(RFM12BData[i]);
	movf	(RFM12B_TxPacket@i),w
	addlw	low(_RFM12BData|8000h)
	movlp	high __stringtab
	callw
	pagesel	$
	fcall	_RFM12B_TxByte
	line	256
	
l8983:	
	incf	(RFM12B_TxPacket@i),f
	
l8985:	
	movlw	(015h)
	subwf	(RFM12B_TxPacket@i),w
	skipc
	goto	u621
	goto	u620
u621:
	goto	l8981
u620:
	line	258
	
l8987:	
;RFM12B.c: 258: RFM12B_TxByte(0xAA);
	movlw	(0AAh)
	fcall	_RFM12B_TxByte
	line	261
	
l5694:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_TxPacket
	__end_of_RFM12B_TxPacket:
;; =============== function _RFM12B_TxPacket ends ============

	signat	_RFM12B_TxPacket,88
	global	_RFM12B_RxByte
psect	text467,local,class=CODE,delta=2
global __ptext467
__ptext467:

;; *************** function _RFM12B_RxByte *****************
;; Defined at:
;;		line 164 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;;  Result          1    0[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/2
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         2       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_RFM12B_RxPacket
;; This function uses a non-reentrant model
;;
psect	text467
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	164
	global	__size_of_RFM12B_RxByte
	__size_of_RFM12B_RxByte	equ	__end_of_RFM12B_RxByte-_RFM12B_RxByte
	
_RFM12B_RxByte:	
	opt	stack 13
; Regs used in _RFM12B_RxByte: [wreg+status,2+status,0]
	line	167
	
l8925:	
;RFM12B.c: 165: byte i,Result;
;RFM12B.c: 167: LATC3=0;
	movlb 2	; select bank2
	bcf	(2163/8)^0100h,(2163)&7
	line	168
;RFM12B.c: 168: LATC4=0;
	bcf	(2164/8)^0100h,(2164)&7
	line	169
;RFM12B.c: 169: LATA0=0;
	bcf	(2144/8)^0100h,(2144)&7
	line	170
	
l8927:	
;RFM12B.c: 170: for(i=0;i<16;i++)
	clrf	(RFM12B_RxByte@i)
	line	171
	
l5666:	
	line	172
;RFM12B.c: 171: {
;RFM12B.c: 172: LATC3=1;
	movlb 2	; select bank2
	bsf	(2163/8)^0100h,(2163)&7
	line	173
;RFM12B.c: 173: _nop();
	nop
	line	174
;RFM12B.c: 174: _nop();
	nop
	line	175
;RFM12B.c: 175: LATC3=0;
	movlb 2	; select bank2
	bcf	(2163/8)^0100h,(2163)&7
	line	176
;RFM12B.c: 176: _nop();
	nop
	line	177
;RFM12B.c: 177: _nop();
	nop
	line	170
	
l8933:	
	incf	(RFM12B_RxByte@i),f
	
l8935:	
	movlw	(010h)
	subwf	(RFM12B_RxByte@i),w
	skipc
	goto	u561
	goto	u560
u561:
	goto	l5666
u560:
	line	179
	
l8937:	
;RFM12B.c: 178: }
;RFM12B.c: 179: Result=0;
	clrf	(RFM12B_RxByte@Result)
	line	180
;RFM12B.c: 180: for(i=0;i<8;i++)
	clrf	(RFM12B_RxByte@i)
	line	181
	
l5668:	
	line	182
;RFM12B.c: 181: {
;RFM12B.c: 182: Result=Result<<1;
	lslf	(RFM12B_RxByte@Result),f
	line	183
;RFM12B.c: 183: if(RC5)
	movlb 0	; select bank0
	btfss	(117/8),(117)&7
	goto	u571
	goto	u570
u571:
	goto	l5670
u570:
	line	185
	
l8943:	
;RFM12B.c: 184: {
;RFM12B.c: 185: Result|=1;
	bsf	(RFM12B_RxByte@Result)+(0/8),(0)&7
	line	186
	
l5670:	
	line	187
;RFM12B.c: 186: }
;RFM12B.c: 187: LATC3=1;
	movlb 2	; select bank2
	bsf	(2163/8)^0100h,(2163)&7
	line	188
;RFM12B.c: 188: _nop();
	nop
	line	189
;RFM12B.c: 189: _nop();
	nop
	line	190
;RFM12B.c: 190: LATC3=0;
	movlb 2	; select bank2
	bcf	(2163/8)^0100h,(2163)&7
	line	191
;RFM12B.c: 191: _nop();
	nop
	line	192
;RFM12B.c: 192: _nop();
	nop
	line	180
	
l8945:	
	incf	(RFM12B_RxByte@i),f
	
l8947:	
	movlw	(08h)
	subwf	(RFM12B_RxByte@i),w
	skipc
	goto	u581
	goto	u580
u581:
	goto	l5668
u580:
	
l5669:	
	line	194
;RFM12B.c: 193: }
;RFM12B.c: 194: LATA0=1;
	movlb 2	; select bank2
	bsf	(2144/8)^0100h,(2144)&7
	line	195
	
l8949:	
;RFM12B.c: 195: return(Result);
	movf	(RFM12B_RxByte@Result),w
	line	196
	
l5671:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_RxByte
	__end_of_RFM12B_RxByte:
;; =============== function _RFM12B_RxByte ends ============

	signat	_RFM12B_RxByte,89
	global	_RFM12B_TxByte
psect	text468,local,class=CODE,delta=2
global __ptext468
__ptext468:

;; *************** function _RFM12B_TxByte *****************
;; Defined at:
;;		line 124 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;  dat             1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  dat             1    3[COMMON] unsigned char 
;;  temp            2    4[COMMON] unsigned int 
;;  RGIT            1    6[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1D/0
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         4       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         4       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_SPIWrite
;; This function is called by:
;;		_RFM12B_TxPacket
;; This function uses a non-reentrant model
;;
psect	text468
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	124
	global	__size_of_RFM12B_TxByte
	__size_of_RFM12B_TxByte	equ	__end_of_RFM12B_TxByte-_RFM12B_TxByte
	
_RFM12B_TxByte:	
	opt	stack 12
; Regs used in _RFM12B_TxByte: [wreg+status,2+status,0+pclath+cstack]
;RFM12B_TxByte@dat stored from wreg
	movwf	(RFM12B_TxByte@dat)
	line	125
	
l8887:	
	line	126
	
l8889:	
;RFM12B.c: 126: word temp=0xB800;
	movlw	low(0B800h)
	movwf	(RFM12B_TxByte@temp)
	movlw	high(0B800h)
	movwf	((RFM12B_TxByte@temp))+1
	line	128
	
l8891:	
;RFM12B.c: 128: temp|=dat;
	movf	(RFM12B_TxByte@dat),w
	iorwf	(RFM12B_TxByte@temp),f
	line	130
	
l8893:	
;RFM12B.c: 130: LATC3=0;
	movlb 2	; select bank2
	bcf	(2163/8)^0100h,(2163)&7
	line	131
	
l8895:	
;RFM12B.c: 131: LATA0=0;
	bcf	(2144/8)^0100h,(2144)&7
	line	132
	
l8897:	
;RFM12B.c: 132: LATC4=0;
	bcf	(2164/8)^0100h,(2164)&7
	line	133
	
l8899:	
;RFM12B.c: 133: LATC3=1;
	bsf	(2163/8)^0100h,(2163)&7
	line	134
	
l8901:	
;RFM12B.c: 134: if(RC5)
	movlb 0	; select bank0
	btfss	(117/8),(117)&7
	goto	u541
	goto	u540
u541:
	goto	l8905
u540:
	line	136
	
l8903:	
;RFM12B.c: 135: {
;RFM12B.c: 136: RGIT=1;
	clrf	(RFM12B_TxByte@RGIT)
	incf	(RFM12B_TxByte@RGIT),f
	line	137
;RFM12B.c: 137: }
	goto	l8907
	line	140
	
l8905:	
;RFM12B.c: 138: else
;RFM12B.c: 139: {
;RFM12B.c: 140: RGIT=0;
	clrf	(RFM12B_TxByte@RGIT)
	line	142
	
l8907:	
;RFM12B.c: 141: }
;RFM12B.c: 142: LATC3=0;
	movlb 2	; select bank2
	bcf	(2163/8)^0100h,(2163)&7
	line	143
	
l8909:	
;RFM12B.c: 143: LATC4=1;
	bsf	(2164/8)^0100h,(2164)&7
	line	144
	
l8911:	
;RFM12B.c: 144: LATA0=1;
	bsf	(2144/8)^0100h,(2144)&7
	line	145
	
l8913:	
;RFM12B.c: 145: if(RGIT==0)
	movf	(RFM12B_TxByte@RGIT),f
	skipz
	goto	u551
	goto	u550
u551:
	goto	l8919
u550:
	goto	l8893
	line	151
	
l8919:	
	line	152
	
l8921:	
;RFM12B.c: 152: SPIWrite(temp);
	movf	(RFM12B_TxByte@temp+1),w
	movwf	(?_SPIWrite+1)
	movf	(RFM12B_TxByte@temp),w
	movwf	(?_SPIWrite)
	fcall	_SPIWrite
	line	155
	
l5663:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_TxByte
	__end_of_RFM12B_TxByte:
;; =============== function _RFM12B_TxByte ends ============

	signat	_RFM12B_TxByte,4217
	global	_RFM12B_Standby
psect	text469,local,class=CODE,delta=2
global __ptext469
__ptext469:

;; *************** function _RFM12B_Standby *****************
;; Defined at:
;;		line 113 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_SPIWrite
;; This function is called by:
;;		_RFM12B_TxPacket
;; This function uses a non-reentrant model
;;
psect	text469
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	113
	global	__size_of_RFM12B_Standby
	__size_of_RFM12B_Standby	equ	__end_of_RFM12B_Standby-_RFM12B_Standby
	
_RFM12B_Standby:	
	opt	stack 12
; Regs used in _RFM12B_Standby: [wreg+status,2+status,0+pclath+cstack]
	line	114
	
l8885:	
;RFM12B.c: 114: SPIWrite(0x8208);
	movlw	low(08208h)
	movwf	(?_SPIWrite)
	movlw	high(08208h)
	movwf	((?_SPIWrite))+1
	fcall	_SPIWrite
	line	115
	
l5655:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_Standby
	__end_of_RFM12B_Standby:
;; =============== function _RFM12B_Standby ends ============

	signat	_RFM12B_Standby,88
	global	_RFM12B_ClearFIFO
psect	text470,local,class=CODE,delta=2
global __ptext470
__ptext470:

;; *************** function _RFM12B_ClearFIFO *****************
;; Defined at:
;;		line 90 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/2
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_SPIWrite
;; This function is called by:
;;		_RFM12B_RxPacket
;; This function uses a non-reentrant model
;;
psect	text470
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	90
	global	__size_of_RFM12B_ClearFIFO
	__size_of_RFM12B_ClearFIFO	equ	__end_of_RFM12B_ClearFIFO-_RFM12B_ClearFIFO
	
_RFM12B_ClearFIFO:	
	opt	stack 12
; Regs used in _RFM12B_ClearFIFO: [wreg+status,2+status,0+pclath+cstack]
	line	91
	
l8883:	
;RFM12B.c: 91: SPIWrite(0xCA80);
	movlw	low(0CA80h)
	movwf	(?_SPIWrite)
	movlw	high(0CA80h)
	movwf	((?_SPIWrite))+1
	fcall	_SPIWrite
	line	92
;RFM12B.c: 92: SPIWrite(0xCA83);
	movlw	low(0CA83h)
	movwf	(?_SPIWrite)
	movlw	high(0CA83h)
	movwf	((?_SPIWrite))+1
	fcall	_SPIWrite
	line	93
	
l5649:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_ClearFIFO
	__end_of_RFM12B_ClearFIFO:
;; =============== function _RFM12B_ClearFIFO ends ============

	signat	_RFM12B_ClearFIFO,88
	global	_RFM12B_Config
psect	text471,local,class=CODE,delta=2
global __ptext471
__ptext471:

;; *************** function _RFM12B_Config *****************
;; Defined at:
;;		line 43 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1D/2
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_SPIWrite
;; This function is called by:
;;		_main
;;		_RFM12B_EntryRx
;;		_RFM12B_EntryTx
;; This function uses a non-reentrant model
;;
psect	text471
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\RFM12B.c"
	line	43
	global	__size_of_RFM12B_Config
	__size_of_RFM12B_Config	equ	__end_of_RFM12B_Config-_RFM12B_Config
	
_RFM12B_Config:	
	opt	stack 12
; Regs used in _RFM12B_Config: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	45
	
l8859:	
;RFM12B.c: 44: byte i;
;RFM12B.c: 45: for(i=0;i<2;i++)
	clrf	(RFM12B_Config@i)
	line	46
	
l8865:	
;RFM12B.c: 46: SPIWrite(RFM12BFreqTbl[1][i]);
	lslf	(RFM12B_Config@i),w
	addlw	low(_RFM12BFreqTbl|8000h+04h)
	movwf	fsr0l
	movlw	high(_RFM12BFreqTbl|8000h+04h)
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	(?_SPIWrite)
	moviw	[1]fsr0
	movwf	(?_SPIWrite+1)
	fcall	_SPIWrite
	line	45
	
l8867:	
	incf	(RFM12B_Config@i),f
	
l8869:	
	movlw	(02h)
	subwf	(RFM12B_Config@i),w
	skipc
	goto	u521
	goto	u520
u521:
	goto	l8865
u520:
	line	47
	
l8871:	
;RFM12B.c: 47: for(i=0;i<13;i++)
	clrf	(RFM12B_Config@i)
	line	48
	
l8877:	
;RFM12B.c: 48: SPIWrite(RFM12BConfigTbl[i]);
	lslf	(RFM12B_Config@i),w
	addlw	low(_RFM12BConfigTbl|8000h)
	movwf	fsr0l
	movlw	high(_RFM12BConfigTbl|8000h)
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	(?_SPIWrite)
	moviw	[1]fsr0
	movwf	(?_SPIWrite+1)
	fcall	_SPIWrite
	line	47
	
l8879:	
	incf	(RFM12B_Config@i),f
	
l8881:	
	movlw	(0Dh)
	subwf	(RFM12B_Config@i),w
	skipc
	goto	u531
	goto	u530
u531:
	goto	l8877
u530:
	line	49
	
l5640:	
	return
	opt stack 0
GLOBAL	__end_of_RFM12B_Config
	__end_of_RFM12B_Config:
;; =============== function _RFM12B_Config ends ============

	signat	_RFM12B_Config,88
	global	_KeyScan
psect	text472,local,class=CODE,delta=2
global __ptext472
__ptext472:

;; *************** function _KeyScan *****************
;; Defined at:
;;		line 10 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\keyscan.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1D/2
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_ReadKey
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text472
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\keyscan.c"
	line	10
	global	__size_of_KeyScan
	__size_of_KeyScan	equ	__end_of_KeyScan-_KeyScan
	
_KeyScan:	
	opt	stack 13
; Regs used in _KeyScan: [wreg+status,2+status,0+pclath+cstack]
	line	11
	
l8843:	
;keyscan.c: 11: if(0!=ReadKey())
	fcall	_ReadKey
	xorlw	0&0ffh
	skipnz
	goto	u491
	goto	u490
u491:
	goto	l3332
u490:
	line	13
	
l8845:	
;keyscan.c: 12: {
;keyscan.c: 13: if(_KeyFlag.BBits.Bit7)
	btfss	(__KeyFlag),7
	goto	u501
	goto	u500
u501:
	goto	l8851
u500:
	line	15
	
l8847:	
;keyscan.c: 14: {
;keyscan.c: 15: _KeyFlag.BBits.Bit6 = 0;
	bcf	(__KeyFlag),6
	line	16
	
l8849:	
;keyscan.c: 16: KeyTime = 0;
	clrf	(_KeyTime)
	line	17
;keyscan.c: 17: }
	goto	l3337
	line	20
	
l8851:	
;keyscan.c: 18: else
;keyscan.c: 19: {
;keyscan.c: 20: if(KeyTime>=0x02)
	movlw	(02h)
	subwf	(_KeyTime),w
	skipc
	goto	u511
	goto	u510
u511:
	goto	l3337
u510:
	line	22
	
l8853:	
;keyscan.c: 21: {
;keyscan.c: 22: _KeyFlag.BBits.Bit7 = 1;
	bsf	(__KeyFlag),7
	line	23
;keyscan.c: 23: _KeyFlag.BBits.Bit6 = 1;
	bsf	(__KeyFlag),6
	line	24
	
l8855:	
;keyscan.c: 24: KeyCode = ReadKey();
	fcall	_ReadKey
	goto	l3337
	line	28
	
l3332:	
	line	30
;keyscan.c: 28: else
;keyscan.c: 29: {
;keyscan.c: 30: _KeyFlag.BBits.Bit7 = 0;
	bcf	(__KeyFlag),7
	line	31
;keyscan.c: 31: _KeyFlag.BBits.Bit6 = 0;
	bcf	(__KeyFlag),6
	goto	l8849
	line	34
	
l3337:	
	return
	opt stack 0
GLOBAL	__end_of_KeyScan
	__end_of_KeyScan:
;; =============== function _KeyScan ends ============

	signat	_KeyScan,88
	global	_SPIWrite
psect	text473,local,class=CODE,delta=2
global __ptext473
__ptext473:

;; *************** function _SPIWrite *****************
;; Defined at:
;;		line 80 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\spi.c"
;; Parameters:    Size  Location     Type
;;  WrPara          2    0[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  bitcnt          1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1D/2
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         2       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         3       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_RFM12B_Config
;;		_RFM12B_EntryRx
;;		_RFM12B_EntryTx
;;		_RFM12B_ClearFIFO
;;		_RFM12B_Standby
;;		_RFM12B_TxByte
;; This function uses a non-reentrant model
;;
psect	text473
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\spi.c"
	line	80
	global	__size_of_SPIWrite
	__size_of_SPIWrite	equ	__end_of_SPIWrite-_SPIWrite
	
_SPIWrite:	
	opt	stack 13
; Regs used in _SPIWrite: [wreg+status,2+status,0]
	line	83
	
l8831:	
;spi.c: 81: byte bitcnt;
;spi.c: 83: LATC3 = 0;
	movlb 2	; select bank2
	bcf	(2163/8)^0100h,(2163)&7
	line	84
;spi.c: 84: LATA0 = 0;
	bcf	(2144/8)^0100h,(2144)&7
	line	86
;spi.c: 86: WrPara |= 0x8000;
	bsf	(SPIWrite@WrPara)+(15/8),(15)&7
	line	88
	
l8833:	
;spi.c: 88: for(bitcnt=16; bitcnt!=0; bitcnt--)
	movlw	(010h)
	movwf	(SPIWrite@bitcnt)
	line	89
	
l7934:	
	line	90
;spi.c: 89: {
;spi.c: 90: LATC3 = 0;
	bcf	(2163/8)^0100h,(2163)&7
	line	91
;spi.c: 91: if(WrPara&0x8000)
	btfss	(SPIWrite@WrPara+1),(15)&7
	goto	u471
	goto	u470
u471:
	goto	l7936
u470:
	line	92
	
l8839:	
;spi.c: 92: LATC4 = 1;
	bsf	(2164/8)^0100h,(2164)&7
	goto	l7937
	line	93
	
l7936:	
	line	94
;spi.c: 93: else
;spi.c: 94: LATC4 = 0;
	bcf	(2164/8)^0100h,(2164)&7
	
l7937:	
	line	95
;spi.c: 95: LATC3 = 1;
	bsf	(2163/8)^0100h,(2163)&7
	line	96
	
l8841:	
;spi.c: 96: WrPara <<= 1;
	lslf	(SPIWrite@WrPara),f
	rlf	(SPIWrite@WrPara+1),f
	line	88
	decf	(SPIWrite@bitcnt),f
	movf	(SPIWrite@bitcnt),f
	skipz
	goto	u481
	goto	u480
u481:
	goto	l7934
u480:
	
l7935:	
	line	98
;spi.c: 97: }
;spi.c: 98: LATC3 = 0;
	bcf	(2163/8)^0100h,(2163)&7
	line	99
;spi.c: 99: LATC4 = 1;
	bsf	(2164/8)^0100h,(2164)&7
	line	100
;spi.c: 100: LATA0= 1;
	bsf	(2144/8)^0100h,(2144)&7
	line	101
	
l7938:	
	return
	opt stack 0
GLOBAL	__end_of_SPIWrite
	__end_of_SPIWrite:
;; =============== function _SPIWrite ends ============

	signat	_SPIWrite,4216
	global	_TxLedOff
psect	text474,local,class=CODE,delta=2
global __ptext474
__ptext474:

;; *************** function _TxLedOff *****************
;; Defined at:
;;		line 143 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/2
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text474
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	line	143
	global	__size_of_TxLedOff
	__size_of_TxLedOff	equ	__end_of_TxLedOff-_TxLedOff
	
_TxLedOff:	
	opt	stack 14
; Regs used in _TxLedOff: [wreg+status,2+status,0]
	line	144
	
l8829:	
;main.c: 144: LATD &= 0x81;
	movlw	(081h)
	andwf	(271)^0100h,f	;volatile
	line	145
	
l4509:	
	return
	opt stack 0
GLOBAL	__end_of_TxLedOff
	__end_of_TxLedOff:
;; =============== function _TxLedOff ends ============

	signat	_TxLedOff,88
	global	_TxLedFlash
psect	text475,local,class=CODE,delta=2
global __ptext475
__ptext475:

;; *************** function _TxLedFlash *****************
;; Defined at:
;;		line 108 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1D/0
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text475
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	line	108
	global	__size_of_TxLedFlash
	__size_of_TxLedFlash	equ	__end_of_TxLedFlash-_TxLedFlash
	
_TxLedFlash:	
	opt	stack 14
; Regs used in _TxLedFlash: [wreg-fsr0h+status,2+status,0]
	line	109
	
l8817:	
;main.c: 109: if(TxLedTime>=15)
	movlw	(0Fh)
	subwf	(_TxLedTime),w
	skipc
	goto	u401
	goto	u400
u401:
	goto	l4506
u400:
	line	111
	
l8819:	
;main.c: 110: {
;main.c: 111: TxLedCnt++;
	incf	(_TxLedCnt),f
	line	112
	
l8821:	
;main.c: 112: if(TxLedCnt>=7)
	movlw	(07h)
	subwf	(_TxLedCnt),w
	skipc
	goto	u411
	goto	u410
u411:
	goto	l4496
u410:
	line	113
	
l8823:	
;main.c: 113: TxLedCnt = 0;
	clrf	(_TxLedCnt)
	
l4496:	
	line	114
;main.c: 114: TxLedTime = 0;
	clrf	(_TxLedTime)
	line	116
;main.c: 116: switch(TxLedCnt)
	goto	l8827
	line	118
;main.c: 117: {
;main.c: 118: case 1:
	
l4498:	
	line	119
;main.c: 119: LATD4 = 1; break;
	movlb 2	; select bank2
	bsf	(2172/8)^0100h,(2172)&7
	goto	l4506
	line	120
;main.c: 120: case 2:
	
l4500:	
	line	121
;main.c: 121: LATD3 = LATD5 = 1; break;
	movlb 2	; select bank2
	bsf	(2173/8)^0100h,(2173)&7
	btfsc	(2173/8)^0100h,(2173)&7
	goto	u421
	goto	u420
	
u421:
	movlb 2	; select bank2
	bsf	(2171/8)^0100h,(2171)&7
	goto	u434
u420:
	movlb 2	; select bank2
	bcf	(2171/8)^0100h,(2171)&7
u434:
	goto	l4506
	line	122
;main.c: 122: case 3:
	
l4501:	
	line	123
;main.c: 123: LATD1 = LATD2 = LATD6 = 1; break;
	movlb 2	; select bank2
	bsf	(2174/8)^0100h,(2174)&7
	btfsc	(2174/8)^0100h,(2174)&7
	goto	u441
	goto	u440
	
u441:
	movlb 2	; select bank2
	bsf	(2170/8)^0100h,(2170)&7
	goto	u454
u440:
	movlb 2	; select bank2
	bcf	(2170/8)^0100h,(2170)&7
u454:
	btfsc	(2170/8)^0100h,(2170)&7
	goto	u451
	goto	u450
	
u451:
	movlb 2	; select bank2
	bsf	(2169/8)^0100h,(2169)&7
	goto	u464
u450:
	movlb 2	; select bank2
	bcf	(2169/8)^0100h,(2169)&7
u464:
	goto	l4506
	line	124
;main.c: 124: case 4:
	
l4502:	
	line	125
;main.c: 125: LATD4 = 0; break;
	movlb 2	; select bank2
	bcf	(2172/8)^0100h,(2172)&7
	goto	l4506
	line	126
;main.c: 126: case 5:
	
l4503:	
	line	127
;main.c: 127: LATD3 = LATD5 = 0; break;
	movlb 2	; select bank2
	bcf	(2173/8)^0100h,(2173)&7
	bcf	(2171/8)^0100h,(2171)&7
	goto	l4506
	line	128
;main.c: 128: case 6:
	
l4504:	
	line	129
;main.c: 129: LATD1 = LATD2 = LATD6 = 0; break;
	movlb 2	; select bank2
	bcf	(2174/8)^0100h,(2174)&7
	bcf	(2170/8)^0100h,(2170)&7
	bcf	(2169/8)^0100h,(2169)&7
	goto	l4506
	line	116
	
l8827:	
	movf	(_TxLedCnt),w
	; Switch size 1, requested type "space"
; Number of cases is 6, Range of values is 1 to 6
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           19    10 (average)
; direct_byte           21     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	1^0	; case 1
	skipnz
	goto	l4498
	xorlw	2^1	; case 2
	skipnz
	goto	l4500
	xorlw	3^2	; case 3
	skipnz
	goto	l4501
	xorlw	4^3	; case 4
	skipnz
	goto	l4502
	xorlw	5^4	; case 5
	skipnz
	goto	l4503
	xorlw	6^5	; case 6
	skipnz
	goto	l4504
	goto	l4506
	opt asmopt_on

	line	134
	
l4506:	
	return
	opt stack 0
GLOBAL	__end_of_TxLedFlash
	__end_of_TxLedFlash:
;; =============== function _TxLedFlash ends ============

	signat	_TxLedFlash,88
	global	_InitPort
psect	text476,local,class=CODE,delta=2
global __ptext476
__ptext476:

;; *************** function _InitPort *****************
;; Defined at:
;;		line 211 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 17F/1
;;		On exit  : 17F/2
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text476
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	line	211
	global	__size_of_InitPort
	__size_of_InitPort	equ	__end_of_InitPort-_InitPort
	
_InitPort:	
	opt	stack 14
; Regs used in _InitPort: [wreg+status,2]
	line	212
	
l8793:	
;main.c: 212: ANSELA= 0x00;
	movlb 3	; select bank3
	clrf	(396)^0180h	;volatile
	line	213
;main.c: 213: ANSELB= 0x00;
	clrf	(397)^0180h	;volatile
	line	214
;main.c: 214: ANSELC= 0x00;
	clrf	(398)^0180h	;volatile
	line	215
;main.c: 215: ANSELD= 0x00;
	clrf	(399)^0180h	;volatile
	line	216
;main.c: 216: ANSELE= 0x00;
	clrf	(400)^0180h	;volatile
	line	218
	
l8795:	
;main.c: 218: TRISA = 0xCE;
	movlw	(0CEh)
	movlb 1	; select bank1
	movwf	(140)^080h	;volatile
	line	219
	
l8797:	
;main.c: 219: TRISB = 0x3F;
	movlw	(03Fh)
	movwf	(141)^080h	;volatile
	line	220
	
l8799:	
;main.c: 220: TRISC = 0xA3;
	movlw	(0A3h)
	movwf	(142)^080h	;volatile
	line	221
	
l8801:	
;main.c: 221: TRISD = 0x01;
	movlw	(01h)
	movwf	(143)^080h	;volatile
	line	222
	
l8803:	
;main.c: 222: TRISE = 0x08;
	movlw	(08h)
	movwf	(144)^080h	;volatile
	line	224
	
l8805:	
;main.c: 224: PORTA = 0x11;
	movlw	(011h)
	movlb 0	; select bank0
	movwf	(12)	;volatile
	line	225
;main.c: 225: PORTB = 0x00;
	clrf	(13)	;volatile
	line	226
	
l8807:	
;main.c: 226: PORTC = 0x50;
	movlw	(050h)
	movwf	(14)	;volatile
	line	227
	
l8809:	
;main.c: 227: PORTD = 0x00;
	clrf	(15)	;volatile
	line	228
;main.c: 228: PORTE = 0x04;
	movlw	(04h)
	movwf	(16)	;volatile
	line	230
;main.c: 230: LATA = 0x11;
	movlw	(011h)
	movlb 2	; select bank2
	movwf	(268)^0100h	;volatile
	line	231
	
l8811:	
;main.c: 231: LATB = 0x00;
	clrf	(269)^0100h	;volatile
	line	232
	
l8813:	
;main.c: 232: LATC = 0x50;
	movlw	(050h)
	movwf	(270)^0100h	;volatile
	line	233
;main.c: 233: LATD = 0x00;
	clrf	(271)^0100h	;volatile
	line	234
	
l8815:	
;main.c: 234: LATE = 0x04;
	movlw	(04h)
	movwf	(272)^0100h	;volatile
	line	235
	
l4515:	
	return
	opt stack 0
GLOBAL	__end_of_InitPort
	__end_of_InitPort:
;; =============== function _InitPort ends ============

	signat	_InitPort,88
	global	_InitSfr
psect	text477,local,class=CODE,delta=2
global __ptext477
__ptext477:

;; *************** function _InitSfr *****************
;; Defined at:
;;		line 154 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 17F/1
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text477
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\main.c"
	line	154
	global	__size_of_InitSfr
	__size_of_InitSfr	equ	__end_of_InitSfr-_InitSfr
	
_InitSfr:	
	opt	stack 14
; Regs used in _InitSfr: [wreg+status,2]
	line	156
	
l8741:	
;main.c: 156: IOCBP = 0;
	movlb 7	; select bank7
	clrf	(916)^0380h	;volatile
	line	157
;main.c: 157: IOCBN = 0;
	clrf	(917)^0380h	;volatile
	line	158
;main.c: 158: IOCBF = 0;
	clrf	(918)^0380h	;volatile
	line	163
;main.c: 163: CCP1CON = 0;
	movlb 5	; select bank5
	clrf	(659)^0280h	;volatile
	line	164
;main.c: 164: CCP2CON = 0;
	clrf	(666)^0280h	;volatile
	line	167
	
l8743:	
;main.c: 167: WPUB = 0x0F;
	movlw	(0Fh)
	movlb 4	; select bank4
	movwf	(525)^0200h	;volatile
	line	168
	
l8745:	
;main.c: 168: WPUE = 0;
	clrf	(528)^0200h	;volatile
	line	169
	
l8747:	
;main.c: 169: SSPCON1 = 0;
	clrf	(533)^0200h	;volatile
	line	170
	
l8749:	
;main.c: 170: SSPCON2 = 0;
	clrf	(534)^0200h	;volatile
	line	171
	
l8751:	
;main.c: 171: SSPCON3 = 0;
	clrf	(535)^0200h	;volatile
	line	174
	
l8753:	
;main.c: 174: BORCON = 0x00;
	movlb 2	; select bank2
	clrf	(278)^0100h	;volatile
	line	175
	
l8755:	
;main.c: 175: FVRCON = 0x00;
	clrf	(279)^0100h	;volatile
	line	176
	
l8757:	
;main.c: 176: APFCON = 0x02;
	movlw	(02h)
	movwf	(285)^0100h	;volatile
	line	179
	
l8759:	
;main.c: 179: OPTION_REG = 0x07;
	movlw	(07h)
	movlb 1	; select bank1
	movwf	(149)^080h	;volatile
	line	180
	
l8761:	
;main.c: 180: WDTCON = 0;
	clrf	(151)^080h	;volatile
	line	181
	
l8763:	
;main.c: 181: OSCCON = 0;
	clrf	(153)^080h	;volatile
	line	182
	
l8765:	
;main.c: 182: ADCON0 = 0;
	clrf	(157)^080h	;volatile
	line	183
	
l8767:	
;main.c: 183: ADCON1 = 0;
	clrf	(158)^080h	;volatile
	line	184
	
l8769:	
;main.c: 184: PIE1 = 0;
	clrf	(145)^080h	;volatile
	line	185
	
l8771:	
;main.c: 185: PIE2 = 0;
	clrf	(146)^080h	;volatile
	line	188
	
l8773:	
;main.c: 188: PIR1 = 0;
	movlb 0	; select bank0
	clrf	(17)	;volatile
	line	189
	
l8775:	
;main.c: 189: PIR2 = 0;
	clrf	(18)	;volatile
	line	190
	
l8777:	
;main.c: 190: TMR1L = (byte)0x8000;
	clrf	(22)	;volatile
	line	191
;main.c: 191: TMR1H = (byte)(0x8000>>8);
	movlw	(080h)
	movwf	(23)	;volatile
	line	192
;main.c: 192: T1CON = 0x8D;
	movlw	(08Dh)
	movwf	(24)	;volatile
	line	193
	
l8779:	
;main.c: 193: T1GCON = 0;
	clrf	(25)	;volatile
	line	195
	
l8781:	
;main.c: 195: T2CON = 0b00000001;
	movlw	(01h)
	movwf	(28)	;volatile
	line	196
	
l8783:	
;main.c: 196: PR2 = 0xFF;
	movlw	(0FFh)
	movwf	(27)	;volatile
	line	198
	
l8785:	
;main.c: 198: GIE = 1;
	bsf	(95/8),(95)&7
	line	199
	
l8787:	
;main.c: 199: PEIE = 1;
	bsf	(94/8),(94)&7
	line	200
	
l8789:	
;main.c: 200: TMR0IE = 1;
	bsf	(93/8),(93)&7
	line	201
	
l8791:	
;main.c: 201: TMR1IE = 1;
	movlb 1	; select bank1
	bsf	(1160/8)^080h,(1160)&7
	line	202
	
l4512:	
	return
	opt stack 0
GLOBAL	__end_of_InitSfr
	__end_of_InitSfr:
;; =============== function _InitSfr ends ============

	signat	_InitSfr,88
	global	_ReadKey
psect	text478,local,class=CODE,delta=2
global __ptext478
__ptext478:

;; *************** function _ReadKey *****************
;; Defined at:
;;		line 43 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\keyscan.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 1D/2
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_KeyScan
;; This function uses a non-reentrant model
;;
psect	text478
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\keyscan.c"
	line	43
	global	__size_of_ReadKey
	__size_of_ReadKey	equ	__end_of_ReadKey-_ReadKey
	
_ReadKey:	
	opt	stack 13
; Regs used in _ReadKey: [wreg+status,2]
	line	44
	
l8737:	
;keyscan.c: 44: return((PORTB^0xff)&0x0F);
	movlb 0	; select bank0
	comf	(13),w	;volatile
	andlw	0Fh
	line	45
	
l3340:	
	return
	opt stack 0
GLOBAL	__end_of_ReadKey
	__end_of_ReadKey:
;; =============== function _ReadKey ends ============

	signat	_ReadKey,89
	global	_BeepOff
psect	text479,local,class=CODE,delta=2
global __ptext479
__ptext479:

;; *************** function _BeepOff *****************
;; Defined at:
;;		line 24 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\buzzer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		status,2
;; Tracked objects:
;;		On entry : 1D/0
;;		On exit  : 1F/2
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text479
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\buzzer.c"
	line	24
	global	__size_of_BeepOff
	__size_of_BeepOff	equ	__end_of_BeepOff-_BeepOff
	
_BeepOff:	
	opt	stack 14
; Regs used in _BeepOff: [status,2]
	line	25
	
l8717:	
;buzzer.c: 25: CCP1CON = 0b00000000;
	movlb 5	; select bank5
	clrf	(659)^0280h	;volatile
	line	26
	
l8719:	
;buzzer.c: 26: TMR2ON = 0;
	movlb 0	; select bank0
	bcf	(226/8),(226)&7
	line	27
	
l8721:	
;buzzer.c: 27: LATC2 = 0;
	movlb 2	; select bank2
	bcf	(2162/8)^0100h,(2162)&7
	line	28
	
l1106:	
	return
	opt stack 0
GLOBAL	__end_of_BeepOff
	__end_of_BeepOff:
;; =============== function _BeepOff ends ============

	signat	_BeepOff,88
	global	_BeepOn
psect	text480,local,class=CODE,delta=2
global __ptext480
__ptext480:

;; *************** function _BeepOn *****************
;; Defined at:
;;		line 10 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\buzzer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text480
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\buzzer.c"
	line	10
	global	__size_of_BeepOn
	__size_of_BeepOn	equ	__end_of_BeepOn-_BeepOn
	
_BeepOn:	
	opt	stack 14
; Regs used in _BeepOn: [wreg]
	line	12
	
l8713:	
;buzzer.c: 12: CCPR1L = 0xC0;
	movlw	(0C0h)
	movlb 5	; select bank5
	movwf	(657)^0280h	;volatile
	line	13
;buzzer.c: 13: CCP1CON = 0b00001100;
	movlw	(0Ch)
	movwf	(659)^0280h	;volatile
	line	14
	
l8715:	
;buzzer.c: 14: TMR2ON = 1;
	movlb 0	; select bank0
	bsf	(226/8),(226)&7
	line	15
	
l1103:	
	return
	opt stack 0
GLOBAL	__end_of_BeepOn
	__end_of_BeepOn:
;; =============== function _BeepOn ends ============

	signat	_BeepOn,88
	global	_ISR
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:

;; *************** function _ISR *****************
;; Defined at:
;;		line 10 in file "E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	intentry
	file	"E:\NewDemo\RFDK_PIC16_LCD_V1.0\Code\RFM12B\RFDK_PIC16_LCD_RFM12B_V1.0-20130520\Source\interrupt.c"
	line	10
	global	__size_of_ISR
	__size_of_ISR	equ	__end_of_ISR-_ISR
	
_ISR:	
	opt	stack 12
; Regs used in _ISR: [wreg+status,2+status,0]
psect	intentry
	pagesel	$
	line	11
	
i1l8723:	
;interrupt.c: 11: if(TMR0IF)
	btfss	(90/8),(90)&7
	goto	u38_21
	goto	u38_20
u38_21:
	goto	i1l8729
u38_20:
	line	13
	
i1l8725:	
;interrupt.c: 12: {
;interrupt.c: 13: TMR0IF = 0;
	bcf	(90/8),(90)&7
	line	14
	
i1l8727:	
;interrupt.c: 14: _SysTime.BByte++;
	incf	(__SysTime),f
	line	15
;interrupt.c: 15: KeyTime++;
	incf	(_KeyTime),f
	line	16
;interrupt.c: 16: RxLimtTime++;
	movlb 0	; select bank0
	incf	(_RxLimtTime),f
	line	17
;interrupt.c: 17: TxLedTime++;
	incf	(_TxLedTime),f
	line	20
	
i1l8729:	
;interrupt.c: 18: }
;interrupt.c: 20: if(TMR1IF)
	movlb 0	; select bank0
	btfss	(136/8),(136)&7
	goto	u39_21
	goto	u39_20
u39_21:
	goto	i1l2221
u39_20:
	line	22
	
i1l8731:	
;interrupt.c: 21: {
;interrupt.c: 22: TMR1IF = 0;
	bcf	(136/8),(136)&7
	line	23
	
i1l8733:	
;interrupt.c: 23: TMR1L = (byte)0x8000;
	clrf	(22)	;volatile
	line	24
	
i1l8735:	
;interrupt.c: 25: _SecTime.BByte++;
	movlw	(080h)
	movwf	(23)	;volatile
	line	27
	
i1l2221:	
	retfie
	opt stack 0
GLOBAL	__end_of_ISR
	__end_of_ISR:
;; =============== function _ISR ends ============

	signat	_ISR,88
psect	intentry
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
