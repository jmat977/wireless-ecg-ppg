#include "RFM12B_demo1.h"

/**********************************************************
**Name:     KeyScan
**Function: Get key code
**Input:    none
**Output:   none
**********************************************************/
void KeyScan(void)
{
 if(0!=ReadKey())
 	{
	if(KeyFree_F)
		{
		KeyPre_F = 0;
		KeyTime  = 0;
		} 	
 	else
 		{
 		if(KeyTime>=0x02)
			{
 			KeyFree_F = 1;
 			KeyPre_F  = 1;
 			KeyCode   = ReadKey();
 			}
 		}
 	}
 else
 	{
	KeyFree_F = 0;                                           //key free	
 	KeyPre_F  = 0;                                           //clear key press
 	KeyTime   = 0;
 	}
}

/**********************************************************
**Name:     ReadKey
**Function: read PortB
**Input:    none
**Output:   none
**********************************************************/
byte ReadKey(void)
{
 return((PORTB^0xff)&0x0F);                               //Read Key
}



