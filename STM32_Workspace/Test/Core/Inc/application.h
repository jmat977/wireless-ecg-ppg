/*
 * application.h
 *
 *  Created on: Nov 4, 2020
 *      Author: jakub
 */

#ifndef INC_APPLICATION_H_
#define INC_APPLICATION_H_

enum state {
	INIT,
	TRACKING,
	SEARCH_WIN_ST,
	EXTREMUM_SEARCH,
	CALC_COEFF,
	ST_REFACT_WIN,
	WAKE_UP
} SM_state;

#define MAIN_BUFF_F_DATA_SIZE 1024
#define ABS_DIF_LONG_DATA_SIZE 128

#define SLOPE_THRESHOLD 0.11

#include "main.h"
#include "usart.h"
#include "circ_buff_f.h"
#include "my_uart.h"
#include "moving_avg.h"
#include "circ_buff_f.h"
#include <math.h>
#include "stm32f3xx_hal.h"
#include "../../Drivers/RFM12B/Inc/rfm12b.h"

extern CircBuff_f_t Main_buff_f, Abs_dif_long_buff;

void app();
void app_init();

void get_value(uint32_t i);
void calc_moving_avg();
void calc_abs_dif();
void send_val();
void check_for_extremum();
void clean_up(void);



bool find_extremum(CircBuff_f_t *circbuff);

bool is_searching_win_active(void);

bool is_refactory_win_active(void);

void activate_refactory_win(void);

void activate_searching_win(void);


extern bool is_cmd_ready;




#endif /* INC_APPLICATION_H_ */
