EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 7000 5000
encoding utf-8
Sheet 8 9
Title "ISM band transceiver"
Date "2020-08-01"
Rev "1"
Comp "AGH Faculty of Electrical Engineering, Automatics, Computer Science and Biomedical Engineering"
Comment1 ""
Comment2 "Sytem for aquisition and transmission of biomedical data "
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Antenna AE?
U 1 1 605D5533
P 1220 920
AR Path="/605D5533" Ref="AE?"  Part="1" 
AR Path="/605C5182/605D5533" Ref="AE1"  Part="1" 
F 0 "AE1" H 1300 909 50  0000 L CNN
F 1 "Antenna" H 1300 818 50  0000 L CNN
F 2 "Wireless_ECG_PPG_lib:868MHZ_Antenna" H 1220 920 50  0001 C CNN
F 3 "~" H 1220 920 50  0001 C CNN
	1    1220 920 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1220 1210 1220 1120
$Comp
L Device:C_Small C?
U 1 1 605D554A
P 4660 2200
AR Path="/605D554A" Ref="C?"  Part="1" 
AR Path="/605C5182/605D554A" Ref="C3"  Part="1" 
F 0 "C3" H 4460 2260 50  0000 L CNN
F 1 "100n" H 4430 2130 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4660 2200 50  0001 C CNN
F 3 "~" H 4660 2200 50  0001 C CNN
	1    4660 2200
	1    0    0    -1  
$EndComp
NoConn ~ 2270 2030
Wire Wire Line
	1220 1210 1380 1210
Text GLabel 2180 2480 0    35   BiDi ~ 0
ANT
Wire Wire Line
	2270 2480 2180 2480
Text GLabel 3820 1980 2    35   BiDi ~ 0
SDI
Wire Wire Line
	3720 1980 3820 1980
Text GLabel 3820 2280 2    35   BiDi ~ 0
~SEL
Wire Wire Line
	3720 2280 3820 2280
Text GLabel 3820 2180 2    35   BiDi ~ 0
SCK
Wire Wire Line
	3720 2180 3820 2180
Text GLabel 1380 1210 2    35   BiDi ~ 0
ANT
Wire Wire Line
	5630 1010 5530 1010
Wire Wire Line
	5630 1110 5530 1110
Text GLabel 5530 1300 0    35   BiDi ~ 0
~SEL
Wire Wire Line
	5630 1300 5530 1300
Text GLabel 5530 1210 0    35   BiDi ~ 0
SCK
Wire Wire Line
	5630 1210 5530 1210
Text HLabel 5630 1010 2    50   BiDi Italic 0
SDO
Text HLabel 5630 1110 2    50   BiDi Italic 0
SDI
Text HLabel 5630 1210 2    50   BiDi Italic 0
SCK
Text HLabel 5630 1300 2    50   BiDi Italic 0
~SEL
$Comp
L power:+3.3V #PWR?
U 1 1 5F9777F2
P 3810 1560
AR Path="/5F9777F2" Ref="#PWR?"  Part="1" 
AR Path="/60503AAD/5F9777F2" Ref="#PWR?"  Part="1" 
AR Path="/605C5182/5F9777F2" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 3810 1410 50  0001 C CNN
F 1 "+3.3V" H 3825 1733 50  0000 C CNN
F 2 "" H 3810 1560 50  0001 C CNN
F 3 "" H 3810 1560 50  0001 C CNN
	1    3810 1560
	1    0    0    -1  
$EndComp
Text GLabel 3820 2080 2    35   BiDi ~ 0
SDO
Wire Wire Line
	3720 2080 3820 2080
Wire Wire Line
	3720 1680 3810 1680
Wire Wire Line
	3810 1680 3810 1560
$Comp
L Wireless_ECG_PPG_lib:RFM12B U?
U 1 1 605D5542
P 3020 1880
AR Path="/605D5542" Ref="U?"  Part="1" 
AR Path="/605C5182/605D5542" Ref="U2"  Part="1" 
F 0 "U2" H 2995 2398 50  0000 C CNN
F 1 "RFM12B" H 2995 2307 50  0000 C CNB
F 2 "Wireless_ECG_PPG_lib:RFM12B" H 2970 2280 50  0001 C CNN
F 3 "" H 3020 2430 50  0001 C CNN
	1    3020 1880
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0116
U 1 1 5FA0F6AE
P 3830 2710
F 0 "#PWR0116" H 3830 2460 50  0001 C CNN
F 1 "GNDD" H 3834 2555 50  0000 C CNN
F 2 "" H 3830 2710 50  0001 C CNN
F 3 "" H 3830 2710 50  0001 C CNN
	1    3830 2710
	1    0    0    -1  
$EndComp
Wire Wire Line
	3720 2630 3830 2630
Wire Wire Line
	3830 2630 3830 2710
Wire Wire Line
	3720 2530 3830 2530
Wire Wire Line
	3830 2530 3830 2630
Connection ~ 3830 2630
Text GLabel 2170 1730 0    35   BiDi ~ 0
~IRQ
Wire Wire Line
	2170 1730 2270 1730
$Comp
L power:+3.3V #PWR?
U 1 1 5FA17A63
P 4660 2030
AR Path="/5FA17A63" Ref="#PWR?"  Part="1" 
AR Path="/60503AAD/5FA17A63" Ref="#PWR?"  Part="1" 
AR Path="/605C5182/5FA17A63" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 4660 1880 50  0001 C CNN
F 1 "+3.3V" H 4675 2203 50  0000 C CNN
F 2 "" H 4660 2030 50  0001 C CNN
F 3 "" H 4660 2030 50  0001 C CNN
	1    4660 2030
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0118
U 1 1 5FA190CC
P 4660 2370
F 0 "#PWR0118" H 4660 2120 50  0001 C CNN
F 1 "GNDD" H 4664 2215 50  0000 C CNN
F 2 "" H 4660 2370 50  0001 C CNN
F 3 "" H 4660 2370 50  0001 C CNN
	1    4660 2370
	1    0    0    -1  
$EndComp
Text GLabel 5530 1110 0    35   BiDi ~ 0
SDI
Text GLabel 5530 1010 0    35   BiDi ~ 0
SDO
Text GLabel 5530 1680 0    35   BiDi ~ 0
~IRQ
Wire Notes Line
	6400 1400 5180 1400
Wire Notes Line
	5180 1400 5180 900 
Wire Notes Line
	5180 900  6400 900 
Text Notes 6130 1190 0    50   ~ 0
SPI
Wire Wire Line
	5630 1680 5530 1680
Text HLabel 5630 1680 2    50   BiDi Italic 0
~IRQ
Wire Wire Line
	4660 2030 4660 2100
Wire Wire Line
	4660 2370 4660 2300
NoConn ~ 2270 1830
NoConn ~ 2270 1930
NoConn ~ 2270 2130
Text GLabel 2170 2230 0    35   BiDi ~ 0
~INT~_VDI
Wire Wire Line
	2170 2230 2270 2230
Text GLabel 5535 1815 0    35   BiDi ~ 0
~INT~_VDI
Text HLabel 5640 1815 2    50   BiDi ~ 0
~INT~_VDI
Wire Wire Line
	5535 1815 5640 1815
$EndSCHEMATC
