/**
 * This library was based on Arduino RFM12B library.
 * Rewritten for STM32
 *
 * Original driver from jeelabs.com (2009-02-09 <jc@wippler.nl>).
 *
 * Contact:
 * Website: http://www.srcpro.pl
 * e-mail: piotr.krzyzaniak@gmail.com
 *
 * (C) 2017-10-09 Piotr Krzyzaniak
 */
#include "../Inc/rfm12b.h"
#include "spi.h"

//#include "utils.h"
//#include "common.h"



//#define rfm_spi hspi2

static const uint8_t Preamble = 0xDEU;
static const uint8_t DeviceAddress = 0xADU;
uint8_t notepad[HEADER_LEN + 1 + 4];


uint16_t RFM12B_registers[14] = {
  0x800DU,                   // Config Setting
  0x8208U,                   // Power Management
  0xA680U,                   // Frequency Setting
  0xC691U,                   // Data Rate
  0x9080U,                   // Receiver Control
  0xC22CU,                   // Data Filter,DQD
  0xCA80U,                   // Fifo and reset
  0xCED4U,                   // Synchro pattern
  0xC4F7U,                   // AFC
  0x9800U,                   // TX Configuration Control
  0xCC77U,                   // PLL Setting
  0xE196U,                   // Wake up Timer
  0xC80EU,                   // Low duty-cycle
  0xC000U                    // Low battery and uC CLK divider
};


void rfm_config(void)
{
/*
  rfm_enableFIFOmode();
  rfm_setBand(RFM_CS_BAND_868);
  rfm_setFrequency(1600U); // f_0 = 868MHz
  rfm_rxControl(RFM_RC_P16_MASK |             // VDI output
                RFM_RC_VDI_FAST |
                RFM_RC_BW_134 |               // receiver baseband
                RFM_RC_LNA_0 |                // LNA 0dB
                RFM_RC_RSSI_103);
  rfm_dataFilter(RFM_DF_RC_FILTER_DIGITAL |
                  RFM_DF_CR_AUTO_LOCK_CTRL |
                  0x0004);
  rfm_fifoAndResetMode(8 << RFM_FARM_F_BIT);  // 8-bit fifo interrupt level
  rfm_setSynchronPattern(0xd4);               // D4 - from datasheet
  rfm_fifoAndResetMode((8 << RFM_FARM_F_BIT) |
                        RFM_FARM_FF_MASK |
                        RFM_FARM_DR_MASK);
  rfm_afc(RFM_AFC_KEEP_WHEN_VDI |
          RFM_AFC_FREQ_RANGE_15 |
          RFM_AFC_ST_MASK |
          RFM_AFC_OE_MASK |
          RFM_AFC_EN_MASK);
  rfm_pllSetting(RFM_PLLS_OB_MASK |
                 RFM_PLLS_DDIT_MASK |
                 RFM_PLLS_BW0_MASK);
  rfm_wakeUpTimer(0x6, 0xff);                 // 1 sec.
  rfm_lowDutyCycle(0x0E);
  rfm_dataRate(0x11, 1);                      // default value
  rfm_txConfig(0, 1, 0);
  rfm_lowBatteryDetector(3, 0);
  */

	rfm_software_reset();
	HAL_Delay(100);
	rfm_readStatus();
	rfm_enableFIFOmode();
	rfm_setBand(RFM_CS_BAND_868);
	rfm_selectCapacitor(RFM_CS_CAP_12_0);
	rfm_powerManager(RFM_PM_CRYSAL_ON|RFM_PM_DISABLE_CLK_OUT);
	rfm_setBand(RFM_CS_BAND_868);
	rfm_dataRate(0x47,0x00);

	rfm_rxControl(RFM_RC_VDI_OUTPUT |             // VDI output
	                //RFM_RC_VDI_FAST |
	                RFM_RC_BW_134 |               // receiver baseband
	                RFM_RC_LNA_0 |                // LNA 0dB
	                RFM_RC_RSSI_103);

	rfm_dataFilter(RFM_DF_RC_FILTER_DIGITAL |
	                  RFM_DF_CR_AUTO_LOCK_CTRL |
	                  0x0004);

	  rfm_fifoAndResetMode((8 << RFM_FARM_F_BIT) |
	                        RFM_FARM_FF_MASK |
	                        RFM_FARM_DR_MASK);

	  rfm_afc(RFM_AFC_KEEP_WHEN_VDI |
			  RFM_AFC_FREQ_NO_RANGE |
	           RFM_AFC_OE_MASK |
	           RFM_AFC_EN_MASK);
	  rfm_txConfig(0, 6, 0);
	  //rfm_lowDutyCycle(duty)
	  //rfm_pllSetting(RFM_PLLS_OB_MASK |
	   //              &(~RFM_PLLS_DDIT_MASK));
	//konfiguracja pętli PLL
	//Rfm_xmit(PLL|PLL_DH_DIS|SEL_CLK_2_5|MAX_BAUD_256);
	  rfm_wakeUpTimer(0, 0);
	  HAL_Delay(100);
}


void rfm_initRxMode(void)
{
  //PORT_CE |= (1 << FSK);
  rfm_config();

  // Default value = 0x82D8U
  rfm_powerManager(RFM_PM_ENABLE_WHOLE_RX |
                  RFM_PM_BASEBAND_ON |
                  RFM_PM_SYMTHEZIER_ON |
                  RFM_PM_CRYSAL_ON);
  (void)rfm_readStatus();
}


void rfm_initTxMode(void)
{
  //PORT_CE |= (1 << FSK);
  rfm_config();

  // Default value = 0x8238U
  rfm_powerManager(RFM_PM_PLL_PA_TX_ON |
                  RFM_PM_SYMTHEZIER_ON |
                  RFM_PM_CRYSAL_ON);
  (void)rfm_readStatus();
}


void rfm_setFrequency(uint16_t frequency) // frequency should be in range 96...3903
{
  rfm_writeReg(FREQUENCY_SETTING_CMD, RFM_FS_FRQ_MASK, frequency);
}


void rfm_setBand(uint16_t band)
{
  rfm_writeReg(CONFIG_SETTING_CMD, RFM_CS_BAND, band);
}


void rfm_enableFIFOmode()
{
  uint16_t mask = RFM_CS_EL | RFM_CS_EF;
  rfm_writeReg(CONFIG_SETTING_CMD, mask, mask);
}

void rfm_selectCapacitor(uint16_t capacitor)
{
  rfm_writeReg(CONFIG_SETTING_CMD, RFM_CS_CAP, capacitor);
}

void rfm_powerManager(uint16_t settings)
{
  rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_POWER_MASK, settings);
}


void rfm_rxControl(uint16_t settings)
{
  rfm_writeReg(RECEIVER_CONTROL_CMD, RFM_RX_CTRL_MASK, settings);
}


void rfm_dataFilter(uint16_t settings)
{
  rfm_writeReg(DATA_FILTER_CMD, RFM_DATA_FILTER_MASK, settings);
}


void rfm_fifoAndResetMode(uint16_t settings)
{
  rfm_writeReg(FIFO_AND_RESET_MODE_CMD, RFM_FARM_MASK, settings);
}


void rfm_setSynchronPattern(uint8_t pattern)
{
  rfm_writeReg(SYNCHRON_PATTERN_CMD, 0x00ff, (uint16_t)pattern);
}


void rfm_afc(uint16_t settings)
{
  rfm_writeReg(AFC_CMD, RFM_AFC_MASK, settings);
}


void rfm_pllSetting(uint16_t settings)
{
  rfm_writeReg(PLL_SETTING_CMD, RFM_PLL_SETTING_MASK, settings);
}


void rfm_wakeUpTimer(uint8_t r, uint8_t m)
{
  // Twake-up = 1.03 * M * 2^R + 0.5 [ms]
  uint16_t settings = (r << RFM_WUT_R_BIT) | (m << RFM_WUT_M_BIT);
  rfm_writeReg(WAKE_UP_TIMER_CMD, RFM_WAKE_UP_TIMER_MASK, settings);
}


void rfm_lowDutyCycle(uint8_t duty)
{
  rfm_writeReg(LOW_DUTY_CYCLE_CMD, 0x00ff, (uint16_t)duty);
}


void rfm_dataRate(uint8_t r, uint8_t cs)
{
  // BR = 10000 / 29 / (r+1) / (1 + 7 * cs)
  uint16_t settings = ((r << RFM_DR_R_BIT) & RFM_DR_R_MASK) |
                      ((cs << RFM_DR_CS_BIT) & RFM_DR_CS_MASK);
  rfm_writeReg(DATA_RATE_CMD, RFM_DATA_RATE_MASK, settings);
}


void rfm_txConfig(uint8_t mp, uint8_t modulation, uint8_t power)
{
  // fout = fo + (-1)^SIGN * (M + 1) * 15kHz
  uint16_t settings = ((mp << RFM_TXC_MP_BIT) & RFM_TXC_MP_MASK) |
                      ((modulation << RFM_TXC_M_BIT) & RFM_TXC_M_MASK) |
                      ((power << RFM_TXC_P_BIT) & RFM_TXC_P_MASK);
  rfm_writeReg(TX_CONFIG_CMD, RFM_TX_CONTROL_MASK, settings);
}


void rfm_lowBatteryDetector(uint8_t divider, uint8_t voltage)
{
  // Vlbd = 2.25 + V * 0.1 [V]
  uint16_t settings = ((divider << RFM_LBD_D_BIT) & RFM_LBD_D_MASK) |
                      ((voltage << RFM_LBD_V_BIT) & RFM_LBD_V_MASK);
  rfm_writeReg(LOW_BAT_DET_AND_UP_CLK_DIV_CMD, RFM_LOW_BATTERY_MASK, settings);
}

void rfm_software_reset(void)
{
	rfm_transmitSPI(SW_RESET);
}


void rfm_enableReceiveChain(uint8_t enable)
{
  uint16_t mask = RFM_PM_ENABLE_WHOLE_RX |
                  RFM_PM_BASEBAND_ON |
                  RFM_PM_SYMTHEZIER_ON |
                  RFM_PM_CRYSAL_ON;
  if (enable) {
    rfm_writeReg(POWER_MANAGEMENT_CMD, mask, mask);
  } else {
    rfm_writeReg(POWER_MANAGEMENT_CMD, mask, 0);
  }

}


void rfm_baseband(uint8_t enable)
{
  if (enable) {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_BASEBAND_ON, RFM_PM_BASEBAND_ON);
  } else {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_BASEBAND_ON, 0);
  }
}


void rfm_transmisionChain(uint8_t enable)
{
  if (enable) {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_PLL_PA_TX_ON, RFM_PM_PLL_PA_TX_ON);
  } else {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_PLL_PA_TX_ON, 0);
  }
}


void rfm_synthesizer(uint8_t enable)
{
  if (enable) {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_SYMTHEZIER_ON, RFM_PM_SYMTHEZIER_ON);
  } else {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_SYMTHEZIER_ON, 0);
  }
}


void rfm_crystalOscilator(uint8_t enable)
{
  if (enable) {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_CRYSAL_ON, RFM_PM_CRYSAL_ON);
  } else {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_CRYSAL_ON, 0);
  }
}


void rfm_activateLowBatteryDetector(uint8_t enable)
{
  if (enable) {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_LOW_BATTERY_DET_ON, RFM_PM_LOW_BATTERY_DET_ON);
  } else {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_LOW_BATTERY_DET_ON, 0);
  }
}


void rfm_activateWakeUpTimer(uint8_t enable)
{
  if (enable) {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_WAKE_UP_TIM_ON, RFM_PM_WAKE_UP_TIM_ON);
  } else {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_WAKE_UP_TIM_ON, 0);
  }
}


void rfm_clkOutputPin(uint8_t enable)
{
  if (enable) {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_DISABLE_CLK_OUT, 0);
  } else {
    rfm_writeReg(POWER_MANAGEMENT_CMD, RFM_PM_DISABLE_CLK_OUT, RFM_PM_DISABLE_CLK_OUT);
  }
}


void rfm_offsetCalculation(uint8_t enable)
{
  if (enable) {
    rfm_writeReg(AFC_CMD, RFM_AFC_EN_MASK, 0);
  } else {
    rfm_writeReg(AFC_CMD, RFM_AFC_EN_MASK, RFM_AFC_EN_MASK);
  }
}


void rfm_clearFIFO(void)
{
  //reset FIFO and read to receive next Byte
  (void)rfm_transmitSPI(0xCA80U);
  (void)rfm_transmitSPI(0xCA83U);
}


void rfm_sleep(void)
{
  (void)rfm_transmitSPI(0x8200U);
}


void rfm_standby(void)
{
  (void)rfm_transmitSPI(0x8208U);
}


void rfm_setLNAGain(uint16_t gain)
{
  uint16_t regValue = 0x94A0U;
  (void)rfm_transmitSPI((regValue & RFM_RC_G_MASK) | gain);
}


uint16_t rfm_readStatus()
{
  uint16_t status = 0;
  rfm_offsetCalculation(DISABLE);
  status = rfm_transmitSPI(0x0000);
  rfm_offsetCalculation(ENABLE);

  return status;
}


void rfm_waitForReady()
{
  uint16_t status = 0;

  while(!(status & RFM_SR_RGIT_MASK)) {
    status = rfm_readStatus();
  }
}


void rfm_writeReg(uint16_t reg, uint16_t mask, uint16_t value)
{
  uint16_t newValue = RFM12B_registers[reg];
  newValue = (newValue & ~mask) | (value & mask);
  RFM12B_registers[reg] = newValue;
  (void)rfm_transmitSPI(newValue);
}


void rfm_writeDataByte(uint8_t *buffer, uint8_t size)
{
  uint16_t txCmdHeader = 0xB800U;
  uint8_t i = 0;

  rfm_waitForReady();
  rfm_transmitSPI(txCmdHeader | (*buffer));

  if (size > 1) {
    while (++i < size) {
      rfm_waitForReady();
      rfm_transmitSPI(txCmdHeader | (*(buffer + i)));
    }
  }
}


uint8_t rfm_readDatabyte(void)
{
  uint16_t value = 0;

  value = rfm_transmitSPI(RX_FIFO_READ_MASK);
  return (uint8_t)(value & 0xFF);
}


uint16_t rfm_transmitSPI(uint16_t value)
{
  uint8_t writeValue[2] = {0};
  uint8_t readValue[2] = {0};
  uint16_t result;
  writeValue[0]= (value & 0xFF00)>>8;
  writeValue[1]= value & 0x00FF;
  HAL_GPIO_WritePin(RFM12B_NSS_GPIO_Port, RFM12B_NSS_Pin, GPIO_PIN_RESET);
  //RFM12B_NSS_GPIO_Port |= RFM12B_NSS_Pin;
  RFM12B_NSS_GPIO_Port -> ODR &= ~RFM12B_NSS_Pin;

  HAL_SPI_TransmitReceive(&hspi2, writeValue, readValue, 2,1000);
  RFM12B_NSS_GPIO_Port -> ODR |= RFM12B_NSS_Pin;
  //HAL_GPIO_WritePin(RFM12B_NSS_GPIO_Port, RFM12B_NSS_Pin, GPIO_PIN_SET);
  result = (readValue[0]<<8) | (readValue[1]);
  return result;
}


uint8_t rfm_crc(uint8_t *buffer, uint8_t size)
{
  uint8_t sum = 0;
  uint8_t offset = 0;
  uint8_t bit = 0;

  if (*buffer != Preamble) {
    return 0;
  }

  if (*(buffer + 1) != DeviceAddress) {
    return 0;
  }

  while (offset < size) {
    while(bit < 8) {
      if (*(buffer + offset) & (1 << bit++)) {
        ++sum;
      }
    }
    ++offset;
    bit = 0;
  }

  return sum;
}


void rfm_sendPayload(uint8_t* buffer, uint8_t size)
{
  uint8_t header[] = {0xAA, 0xAA, RFM_PREAMBLE_BYTE_1, RFM_PREAMBLE_BYTE_0};
  uint16_t i=0;
  rfm_writeDataByte(header, sizeof(header));
  notepad[PREAMBLE_FIELD] = Preamble;
  notepad[DEV_ADDR_FIELD] = DeviceAddress;
  notepad[SIZE_FIELD] = size;

  while (i < size) {
    notepad[HEADER_LEN + i] = *(buffer + i);
    ++i;

  notepad[HEADER_LEN + i] = rfm_crc(notepad, size + HEADER_LEN);
  rfm_writeDataByte(notepad, sizeof(notepad));
  }
}


void rfm_receivePayload(uint8_t* rxBuffer, uint8_t sizeLimit)
{
	  uint16_t data = rfm_readDatabyte();
	  uint16_t i=0;
	  rxBuffer[i] = data & 0xff;

	  // implement your action for received packet
	  // ... here ...
}


