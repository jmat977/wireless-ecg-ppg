/*
 * r_peak_alg.h
 *
 *  Created on: Jan 1, 2021
 *      Author: jakub
 */


#ifndef INC_R_PEAK_ALG_H_
#define INC_R_PEAK_ALG_H_

#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include "circ_buff_f.h"
#include "usart.h"
#include "rfm12b.h"


#define SIG_LEN 1000

#define ALG

#define ALPHA_COEF 0.46
#define GAMMA_COEF 0.97



#define MAIN_BUFF_F_DATA_SIZE 1024
#define ABS_DIF_LONG_DATA_SIZE 128

#define SLOPE_THRESHOLD 0.11

void wait_for_sample(void);

void r_peak_alg_init(void);

void get_value(uint32_t i);
void calc_moving_avg();
void calc_abs_dif();
void send_val();
void check_for_extremum();
void clean_up(void);

void add_ecg_sample(float * sample);

bool find_extremum(CircBuff_f_t *circbuff);

bool is_searching_win_active(void);

bool is_refactory_win_active(void);

void activate_refactory_win(void);

void activate_searching_win(void);

void update_threshold(void);


extern bool is_cmd_ready;


extern uint32_t cnt, abs_cnt;
extern uint8_t sample_available;

#endif /* INC_R_PEAK_ALG_H_ */
