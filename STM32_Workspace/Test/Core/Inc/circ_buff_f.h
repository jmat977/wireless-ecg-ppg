/*
 * circ_buff.h
 *
 *  Created on: Oct 23, 2020
 *      Author: jakub
 */

#ifndef CIRC_BUFF_H_
#define CICR_BUFF_H_



#include "main.h"
//#include <stdbool.h>
//#include <string.h>
#include <stdio.h>

typedef struct CircBuff CircBuff_f_t;

void circbuff_f_init(CircBuff_f_t *circbuff, float *data, uint32_t size);
bool circbuff_f_write_newest(CircBuff_f_t *circbuff, float *elem);
bool circbuff_f_discard_newest(CircBuff_f_t *circbuff, float *elem);
bool circbuff_f_get_oldest(CircBuff_f_t *circbuff, float *elem);
bool circbuff_f_peek_nth_oldest(CircBuff_f_t *circbuff, float *elem, uint32_t n);
void circbuff_f_emptify(CircBuff_f_t *circbuff);
bool circbuff_f_is_empty(CircBuff_f_t *circbuff);
bool circbuff_f_is_full(CircBuff_f_t *circbuff);


#endif /* CIRC_BUFF_H_ */
