/*
 * moving_avg.h
 *
 *  Created on: Nov 4, 2020
 *      Author: jakub
 */

#ifndef INC_MOVING_AVG_H_
#define INC_MOVING_AVG_H_

#include "stdio.h"
#include "circ_buff_f.h"

void n_pt_moving_avg(CircBuff_f_t *CircBuff_f, const uint8_t n, float *acc);



#endif /* INC_MOVING_AVG_H_ */
