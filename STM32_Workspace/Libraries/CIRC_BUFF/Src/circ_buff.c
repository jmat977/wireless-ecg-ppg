/*
 * circ_buff.c
 *
 *  Created on: Sep 19, 2020
 *      Author: jakub
 */




#include "../Inc/circ_buff.h"


void circbuff_init(CircBuff_t *circbuff, uint8_t *data, uint32_t size)
{
	circbuff->data = data;
	circbuff->size = size;

	circbuff->rd = 0;
	circbuff->wr = 0;
}

bool circbuff_write_newest(CircBuff_t *circbuff, uint8_t *elem)
{
	if (circbuff_is_full(circbuff)) {
		return false;
	}

	circbuff->data[circbuff->wr] = *elem;
	circbuff->wr = (circbuff->wr+1)%circbuff->size;

	return true;
}

bool circbuff_discard_newest(CircBuff_t *circbuff, uint8_t *elem)
{
	if (circbuff_is_empty(circbuff)) {
		return false;
	}

	circbuff->wr = (circbuff->wr-1)%circbuff->size;
	*elem = circbuff->data[circbuff->wr];

	return true;
}

bool circbuff_get_oldest(CircBuff_t *circbuff, uint8_t *elem)
{
	if (circbuff_is_empty(circbuff)) {
		return false;
	}

	*elem = circbuff->data[circbuff->rd];
	circbuff->rd = (circbuff->rd+1)%circbuff->size;

	return true;
}

bool circbuff_peek_nth_oldest(CircBuff_t *circbuff, uint8_t *elem, uint32_t n)
{
	if (circbuff_is_empty(circbuff)) {
		return false;
	}
	uint8_t tmp_rd = (circbuff->rd + n) % (circbuff->size);
	if(tmp_rd < (circbuff->wr) ){
		*elem = circbuff->data[circbuff->rd +n];
		return true;
	}
	else{
		return false;
	}
}

void circbuff_emptify(CircBuff_t *circbuff)
{
	circbuff->rd = circbuff->wr;
}

bool circbuff_is_empty(CircBuff_t *circbuff)
{
	return circbuff->rd == circbuff->wr;
}

bool circbuff_is_full(CircBuff_t *circbuff)
{
	return circbuff->rd == (circbuff->wr+1)%circbuff->size;
}
