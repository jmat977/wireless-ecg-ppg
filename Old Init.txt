    //pc.printf("Initializing MAX86150\r\n");

    //print configurations
    //pc.printf( (ppgOn ? "PPG on" : "PPG off") );
    //pc.printf( (ecgOn ? ", ECG On" : ", ECG off") );
    //pc.printf( (etiOn ? ", ETI On\r\n" : ",  ETI off\r\n") );
    //pc.printf( (ppgRdyIntEn ? "PPG Int On" : "PPG Int off") );
    //pc.printf( (ecgRdyIntEn ? ", ECG Int On" : ", ECG Int off") );
    //pc.printf( (almostFullIntEn ? ", Afull Int On\r\n" : ", Afull Int off\r\n") );
    //write register configurations
	max86xxx_write_reg(SystemControlReg,0x01,1); //chip reset
    HAL_Delay(2); //wait for chip to come back on line
    //if PPG or ETI are not enabled, then FIFO is setup for ECG
    max86xxx_write_reg(FIFOConfigReg,0x6F,1); // [4] FIFO_ROLLS_ON_FULL, clears with status read or FIFO_data read, asserts only once
    uint16_t FIFOCode;
    FIFOCode = ETI_ON ? 0x000A : 0x0000 ; //ETI is last in FIFO
    FIFOCode = ECG_ON ? (FIFOCode<<4 | 0x0009) : FIFOCode;  //insert ECG front of ETI in FIFO
    FIFOCode = PPG_ON ? (FIFOCode<<8 | 0x0021) : FIFOCode; //insert Red(2) and IR (1) in front of ECG in FIFO
    max86xxx_write_reg(FIFODataControlReg1, (char)(FIFOCode & 0x00FF), 1);
    max86xxx_write_reg(FIFODataControlReg2, (char)(FIFOCode >>8), 1);
    max86xxx_write_reg( ppgConfigReg0,0b11010111, 1); //D3 for 100Hz, PPG_ADC_RGE: 32768nA, PPG_SR: 100SpS, PPG_LED_PW: 400uS
    max86xxx_write_reg(LED1PulseAmpReg, PPG_ON ? R_CURRENT : 0x00, 1);
    max86xxx_write_reg(LED2PulseAmpReg, PPG_ON ? IR_CURRENT : 0x00, 1);
    max86xxx_write_reg(ppgConfigReg1, PPG_AVG ? PPG_ON_CHIP_AVG : 0x00, 1);
    max86xxx_write_reg(LEDRangeReg, IR_HI_PWR * 2 + RED_HI_PWR, 1); // PPG_ADC_RGE: 32768nA
    //ecg configuration
    if (ECG_ON) {

        //****************************************
        //ECG data rate is user configurable in theory, but you have to adjust your filter time constants and serial printEvery variables accordingly
    	max86xxx_write_reg(EcgConfigReg1,0x07, 1); //ECG sample rate 0x00 =1600, 0x01=800Hz etc down to 200Hz. add 4 to double frequency (double ADC clock)
    	max86xxx_write_reg(EcgConfigReg3,0x0D, 1); //ECG config 3

    	max86xxx_write_reg(EcgConfigReg2,0x11, 1); //hidden register at ECG config 2, per JY's settings
    	max86xxx_write_reg(EcgConfigReg3,0x3D, 1); //ECG config 3
    	max86xxx_write_reg(EcgConfigReg4,0x02, 1); //ECG config 4 per JY's settings
        //enter test mode
/*
    	max86xxx_write_reg(0xFF,0x54, 1); //write 0x54 to register 0xFF
    	max86xxx_write_reg(0xFF,0x4D, 1); //write 0x4D to register 0xFF
    	max86xxx_write_reg(0xCE,0x01, 1);
    	max86xxx_write_reg(0xCF,0x18, 1); //adjust hidden registers at CE and CF per JY's settings in EV kit
    	max86xxx_write_reg(0xD0,0x01, 1);   //adjust hidden registers D0 (probably ETI)
    	max86xxx_write_reg(0xFF,0x00, 1); //exit test mode
    	*/

    }
    //setup interrupts last
    max86xxx_write_reg(InterruptEnableReg1,( ALMOST_FULL_INT_EN ? 0x80 : (PPG_RDY_INT_EN ? 0x40 : 0x00) ), 1);
    max86xxx_write_reg(InterruptEnableReg2, (ECG_RDY_INT_EN ? 0x04 : 0x00), 1);
    max86xxx_write_reg(SystemControlReg,0x04, 1);//start FIFO

    //pc.printf("done configuring MAX86150\r\n");