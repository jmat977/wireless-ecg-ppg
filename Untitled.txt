#define MAX86XXX_SLAVE_ADDR				0xBC


/*	MAX86908 Registers  22 total	*/
//---------------------------------------------------
#define MAX86908_REG_INT_STATUS_1		0x00
#define MAX86908_INT_A_FULL_			(1<<7)
#define MAX86908_INT_PPG_RDY_			(1<<6)
#define MAX86908_INT_ALC_OVF_			(1<<5)
#define MAX86908_INT_PROX_INT_			(1<<4)
#define MAX86908_INT_PWR_RDY_			(1<<0)

//---------------------------------------------------
#define MAX86908_REG_INT_STATUS_2		0x01
#define MAX86908_INT_VDD_OOR_			(1<<7)
#define MAX86908_INT_ECG_RDY_			(1<<2)

//---------------------------------------------------
#define MAX86908_REG_INT_ENABLE_1		0x02
#define MAX86908_INT_A_FULL_EN_			(1<<7)
#define MAX86908_INT_PPG_RDY_EN_		(1<<6)
#define MAX86908_INT_ALC_OVF_EN_		(1<<5)
#define MAX86908_INT_PROX_INT_EN_		(1<<4)

//---------------------------------------------------
#define MAX86908_REG_INT_ENABLE_2		0x03
#define MAX86908_INT_VDD_OOR_EN_		(1<<7)
#define MAX86908_INT_ECG_RDY_EN_		(1<<2)

//---------------------------------------------------
#define MAX86908_REG_FIFO_WR_PTR		0x04
#define MAX86908_FIFO_WR_PTR_MASK		(0x1F << 0)

//---------------------------------------------------
#define MAX86908_REG_OVF_CNT			0x05
#define MAX86908_OVF_CNT_MASK			(0x1F << 0)

//---------------------------------------------------
#define MAX86908_REG_FIFO_RD_PTR			0x06
#define MAX86908_MASK_FIFO_RD_PTR		(0x1F << 0)

//---------------------------------------------------
#define MAX86908_REG_FIFO_DATA_REG		0x07

//---------------------------------------------------
#define MAX86908_REG_FIFO_CFG			0x08
#define MAX86908_FIFO_A_FULL_MASK		(0xF << 0)
#define MAX86908_FIFO_ROLLS_ON_FULL		(0x1 << 4)
#define MAX86908_A_FULL_TYPE			(0x1 << 5)
#define MAX86908_A_FULL_CLR				(0x1 << 6)

//---------------------------------------------------
#define MAX86908_REG_FIFO_DATA_CTRL_1	0x09
#define	FD1_MASK						(0xF << 0)
#define	FD2_MASK						(0xF << 4)

#define IR_CHANNEL						1
#define RED_CHANNEL						2
#define PILOT_IR_CHANNEL				5
#define PILOT_RED_CHANNEL				6
#define ECG_CHANNEL						9

//---------------------------------------------------
#define MAX86908_REG_FIFO_DATA_CTRL_2	0x0A
#define	FD3_MASK						(0xF << 0)
#define	FD4_MASK						(0xF << 4)

//---------------------------------------------------
#define MAX86908_REG_SYSTEM_CTRL		0x0D

#define MAX86908_SYSTEM_RESET			(0x1 << 0)
#define MAX86908_SYSTEM_SHDN			(0x1 << 1)
#define MAX86908_SYSTEM_FIFO_EN			(0x1 << 2)

//---------------------------------------------------
#define MAX86908_REG_PPG_CFG_1			0x0E
#define MAX86908_PPG_LED_PW_MASK		(0x3 << 0)
#define MAX86908_PPG_SR_MASK			(0xF << 2)
#define MAX86908_PPG_ADC_RGE_MASK		(0x3 << 6)

#define MAX86908_PPG_LED_PW_50US		(0 << 0)
#define MAX86908_PPG_LED_PW_100US		(1 << 0)
#define MAX86908_PPG_LED_PW_200US		(2 << 0)
#define MAX86908_PPG_LED_PW_400US		(3 << 0)

#define MAX86908_PPG_SR_10HZ			(0 << 2)
#define MAX86908_PPG_SR_20HZ			(1 << 2)
#define MAX86908_PPG_SR_50HZ			(2 << 2)
#define MAX86908_PPG_SR_84HZ			(3 << 2)
#define MAX86908_PPG_SR_100HZ			(4 << 2)
#define MAX86908_PPG_SR_200HZ			(5 << 2)
#define MAX86908_PPG_SR_400HZ			(6 << 2)
#define MAX86908_PPG_SR_800HZ			(7 << 2)
#define MAX86908_PPG_SR_1000HZ			(8 << 2)
#define MAX86908_PPG_SR_1600HZ			(9 << 2)
#define MAX86908_PPG_SR_3200HZ			(A << 2)
#define MAX86908_PPG_SR_10HZ_2P			(B << 2)
#define MAX86908_PPG_SR_20HZ_2P			(C << 2)
#define MAX86908_PPG_SR_50HZ_2P			(D << 2)
#define MAX86908_PPG_SR_84HZ_2P			(E << 2)
#define MAX86908_PPG_SR_100HZ_2P			(F << 2)

#define MAX86908_PPG_ADC_RGE_4096		(0 << 6)
#define MAX86908_PPG_ADC_RGE_8192		(1 << 6)
#define MAX86908_PPG_ADC_RGE_16384		(2 << 6)
#define MAX86908_PPG_ADC_RGE_32768		(3 << 6)

//---------------------------------------------------
#define MAX86908_REG_PPG_CFG_2			0x0F
#define MAX86908_SMP_AVE_MASK			(0x7 << 0)

#define MAX86908_SMP_AVE_NONE			(0 << 0)
#define MAX86908_SMP_AVE_2				(1 << 0)
#define MAX86908_SMP_AVE_4				(2 << 0)
#define MAX86908_SMP_AVE_8				(3 << 0)
#define MAX86908_SMP_AVE_16				(4 << 0)
#define MAX86908_SMP_AVE_32				(7 << 0)

//---------------------------------------------------
#define MAX86908_REG_PROX_INT_TH		0x10

//---------------------------------------------------
#define MAX86908_REG_LED1_PA			0x11

//---------------------------------------------------
#define MAX86908_REG_LED2_PA			0x12

//---------------------------------------------------
#define MAX86908_REG_LED_RGE			0x14
#define MAX86908_LED1_RGE_MASK			(0x3 << 0)
#define MAX86908_LED2_RGE_MASK			(0x3 << 2)

#define LED1_RANGE_0_50MA				(0 << 0)
#define LED1_RANGE_50_100MA				(1 << 0)

#define LED2_RANGE_0_50MA				(0 << 2)
#define LED2_RANGE_50_100MA				(1 << 2)

//---------------------------------------------------
#define MAX86908_REG_LED_PILOT_PA		0x15
#define MAX86908_PILOT_PA_MASK			0xFF


//----------------------ECG--------------------------
#define MAX86908_REG_ECG_CFG_1			0x3C
#define MAX86908_ECG_CFG_1_MASK			(0x3 << 0)

#define MAX86908_ECG_ADC_CLK			(1 << 2)

#define MAX86908_ECG_ADC_OSR_0			(0 << 0)
#define MAX86908_ECG_ADC_OSR_1			(1 << 0)
#define MAX86908_ECG_ADC_OSR_2			(2 << 0)
#define MAX86908_ECG_ADC_OSR_3			(3 << 0)


//---------------------------------------------------
#define MAX86908_REG_ECG_CFG_3			0x3E

#define MAX86908_IA_GAIN_MASK			(0x3 << 0)
#define MAX86908_MASK_ECG_IA_GAIN		(3 << 0)
#define MAX86908_ECG_IA_GAIN_5			(0 << 0)
#define MAX86908_ECG_IA_GAIN_9_5		(1 << 0)
#define MAX86908_ECG_IA_GAIN_20			(2 << 0)
#define MAX86908_ECG_IA_GAIN_50			(3 << 0)

#define MAX86908_MASK_ECG_PGA_GAIN		(0x3 << 2)
#define MAX86908_ECG_PGA_GAIN_1			(0 << 2)
#define MAX86908_ECG_PGA_GAIN_2			(1 << 2)
#define MAX86908_ECG_PGA_GAIN_4			(2 << 2)
#define MAX86908_ECG_PGA_GAIN_8			(3 << 2)


//---------------------------------------------------
#define MAX86908_REG_WHOAMI_PART_ID		0xFF

//#define MAX86908_EXPECTED_PART_ID		0x1E


//#define MAX86908_DEFAULT_CURRENT1_VAL	\
			(MAX86908_DEFAULT_CURRENT1 * LED_RANGE_STEP_200uA)
//#define MAX86908_DEFAULT_CURRENT2_VAL	\
			(MAX86908_DEFAULT_CURRENT2 * LED_RANGE_STEP_200uA)
//#define MAX86908_DEFAULT_CURRENT3_VAL	\
			(MAX86908_DEFAULT_CURRENT3 * LED_RANGE_STEP_200uA)

//#define BLUE_NUM_ITEM_IN_FIFO			3
//#define EUV_NUM_ITEM_IN_FIFO			3

//define MAX_PPG_DIODE_VAL				((1 << 19) - 1)
//#define MIN_PPG_DIODE_VAL						0

//#define MIN_LED_DRIVE_CURRENT					0
//#define MAX_LED_DRIVE_CURRENT				204000
