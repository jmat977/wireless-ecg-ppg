/*******************************************************************************
* Author: Ismail Kose, Ismail.Kose@maximintegrated.com
* Copyright (C) 2016 Maxim Integrated Products, Inc., All Rights Reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL MAXIM INTEGRATED BE LIABLE FOR ANY CLAIM, DAMAGES
* OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of Maxim Integrated
* Products, Inc. shall not be used except as stated in the Maxim Integrated
* Products, Inc. Branding Policy.
*
* The mere transfer of this software does not imply any licenses
* of trade secrets, proprietary technology, copyrights, patents,
* trademarks, maskwork rights, or any other form of intellectual
* property whatsoever. Maxim Integrated Products, Inc. retains all
* ownership rights.
*******************************************************************************
*/

#ifndef MAX86XXX_COMMON_H
#define MAX86XXX_COMMON_H
#include "platform.h"
#include "queue.h"

#define LED_DRIVE_CURRENT_FULL_SCALE		\
		(MAX_LED_DRIVE_CURRENT - MIN_LED_DRIVE_CURRENT)

#define AGC_DEFAULT_LED_OUT_RANGE_IR_RED			30
#define AGC_DEFAULT_LED_OUT_RANGE_GREEN				70

#define AGC_DEFAULT_CORRECTION_COEFF			50
#define AGC_DEFAULT_SENSITIVITY_PERCENT			10
#define AGC_DEFAULT_MIN_NUM_PERCENT				50

#define ILLEGAL_OUTPUT_POINTER				1
#define ILLEGAL_DIODE_OUTPUT_MIN_MAX_PAIR	2
#define ILLEGAL_LED_SETTING_MIN_MAX_PAIR	3
#define CONSTRAINT_VIOLATION				4

#define MIN_SAMPLES_NEEDED_FOR_AVG			100
#define MASK_LED_RGE_BITS					3
#if defined (SENSOR_MAX30110) && defined (DUAL_LED)
#define PROX_THRESHOLD_1			100000
#define PROX_THRESHOLD_2			400000
#elif defined(SENSOR_MAX86908) || defined(SENSOR_MAX30110)
#define PROX_THRESHOLD_1			10000
#define PROX_THRESHOLD_2			40000
#else
	#error "Sensor type is not defined"
#endif
#define PROX_DEBOUNCE_SPS			2
#define DAQ_DEBOUNCE_SPS			20

#define DEFAULT_DAQ_LED_CURRENT_1	40000
#define DEFAULT_DAQ_LED_CURRENT_2	40000
#define DEFAULT_PROX_LED_CURRENT_1	10000
#define DEFAULT_PROX_LED_CURRENT_2	0

typedef struct _ecg_cfg_t {
	uint16_t fs;
	uint16_t notch_freq;
	uint16_t cutoff_freq;
	char adaptive_filter_on;
} ecg_cfg_t;

enum LED_CTRL_SM {
	LED_PROX = 1,
	LED_DATA_ACQ,
};

enum {
	LED_1 = 0,
	LED_2,
	LED_3,
	NUM_OF_LED,
};

union led_range {
	struct {
		u8 led1:2;
		u8 led2:2;
		u8 led3:2;
		u8:2;
	};
	u8 val;
};

struct max86xxx_dev;

struct led_control {
	u32 diode_sum[NUM_OF_LED];
	u32 state;
	u32 prox_sum;
	u32 prox_sample_cnt;
	s32 led_current[NUM_OF_LED];
	u32 default_current[NUM_OF_LED];
	s32 agc_led_out_percent;
	s32 agc_corr_coeff;
	s32 agc_min_num_samples;
	s32 agc_sensitivity_percent;
	s32 change_by_percent_of_range[NUM_OF_LED];
	s32 change_by_percent_of_current_setting[NUM_OF_LED];
	s32 change_led_by_absolute_count[NUM_OF_LED];
	int agc_is_enabled;
	union led_range led_range_settings;
	u8 led_ranges;
};

extern struct device_attribute dev_attr_device_id;

int update_led_current(
		union led_range *led_range_settings,
		int led_new_val, int led_num);
void ppg_auto_gain_ctrl(struct max86xxx_dev *dev,
		struct led_control *led_ctrl,
		u32 sample_cnt, int diode_data, int led_num);
int led_prox_init(struct max86xxx_dev *dev,
		struct led_control *led_ctrl, char lpm);
int led_daq_init(struct max86xxx_dev *dev,
		struct led_control *led_ctrl, char lpm);
int led_control_sm(struct max86xxx_dev *dev,
		struct led_control *led_ctrl, int diode_data, char lpm);
void led_control_init(struct led_control *led_ctrl);
void led_control_reset(struct led_control *led_ctrl);

int max86xxx_get_ecg_imp_sample_rate(int *sample_rate);
int max86xxx_get_ppg_flicker_sample_rate(int *sample_rate);
#endif
