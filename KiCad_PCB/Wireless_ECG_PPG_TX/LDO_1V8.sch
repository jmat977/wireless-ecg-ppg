EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8000 5000
encoding utf-8
Sheet 2 9
Title ""
Date "2020-08-01"
Rev "1"
Comp "AGH Faculty of Electrical Engineering, Automatics, Computer Science and Biomedical Engineering"
Comment1 ""
Comment2 "Sytem for aquisition and transmission of biomedical data "
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2580 1800 0    50   Input ~ 0
V_IN
Text HLabel 3380 2000 0    50   Input ~ 0
EN
Text HLabel 5190 1800 2    50   Output ~ 0
V_OUT
$Comp
L Regulator_Linear:MIC5504-1.8YM5 U7
U 1 1 5F98643C
P 3820 1900
F 0 "U7" H 3820 2267 50  0000 C CNN
F 1 "MIC5504-1.8YM5" H 3820 2176 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 3820 1500 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MIC550X.pdf" H 3570 2150 50  0001 C CNN
	1    3820 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3380 2000 3420 2000
Wire Wire Line
	2580 1800 2900 1800
Wire Wire Line
	4220 1800 4600 1800
Wire Wire Line
	3820 2200 3820 2530
$Comp
L power:GNDD #PWR02
U 1 1 5F988952
P 3820 2700
F 0 "#PWR02" H 3820 2450 50  0001 C CNN
F 1 "GNDD" H 3824 2545 50  0000 C CNN
F 2 "" H 3820 2700 50  0001 C CNN
F 3 "" H 3820 2700 50  0001 C CNN
	1    3820 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C18
U 1 1 5F989CE8
P 2900 2250
F 0 "C18" H 3015 2296 50  0000 L CNN
F 1 "1­μ" H 3015 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2938 2100 50  0001 C CNN
F 3 "~" H 2900 2250 50  0001 C CNN
	1    2900 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C19
U 1 1 5F98A377
P 4600 2250
F 0 "C19" H 4715 2296 50  0000 L CNN
F 1 "1­μ" H 4715 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4638 2100 50  0001 C CNN
F 3 "~" H 4600 2250 50  0001 C CNN
	1    4600 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2100 2900 1800
Connection ~ 2900 1800
Wire Wire Line
	2900 1800 3420 1800
Wire Wire Line
	2900 2400 2900 2530
Wire Wire Line
	2900 2530 3820 2530
Connection ~ 3820 2530
Wire Wire Line
	3820 2530 3820 2700
Wire Wire Line
	3820 2530 4600 2530
Wire Wire Line
	4600 2530 4600 2400
Wire Wire Line
	4600 2100 4600 1800
Connection ~ 4600 1800
Wire Wire Line
	4600 1800 5190 1800
$EndSCHEMATC
